<?php

namespace FC\MainBundle\Constantes;

class MaestrosSimplesFieldTypeEnum { //OJO - Al agregar ítems preferentemente hacerlo al final y modificar el form.html.twig para considerar el nuevo tipo
    const ftString=0;
    const ftDate=1;
    const ftTime=2;
    const ftDateTime=3;
    const ftInteger=4;
    const ftBigInteger=5;
    const ftFloat=6;
    const ftBoolean=7;
    const ftEmail=8;
    const ftFile=9;
}
