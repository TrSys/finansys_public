//jquery de evaluacion
$(document).ready(function() {
	var id = $("#NameFather").val();
	calculoRiesgo(id);

	$(".tab-click").on('click',function() {
		var id = $(this).attr('data-id');
		calculoRiesgo(id);
	});

	$(".calcular").on('change',function() {
		var id = $(this).closest('.bloque').attr('id');
		calculoRiesgo(id);
	});

	$(".idprobabilidadestimada").change(function() {
		var id = $(this).closest('.bloque').attr('id');
		var contribfachum = $("#"+id).find(".contribfachum").is(':checked');

		if($(this).val() != 0){
			$("#"+id).find('input[type=checkbox]').attr('disabled', 'disabled');
			$("#"+id).find('input[type=checkbox]').prop( "checked", false )
			$("#"+id).find("#contribuciones, #respuesta").find('input:radio, select').val(0);
			$("#"+id).find("#contribuciones, #respuesta").find('input:radio, select').attr('disabled', 'disabled');
		}else{
			$("#"+id).find('input[type=checkbox]').removeAttr('disabled');
		}
	});

	$(".contribuciones, .respuesta").find('input[type=checkbox]').click(function(){
		var id = $(this).closest('.bloque').attr('id');
		name = $(this).attr('name');
		checkstatus = $(this).is(':checked');

		//alert($(this).hasClass( "contribcondmat" ));

		if(!checkstatus && $(this).hasClass( "contribcondmat" )){
			$("#"+id).find('.idinspcontequi, .idinspcontambtrabajo').val(0);
			$("#"+id).find('.idinspcontequi, .idinspcontambtrabajo').attr('disabled', 'disabled');
		}else if(checkstatus && $(this).hasClass( "contribcondmat" )){
			$("#"+id).find('.idinspcontequi, .idinspcontambtrabajo').removeAttr('disabled');
		}

		if(!checkstatus && $(this).hasClass( "contribfachum" )){
			$("#"+id).find('.idinspcontcapacpers, .idinspcontaptitud').val(0);
			$("#"+id).find('.idinspcontcapacpers, .idinspcontaptitud').attr('disabled', 'disabled');
		}else if(checkstatus && $(this).hasClass( "contribfachum" )){
			$("#"+id).find('.idinspcontcapacpers, .idinspcontaptitud').removeAttr('disabled');
		}

		if(!checkstatus && $(this).hasClass( "contribsuperv" )){
			$("#"+id).find('.idinspconttipotarea, .idinspcontsupervision, .idinspcontexperiencia').val(0);
			$("#"+id).find('.idinspconttipotarea, .idinspcontsupervision, .idinspcontexperiencia').attr('disabled', 'disabled');
		}else if(checkstatus && $(this).hasClass( "contribsuperv" )){
			$("#"+id).find('.idinspconttipotarea, .idinspcontsupervision, .idinspcontexperiencia').removeAttr('disabled');
		}

		if(!checkstatus && $(this).hasClass( "resppasiva" )){
			$("#"+id).find('.idinsprespmedpasiva, .idinsprespequipasiva').val(0);
			$("#"+id).find('.idinsprespmedpasiva, .idinsprespequipasiva').attr('disabled', 'disabled');
		}else if(checkstatus && $(this).hasClass( "resppasiva" )){
			$("#"+id).find('.idinsprespmedpasiva, .idinsprespequipasiva').removeAttr('disabled');
		}


		if(!checkstatus && $(this).hasClass( "respactiva" )){
			$("#"+id).find('.idinsprespmedactiva, .idinsprespequiactiva').val(0);
			$("#"+id).find('.idinsprespmedactiva, .idinsprespequiactiva').attr('disabled', 'disabled');
		}else if(checkstatus && $(this).hasClass( "respactiva" )){
			$("#"+id).find('.idinsprespmedactiva, .idinsprespequiactiva').removeAttr('disabled');
		}

		if(!checkstatus && $(this).hasClass( "resporganizacion" )){
			$("#"+id).find('.idinsprespaptitud, .idinsprespcapac, .idinsprespsuperv').val(0);
			$("#"+id).find('.idinsprespaptitud, .idinsprespcapac, .idinsprespsuperv').attr('disabled', 'disabled');
		}else if(checkstatus && $(this).hasClass( "resporganizacion" )){
			$("#"+id).find('.idinsprespaptitud, .idinsprespcapac, .idinsprespsuperv').removeAttr('disabled');
		}

	});

	function calculoRiesgo(id) {
		//Limpiar Cajas Ev. Riesgo
		var Txt_RiesgoExposicionDescrip = "";
		var Txt_RiesgoProbOcurrencia = 0;
		var Txt_RiesgoProbConsecuencias = 0;
		var Txt_RiesgoProbTotal = 0;
		var Txt_RiesgoProbTotalDescrip = "";
		var Txt_RiesgoNivelEval = 0;
		var Txt_RiesgoNivelEvalDescrip = "";
		var Txt_RiesgoColorEvalKBC = "";
		var Txt_RiesgoPlazo = 0;
		var Lbl_MatrizDeRiesgoMarcador = 0;
		//Limpiar Cajas Ev. Riesgo

		var contribcondmat = $("#"+id).find(".contribcondmat").is(':checked');
		var contribcondmat_valor = contribcondmat ? 1 : 0;

		var contribfachum = $("#"+id).find(".contribfachum").is(':checked');
		var contribfachum_valor = contribfachum ? 1 : 0;
		var contribsuperv = $("#"+id).find(".contribsuperv").is(':checked');
		var contribsuperv_valor = contribsuperv ? 1 : 0;
		var resppasiva = $("#"+id).find(".resppasiva").is(':checked');
		var resppasiva_valor = resppasiva ? 1 : 0;
		var respactiva = $("#"+id).find(".respactiva").is(':checked');
		var respactiva_valor = respactiva ? 1 : 0;
		var resporganizacion = $("#"+id).find(".resporganizacion").is(':checked');
		var resporganizacion_valor = resporganizacion ? 1 : 0;

		var probabilidadestimada_valor = parseInt($("#"+id).find('.idprobabilidadestimada  option:selected').val());
		var probabilidadestimada_indice = parseFloat($("#"+id).find('.idprobabilidadestimada option:selected').attr('data-indice'));
		if (isNaN(probabilidadestimada_indice)) {
			probabilidadestimada_indice = 0;
		}

		var probabilidadestimada_clasificacion = $("#"+id).find(".idprobabilidadestimada  option:selected").attr('data-clasificacion');
		var probabilidad_descripcion = $("#"+id).find( ".idprobabilidadestimada  option:selected").attr('data-descripcion');


		var inspcontequi_valor = parseInt($("#"+id).find(".idinspcontequi option:selected").val());
		var inspcontequi_indice = parseInt($("#"+id).find(".idinspcontequi option:selected").attr('data-indice'));
		var inspcontambtrabajo_valor = parseInt($("#"+id).find(".idinspcontambtrabajo option:selected").val());
		var inspcontambtrabajo_indice = parseInt($("#"+id).find(".idinspcontambtrabajo option:selected").attr('data-indice'));
		var inspcontcapacpers_valor = parseInt($("#"+id).find(".idinspcontcapacpers option:selected").val());
		var inspcontcapacpers_indice = parseInt($("#"+id).find(".idinspcontcapacpers option:selected").attr('data-indice'));
		var inspcontaptitud_valor = parseInt($("#"+id).find(".idinspcontaptitud option:selected").val());
		var inspcontaptitud_indice = parseInt($("#"+id).find(".idinspcontaptitud option:selected").attr('data-indice'));
		var inspconttipotarea_valor = parseInt($("#"+id).find(".idinspconttipotarea option:selected").val());
		var inspconttipotarea_indice = parseInt($("#"+id).find(".idinspconttipotarea option:selected").attr('data-indice'));
		var inspcontsupervision_valor = parseInt($("#"+id).find(".idinspcontsupervision option:selected").val());
		var inspcontsupervision_indice = parseInt($("#"+id).find(".idinspcontsupervision option:selected").attr('data-indice'));
		var inspcontexperiencia_valor = parseInt($("#"+id).find(".idinspcontexperiencia option:selected").val());
		var inspcontexperiencia_indice = parseInt($("#"+id).find(".idinspcontexperiencia option:selected").attr('data-indice'));

		var insprespmedactiva_valor = parseInt($("#"+id).find(".idinsprespmedactiva option:selected").val());
		var insprespmedactiva_indice = parseInt($("#"+id).find(".idinsprespmedactiva option:selected").attr('data-indice'));
		var insprespequiactiva_valor = parseInt($("#"+id).find(".idinsprespequiactiva option:selected").val());
		var insprespequiactiva_indice = parseInt($("#"+id).find(".idinsprespequiactiva option:selected").attr('data-indice'));

		var insprespmedpasiva_valor = parseInt($("#"+id).find(".idinsprespmedpasiva option:selected").val());
		var insprespmedpasiva_indice = parseInt($("#"+id).find(".idinsprespmedpasiva option:selected").attr('data-indice'));
		var insprespequipasiva_valor = parseInt($("#"+id).find(".idinsprespequipasiva option:selected").val());
		var insprespequipasiva_indice = parseInt($("#"+id).find(".idinsprespequipasiva option:selected").attr('data-indice'));
		var insprespaptitud_valor = parseInt($("#"+id).find(".idinsprespaptitud option:selected").val());
		var insprespaptitud_indice = parseInt($("#"+id).find(".idinsprespaptitud option:selected").attr('data-indice'));
		var insprespcapac_valor	 = parseInt($("#"+id).find(".idinsprespcapac option:selected").val());
		var insprespcapac_indice = parseInt($("#"+id).find(".idinsprespcapac option:selected").attr('data-indice'));
		var insprespsuperv_valor = parseInt($("#"+id).find(".idinsprespsuperv option:selected").val());
		var insprespsuperv_indice = parseInt($("#"+id).find(".idinsprespsuperv option:selected").attr('data-indice'));

		//Riesgos
		var riesgodanopers_valor = parseInt($("#"+id).find(".idriesgodanopers option:selected").val());
		var riesgodanopers_indice = parseFloat($("#"+id).find(".idriesgodanopers option:selected").attr('data-indice'));
		if (isNaN(riesgodanopers_indice)) {
			riesgodanopers_indice = 0;
		}
		var riesgodanopers_clasificacion =$("#"+id).find(".idriesgodanopers option:selected").attr('data-clasificacion');
		var riesgodanopers_valorkbc = parseFloat($("#"+id).find(".idriesgodanopers option:selected").attr('data-valorkbc'));

		var riesgodanomat_valor = parseInt($("#"+id).find(".idriesgodanomat option:selected").val());
		var riesgodanomat_indice = parseFloat($("#"+id).find(".idriesgodanomat option:selected").attr('data-indice'));
		if (isNaN(riesgodanomat_indice)) {
			riesgodanomat_indice = 0;
		}
		var riesgodanomat_clasificacion = $("#"+id).find(".idriesgodanomat option:selected").attr('data-clasificacion');
		var riesgodanomat_valorkbc = parseFloat($("#"+id).find(".idriesgodanomat option:selected").attr('data-valorkbc'));

		var riesgodanoamb_valor = parseInt($("#"+id).find(".idriesgodanoamb option:selected").val());
		var riesgodanoamb_indice = parseFloat($("#"+id).find(".idriesgodanoamb option:selected").attr('data-indice'));
		if (isNaN(riesgodanoamb_indice)) {
			riesgodanoamb_indice = 0;
		}
		var riesgodanoamb_clasificacion =$("#"+id).find(".idriesgodanoamb option:selected").attr('data-clasificacion');
		var riesgodanoamb_valorkbc = parseFloat($("#"+id).find(".idriesgodanoamb option:selected").attr('data-valorkbc'));

		var riesgoexposicion_valor = parseInt($("#"+id).find(".idriesgoexposicion option:selected").val());
		var riesgoexposicion_indice = parseFloat($("#"+id).find(".idriesgoexposicion option:selected").attr('data-indice'));
		if (isNaN(riesgoexposicion_indice)) {
			riesgoexposicion_indice = 0;
		}

		var riesgoexposicion_clasificacion = $("#"+id).find(".idriesgoexposicion option:selected").attr('data-clasificacion');

		//=== Calculo de Riesgo Material / Personal / Exposición ===============

		//=== Factor de Condiciones Materiales =============
		var llFactorCondMat = 0;
		if(contribcondmat){
			llAux1 = 0; llAux2 = 0; llAuxMedio = 0;
			llAux1 = inspcontequi_indice;
			llAux2 = inspcontambtrabajo_indice;
			sum = llAux1 + llAux2;

			if (sum < 0) {
				llFactorCondMat = -1;
			} else if (sum > 0) {
				llFactorCondMat = 1;
			} else {
				llFactorCondMat = 0;
			}
		}

		//=== Factor de Supervisión ========================
		var llFactorSuperv = 0;
		if(contribsuperv){
			llAux1 = 0; llAux2 = 0; llAuxMedio = 0;
			llAux1 =  inspconttipotarea_indice;
			llAux2 =  inspcontexperiencia_indice;
			sum = llAux1 + llAux2;

			if (sum < 0) {
				llAuxMedio = -1;
			} else if (sum > 0) {
				llAuxMedio = 1;
			} else {
				llAuxMedio = 0;
			}

			llAux1 = llAuxMedio;
			llAux2 = inspcontsupervision_indice;
			sum = llAux1 + llAux2;

			if (sum < 0) {
				llFactorSuperv = -1;
			} else if (sum > 0) {
				llFactorSuperv = 1;
			} else {
				llFactorSuperv = 0;
			}
		}


		//=== Factor de Factor humano ========================
		var llFactorHumano = 0;
		if(contribfachum){
			llAux1 = 0; llAux2 = 0; llAuxMedio = 0;
			llAux1 =  inspcontcapacpers_indice;
			llAux2 =  inspcontaptitud_indice;
			sum = llAux1 + llAux2;
			if (sum < 0) {
				llFactorHumano = -1;
			} else if (sum > 0) {
				llFactorHumano = 1;
			} else {
				llFactorHumano = 0;
			}
		}

		//=== Factor Prevención ==================
		var ldFactorPrevencion = 0;
		var ldProbabilidadOcurrencia = 0;
		if(contribcondmat_valor > 0 || contribsuperv_valor > 0 || contribfachum_valor > 0){
			ldFactorPrevencion = ((contribcondmat_valor * llFactorCondMat) + (contribsuperv_valor * llFactorSuperv) + (contribfachum_valor * llFactorHumano)) / (contribcondmat_valor + contribsuperv_valor + contribfachum_valor);
			//=== Probabilidad de Ocurrencia ===================
			ldProbabilidadOcurrencia = Math.pow(10,(-ldFactorPrevencion));
			//=== Probabilidad de Ocurrencia ===================
		}

		//=== Factor de Respuesta Pasiva ===================
		var llFactorRespPasiva = 0;
		if(resppasiva){
			llAux1 = 0; llAux2 = 0; llAuxMedio = 0;
			llAux1 =  insprespmedpasiva_indice;
			llAux2 =  insprespequipasiva_indice;
			sum = llAux1 + llAux2;
			if (sum < 0) {
				llFactorRespPasiva = -1;
			} else if (sum > 0) {
				llFactorRespPasiva = 1;
			} else {
				llFactorRespPasiva = 0;
			}
		}

		//=== Factor de Respuesta Activa ===================
		var llFactorRespActiva = 0;
		if(respactiva){
			llAux1 = 0; llAux2 = 0; llAuxMedio = 0;
			llAux1 =  insprespmedactiva_indice;
			llAux2 =  insprespequiactiva_indice;
			sum = llAux1 + llAux2;
			if (sum < 0) {
				llFactorRespActiva = -1;
			} else if (sum > 0) {
				llFactorRespActiva = 1;
			} else {
				llFactorRespActiva = 0;
			}
		}


		//=== Factor de Respuesta Organizacional ===========
		var llFactorRespOrg = 0;
		if(resporganizacion){
			llAux1 = 0; llAux2 = 0; llAuxMedio = 0;
			llAux1 =  insprespaptitud_indice;
			llAux2 =  insprespcapac_indice;

			sum = llAux1 + llAux2;
			if (sum < 0) {
				llAuxMedio = -1;
			} else if (sum > 0) {
				llAuxMedio = 1;
			} else {
				llAuxMedio = 0;
			}
			llAux1 = llAuxMedio;
			llAux2 = insprespsuperv_indice;

			sum = llAux1 + llAux2;
			if (sum < 0) {
				llFactorRespOrg = -1;
			} else if (sum > 0) {
				llFactorRespOrg = 1;
			} else {
				llFactorRespOrg = 0;
			}
		}

		//=== Factor De Respuesta ===================
		var ldFactorRespuesta = 0;
		var ldProbabilidadConsecuencias = 0;
		if(resppasiva ||  respactiva || resporganizacion){
			ldFactorRespuesta = ((resppasiva_valor * llFactorRespPasiva) + (respactiva_valor * llFactorRespActiva) + (resporganizacion_valor * llFactorRespOrg)) / (resppasiva_valor + respactiva_valor + resporganizacion_valor);
			//=== Probabilidad de Consecuencias ===================
			ldProbabilidadConsecuencias = Math.pow(10,(-ldFactorRespuesta));
			//=== Probabilidad de Consecuencias ===================
		}

		//=== Últimos Calculos ================================
		var IndiceProbabilidadTotal = 0;
		IndiceProbabilidadTotal = Math.pow((ldProbabilidadOcurrencia * ldProbabilidadConsecuencias), 0.5);
		if (isNaN(IndiceProbabilidadTotal)) {
			IndiceProbabilidadTotal = 0;
		}

		//alert(ldProbabilidadConsecuencias);

		//=== Últimos Calculos ================================

		//=== Seteo de Control de Usuario =====================
		var Txt_RiesgoGravedadIndice = 0;
		var llValorKBCRiesgoDanoTotal = 0;
		var Txt_RiesgoGravedadDescrip = "";
		var Txt_RiesgoGravedadTipo = "";
		if (riesgodanopers_valor > 0 || riesgodanomat_valor > 0 || riesgodanoamb_valor > 0) {
			if (riesgodanomat_indice > riesgodanopers_indice) {
				if (riesgodanoamb_indice > riesgodanomat_indice) {
					Txt_RiesgoGravedadIndice = riesgodanoamb_indice;
					llValorKBCRiesgoDanoTotal = riesgodanoamb_valorkbc;
					Txt_RiesgoGravedadDescrip = riesgodanoamb_clasificacion;
					Txt_RiesgoGravedadTipo = "Ambiental";
				} else {
					Txt_RiesgoGravedadIndice = riesgodanomat_indice;
					llValorKBCRiesgoDanoTotal = riesgodanomat_valorkbc;
					Txt_RiesgoGravedadDescrip = riesgodanomat_clasificacion;
					Txt_RiesgoGravedadTipo = "Material";
				}
			} else {
				if (riesgodanoamb_indice > riesgodanopers_indice) {
					Txt_RiesgoGravedadIndice = riesgodanoamb_indice;
					llValorKBCRiesgoDanoTotal = riesgodanoamb_valorkbc;
					Txt_RiesgoGravedadDescrip = riesgodanoamb_clasificacion;
					Txt_RiesgoGravedadTipo = "Ambiental";
				} else {
					Txt_RiesgoGravedadIndice = riesgodanopers_indice;
					llValorKBCRiesgoDanoTotal = riesgodanopers_valorkbc;
					Txt_RiesgoGravedadDescrip = riesgodanopers_clasificacion;
					Txt_RiesgoGravedadTipo = "Personal";
				}
			}
		}

		var IndiceRiesgoExposicion = 0;
		IndiceRiesgoExposicion = 0.1 * riesgoexposicion_indice;
		Txt_RiesgoExposicionDescrip = riesgoexposicion_clasificacion;


		if(probabilidadestimada_valor > 0){
			//alert(probabilidadestimada_valor);
			ldProbabilidadOcurrencia = probabilidadestimada_indice;
			ldProbabilidadConsecuencias = probabilidadestimada_indice;
			IndiceProbabilidadTotal = probabilidadestimada_indice;
			Txt_RiesgoProbOcurrencia = probabilidadestimada_indice;
			Txt_RiesgoProbConsecuencias = probabilidadestimada_indice;
			Txt_RiesgoProbTotal = probabilidadestimada_indice;
			Txt_RiesgoProbTotalDescrip = probabilidad_descripcion;
		}else{
			Txt_RiesgoProbOcurrencia = parseFloat(ldProbabilidadOcurrencia);
			Txt_RiesgoProbConsecuencias = parseFloat(ldProbabilidadConsecuencias);
			Txt_RiesgoProbTotal = parseFloat(IndiceProbabilidadTotal);
			if (IndiceProbabilidadTotal > 8) {
				Txt_RiesgoProbTotalDescrip = "Muy probable";
			} else if (IndiceProbabilidadTotal > 4.5) {
				Txt_RiesgoProbTotalDescrip = "Muy posible";
			} else if (IndiceProbabilidadTotal > 2) {
				Txt_RiesgoProbTotalDescrip = "Poco usual";
			} else if (IndiceProbabilidadTotal > 0.75) {
				Txt_RiesgoProbTotalDescrip = "Muy poco usual";
			} else if (IndiceProbabilidadTotal > 0.35) {
				Txt_RiesgoProbTotalDescrip = "Imaginable pero muy poco posible";
			} else if (IndiceProbabilidadTotal > 0.35) {
				Txt_RiesgoProbTotalDescrip = "Prácticamente imposible";
			} else if (IndiceProbabilidadTotal > 0.15) {
				Txt_RiesgoProbTotalDescrip = "Prácticamente imposible";
			} else {
				Txt_RiesgoProbTotalDescrip = "Virtualmente imposible";
			}
		}


		Txt_RiesgoNivelEval = parseFloat(IndiceRiesgoExposicion * Txt_RiesgoGravedadIndice * Txt_RiesgoProbTotal);

		if (Txt_RiesgoNivelEval > 400) {
			Txt_RiesgoNivelEvalDescrip = "Muy alto. Requiere corrección inmediata";
			Txt_RiesgoPlazo = 7;
		} else if (Txt_RiesgoNivelEval > 200) {
			Txt_RiesgoNivelEvalDescrip = "Alto. Requiere corrección prioritaria";
			Txt_RiesgoPlazo = 30;
		} else if (Txt_RiesgoNivelEval > 50) {
			Txt_RiesgoNivelEvalDescrip = "Medio. Necesita corrección";
			Txt_RiesgoPlazo = 90;
		} else if (Txt_RiesgoNivelEval > 20) {
			Txt_RiesgoNivelEvalDescrip = "Bajo. Estudiar posibles correcciones";
			Txt_RiesgoPlazo = 180;
		} else if (Txt_RiesgoNivelEval > 0) {
			Txt_RiesgoNivelEvalDescrip = "Aceptable";
			Txt_RiesgoPlazo = 360;
		} else {
			Txt_RiesgoNivelEval = 0;
			Txt_RiesgoNivelEvalDescrip = "";
			Txt_RiesgoPlazo = 0;
		}

		var EvRiesgoKBC = "";
		if(IndiceProbabilidadTotal  != 0 || ldProbabilidadOcurrencia  != 0 || ldProbabilidadConsecuencias  != 0 ){
			if (llValorKBCRiesgoDanoTotal >= 6) {
				EvRiesgoKBC = "A";
			} else if (llValorKBCRiesgoDanoTotal == 5) {
				EvRiesgoKBC = "B";
			} else if (llValorKBCRiesgoDanoTotal == 4) {
				EvRiesgoKBC = "C";
			} else if (llValorKBCRiesgoDanoTotal == 3) {
				EvRiesgoKBC = "D";
			} else if (llValorKBCRiesgoDanoTotal == 2) {
				EvRiesgoKBC = "E";
			} else {
				EvRiesgoKBC = "F";
			}
			if ((IndiceRiesgoExposicion * IndiceProbabilidadTotal) <= 0.5) {
				EvRiesgoKBC = EvRiesgoKBC + "1";
			} else if ((IndiceRiesgoExposicion * IndiceProbabilidadTotal) <= 1.33) {
				EvRiesgoKBC = EvRiesgoKBC + "2";
			} else if ((IndiceRiesgoExposicion * IndiceProbabilidadTotal) <= 2.86) {
				EvRiesgoKBC = EvRiesgoKBC + "3";
			} else if ((IndiceRiesgoExposicion * IndiceProbabilidadTotal) <= 6.67) {
				EvRiesgoKBC = EvRiesgoKBC + "4";
			} else {
				EvRiesgoKBC = EvRiesgoKBC + "5";
			}
			Txt_RiesgoColorEvalKBC = gTomarColorDescripcionEvRiesgoMatriz(EvRiesgoKBC);
		}

		$("#"+id).find(".exp1").val(IndiceRiesgoExposicion);
		$("#"+id).find(".exp2").val(Txt_RiesgoExposicionDescrip);
		$("#"+id).find(".probp1").val(IndiceProbabilidadTotal.toFixed(2));
		$("#"+id).find(".probp2").val(Txt_RiesgoProbTotalDescrip);
		$("#"+id).find(".nivr1").val(Txt_RiesgoNivelEval.toFixed(2));
		$("#"+id).find(".nivr2").val(Txt_RiesgoNivelEvalDescrip);
		$("#"+id).find(".nivkbc1").val(EvRiesgoKBC);
		$("#"+id).find(".evriesgoprioridad").val(gTomarEvRiesgoPrioridad(EvRiesgoKBC));
		$("#"+id).find(".nivkbc2").val(Txt_RiesgoColorEvalKBC);
		$("#"+id).find(".grav1").val(Txt_RiesgoGravedadIndice);
		$("#"+id).find(".grav2").val(Txt_RiesgoGravedadDescrip);
		$("#"+id).find(".grav3").val(Txt_RiesgoGravedadTipo);
		$("#"+id).find(".probo1").val(Txt_RiesgoProbOcurrencia.toFixed(2));
		$("#"+id).find(".probc1").val(Txt_RiesgoProbConsecuencias.toFixed(2));
		$("#"+id).find(".plazo1").val(Txt_RiesgoPlazo);

		$('.grafico').empty();
		if (EvRiesgoKBC != "") {
			$("#" + id).find('.' + EvRiesgoKBC).html('<strong style="font-weight: bold; solid-color: true; color: ' + gTomarForeColorEvRiesgoMatriz(EvRiesgoKBC) + ';">X</strong>');
		}
	}
});

function gTomarColorDescripcionEvRiesgoMatriz(Value) {
	switch (Value) {
		case "A3":
		case "A4":
		case "A5":
		case "B4":
		case "B5":
		case "C5":
			return "Rojo (alto)";
			break;
		case "A1":
		case "A2":
		case "B2":
		case "B3":
		case "C3":
		case "C4":
		case "D4":
		case "D5":
		case "E5":
			return "Azul (medio)";
			break;
		default:
			return "Amarillo (bajo)";
			break;
	}
}

function gTomarForeColorEvRiesgoMatriz(Value) {
	switch (Value) {
		case "A3":
		case "A4":
		case "A5":
		case "B4":
		case "B5":
		case "C5":
			return 'white';
			break;
		case "A1":
		case "A2":
		case "B2":
		case "B3":
		case "C3":
		case "C4":
		case "D4":
		case "D5":
		case "E5":
			return 'white';
			break;
		default:
			return 'black';
			break;
	}
}

function gTomarEvRiesgoPrioridad(Value) {
	switch (Value) {
		case "A3":
		case "A4":
		case "A5":
		case "B4":
		case "B5":
		case "C5":
			return 0;
			break;
		case "A1":
		case "A2":
		case "B2":
		case "B3":
		case "C3":
		case "C4":
		case "D4":
		case "D5":
		case "E5":
			return 1;
			break;
		default:
			return 2;
			break;
	}
}