<?php

namespace FC\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CapturasSimplesController extends Controller {
    public function callerCapturaAction($Title, $Entity, $Query = null, $Fields, $Where, $Button, $Input = null, $Trigger = null, $CleanInput = null) {
        $uniqid = str_replace('.', '', uniqid('id', true));
        return $this->render('FCMainBundle:CapturasSimples:form.html.twig', array(
            'Title'.$uniqid => $Title,
            'Entity'.$uniqid => $Entity,
            'Query'.$uniqid => $Query,
            'Fields'.$uniqid => $Fields,
            'Where'.$uniqid => $Where,
            'Button'.$uniqid => $Button,
            'Input'.$uniqid => $Input,
            'Trigger'.$uniqid => $Trigger,
            'CleanInput'.$uniqid => $CleanInput,
            'uniqid' => $uniqid
        ));
    }

    public function ajaxCapturaAction(Request $request) {
        $params = $request->request->all();
        $uniqid = (isset($params["uniqid"]) ? $params["uniqid"] : null);
        $Entity = (isset($params["Entity".$uniqid]) ? $params["Entity".$uniqid] : null);
        $Query = (isset($params["Query".$uniqid]) ? $params["Query".$uniqid] : null);
        $Fields = (isset($params["Fields".$uniqid]) ? json_decode($params["Fields".$uniqid], true) : null);
        $Where = (isset($params["Where".$uniqid]) ? $params["Where".$uniqid] : null);

        if ($Query=="" or $Query==null) {
            $dql = "";
            foreach ($Fields as $item) {
                $dql .= ($dql == "" ? "a." : ", a.") . $item[1];
            }
            $dql = "SELECT " . $dql . " FROM " . $Entity . " a" . ($Where == null ? "" : " WHERE " . $Where);
        } else {
            $dql = $Query;
        }

        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery($dql)->getResult();

        return new Response(json_encode(array("success".$uniqid => $result)));
    }
}
