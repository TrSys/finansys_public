<?php

namespace FC\MainBundle\Constantes;

class ConstantesEnum {
    const idTipoGasto = 1;
    const idTipoIngreso = 2;
    const idTipoTransferencia = 3;

    static $Sistemas = array(
        'FI' => 'finansys',
    );

    static $Modulos = array(
        'finansys' => array('Sistema' => 'FI', 'RootIndex' => 'fc_finansys_homepage'),
    );
}
