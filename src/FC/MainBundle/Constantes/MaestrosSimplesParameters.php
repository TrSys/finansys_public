<?php

namespace FC\MainBundle\Constantes;

use Doctrine\Common\Persistence\ObjectManager;
use FC\MainBundle\Servicios\ListadosTypesFilterEnum;
use FC\MainBundle\Servicios\Seguridad;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MaestrosSimplesParameters {
    private $Parameters;
    private $modulo;
    private $moduloAlias;
    private $Sistema;
    private $RootIndex;
    private $em;
    private $container;
    private $seguridad;

    public function __construct(ObjectManager $em, ContainerInterface $container, Seguridad $seguridad) {
        $this->em = $em;
        $this->container = $container;
        $this->seguridad = $seguridad;
    }

    public function SetParameters($tableName, $id = null) {
        $this->setSistemaAndRootIndex();

        switch (strtolower($tableName)) {
            case 'cuentas':
                $this->setTitles('Cuentas');
                $this->setEntity('Cuentas');
                if ($id != null) {
                    $entity = $this->em->getRepository($this->Parameters['EntityBundle'] . ':' . $this->Parameters['EntityName'])->find($id);
                }
                $this->addField('Código', '', 'id', true, true, true, MaestrosSimplesFieldTypeEnum::ftInteger, 8, (isset($entity) ? $entity->getId(): '<Nuevo>'), '', false);
                $this->addField('Nombre', '', 'nombre', false, true, false, MaestrosSimplesFieldTypeEnum::ftString, 50, (isset($entity) ? $entity->getNombre(): ''));
                $this->addField('Icono', '', 'icon', false, true, false, MaestrosSimplesFieldTypeEnum::ftString, 100, (isset($entity) ? $entity->getIcon(): ''));

                $this->setOrderBy('a.id ASC');
                $this->addFiltro('Nombre', '', 'nombre', 'a', null, true, ListadosTypesFilterEnum::tLike);
                $this->addFiltro('Icono', '', 'icon', 'a', null, true, ListadosTypesFilterEnum::tLike);

                $this->addForeignTable('Transacciones', 'idCuentaOrigen');
                $this->addForeignTable('Transacciones', 'idCuentaDestino');

                $Acceso = $this->seguridad->verificarPermisoOperacion('FINANSYS', $this->Sistema);
                if ($Acceso=="true") {
                    $lbShow = true;
                } else {
                    $lbShow = false;
                }
                $lbCreate = $lbShow;
                $lbUpdate = $lbShow;
                $lbDelete = $lbShow;
                $this->setSecurityStatusProfile($lbShow, $lbCreate, $lbUpdate, $lbDelete);
                break;
            case 'categorias_gastos':
                $this->setTitles('Categorias de Gastos');
                $this->setEntity('Categorias');
                if ($id != null) {
                    $entity = $this->em->getRepository($this->Parameters['EntityBundle'] . ':' . $this->Parameters['EntityName'])->find($id);
                }
                $this->addField('Código', '', 'id', true, true, true, MaestrosSimplesFieldTypeEnum::ftInteger, 8, (isset($entity) ? $entity->getId(): '<Nuevo>'), '', false);
                $this->addField('Nombre', '', 'nombre', false, true, false, MaestrosSimplesFieldTypeEnum::ftString, 50, (isset($entity) ? $entity->getNombre(): ''));
                $this->addField('Tipo', '', 'idtipo', false, true, false, MaestrosSimplesFieldTypeEnum::ftInteger, 8, ConstantesEnum::idTipoGasto, '', false, false);

                $this->setWhere('a.idtipo=' . ConstantesEnum::idTipoGasto);
                $this->setOrderBy('a.id ASC');
                $this->addFiltro('Nombre', '', 'nombre', 'a', null, true, ListadosTypesFilterEnum::tLike);

                $this->addForeignTable('Transacciones', 'idCategoria');

                $Acceso = $this->seguridad->verificarPermisoOperacion('FINANSYS', $this->Sistema);
                if ($Acceso=="true") {
                    $lbShow = true;
                } else {
                    $lbShow = false;
                }
                $lbCreate = $lbShow;
                $lbUpdate = $lbShow;
                $lbDelete = $lbShow;
                $this->setSecurityStatusProfile($lbShow, $lbCreate, $lbUpdate, $lbDelete);
                break;
            case 'categorias_ingresos':
                $this->setTitles('Categorias de Ingreso');
                $this->setEntity('Categorias');
                if ($id != null) {
                    $entity = $this->em->getRepository($this->Parameters['EntityBundle'] . ':' . $this->Parameters['EntityName'])->find($id);
                }
                $this->addField('Código', '', 'id', true, true, true, MaestrosSimplesFieldTypeEnum::ftInteger, 8, (isset($entity) ? $entity->getId(): '<Nuevo>'), '', false);
                $this->addField('Nombre', '', 'nombre', false, true, false, MaestrosSimplesFieldTypeEnum::ftString, 50, (isset($entity) ? $entity->getNombre(): ''));
                $this->addField('Tipo', '', 'idtipo', false, true, false, MaestrosSimplesFieldTypeEnum::ftInteger, 8, ConstantesEnum::idTipoIngreso, '', false, false);

                $this->setWhere('a.idtipo=' . ConstantesEnum::idTipoIngreso);
                $this->setOrderBy('a.id ASC');
                $this->addFiltro('Nombre', '', 'nombre', 'a', null, true, ListadosTypesFilterEnum::tLike);

                $this->addForeignTable('Transacciones', 'idCategoria');

                $Acceso = $this->seguridad->verificarPermisoOperacion('FINANSYS', $this->Sistema);
                if ($Acceso=="true") {
                    $lbShow = true;
                } else {
                    $lbShow = false;
                }
                $lbCreate = $lbShow;
                $lbUpdate = $lbShow;
                $lbDelete = $lbShow;
                $this->setSecurityStatusProfile($lbShow, $lbCreate, $lbUpdate, $lbDelete);
                break;
            case 'categorias_transferencias':
                $this->setTitles('Categorias de Transferencias');
                $this->setEntity('Categorias');
                if ($id != null) {
                    $entity = $this->em->getRepository($this->Parameters['EntityBundle'] . ':' . $this->Parameters['EntityName'])->find($id);
                }
                $this->addField('Código', '', 'id', true, true, true, MaestrosSimplesFieldTypeEnum::ftInteger, 8, (isset($entity) ? $entity->getId(): '<Nuevo>'), '', false);
                $this->addField('Nombre', '', 'nombre', false, true, false, MaestrosSimplesFieldTypeEnum::ftString, 50, (isset($entity) ? $entity->getNombre(): ''));
                $this->addField('Tipo', '', 'idtipo', false, true, false, MaestrosSimplesFieldTypeEnum::ftInteger, 8, ConstantesEnum::idTipoTransferencia, '', false, false);

                $this->setWhere('a.idtipo=' . ConstantesEnum::idTipoTransferencia);
                $this->setOrderBy('a.id ASC');
                $this->addFiltro('Nombre', '', 'nombre', 'a', null, true, ListadosTypesFilterEnum::tLike);

                $this->addForeignTable('Transacciones', 'idCategoria');

                $Acceso = $this->seguridad->verificarPermisoOperacion('FINANSYS', $this->Sistema);
                if ($Acceso=="true") {
                    $lbShow = true;
                } else {
                    $lbShow = false;
                }
                $lbCreate = $lbShow;
                $lbUpdate = $lbShow;
                $lbDelete = $lbShow;
                $this->setSecurityStatusProfile($lbShow, $lbCreate, $lbUpdate, $lbDelete);
                break;
            case 'transacciones':
                $this->setTitles('Cuentas');
                $this->setEntity('Cuentas');
                if ($id != null) {
                    $entity = $this->em->getRepository($this->Parameters['EntityBundle'] . ':' . $this->Parameters['EntityName'])->find($id);
                }
                $this->addField('Código', '', 'id', true, true, true, MaestrosSimplesFieldTypeEnum::ftInteger, 8, (isset($entity) ? $entity->getId(): '<Nuevo>'), '', false);
                $this->addField('Nombre', '', 'nombre', false, true, false, MaestrosSimplesFieldTypeEnum::ftString, 50, (isset($entity) ? $entity->getNombre(): ''));

                $this->setOrderBy('a.id ASC');
                $this->addFiltro('Nombre', '', 'nombre', 'a', null, true, ListadosTypesFilterEnum::tLike);

                $this->addForeignTable('Transacciones', 'idCuentaOrigen');
                $this->addForeignTable('Transacciones', 'idCuentaDestino');

                $Acceso = $this->seguridad->verificarPermisoOperacion('FINANSYS', $this->Sistema);
                if ($Acceso=="true") {
                    $lbShow = true;
                } else {
                    $lbShow = false;
                }
                $lbCreate = $lbShow;
                $lbUpdate = $lbShow;
                $lbDelete = $lbShow;
                $this->setSecurityStatusProfile($lbShow, $lbCreate, $lbUpdate, $lbDelete);
                break;
            default:
                return false;
        }
        return true;
    }

    public function getParameters() {
        return $this->Parameters;
    }

    public function getmodulo() {
        return $this->modulo;
    }

    public function getmoduloAlias() {
        return $this->moduloAlias;
    }

    public function getRootIndex() {
        return $this->RootIndex;
    }

    public function FormAction($Caption, $Path, $Icon, $Security, $StyleClass = '') {
        $this->Parameters['FormAction'] =
            array(
                'Caption' => $Caption,
                'Path' => $Path,
                'Icon' => $Icon,
                'StyleClass' => $StyleClass,
                'Security' => $Security
            );
    }

    public function addActions($Caption, $Path, $Icon, $Security, $StyleClass = '', $CancelButton = false) {
        $this->Parameters['addActions'][] =
            array(
                'Caption' => $Caption,
                'Path' => $Path,
                'Icon' => $Icon,
                'StyleClass' => $StyleClass,
                'Security' => $Security,
                'CancelButton' => $CancelButton
            );
    }

    public function getTransformChars($Value) {
        $array = str_split(strtolower($Value));

        $lsReturn = '';
        $nextUpper = true;
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i] == '_') {
                $nextUpper = true;
                // Omite este carater y pone en mayúsculas para el próximo
            } else {
                if ($nextUpper) {
                    $nextUpper=false;
                    $lsReturn .= strtoupper($array[$i]);
                } else {
                    $lsReturn .= $array[$i];
                }
            }
        }
        return $lsReturn;
    }

    private function getTitleForeignTable($Table) {
        switch (strtolower($Table)) {
            case 'personal':
                return 'Maestro de Personal';
                break;
            case 'intervenciones':
                return 'Atenciones de Enfermería a Incidentes';
                break;
            case 'intervencionesotras':
                return 'Atenciones Generales de Enfermería';
                break;
            case 'inspeccionescabecera':
            case 'inspeccionesdetalle':
                return 'Reporte General';
                break;
            case 'reportedepeligroscabecera':
                return 'Reporte de Peligros / Observación de Seguridad Industrial';
                break;
            case 'reportedeincidentescabecera':
            case 'reportedeincidentespersonas':
                return 'Reporte de Incidentes';
                break;
            case 'higienereportedosimetriaspersonales':
                return 'Reporte de Dosimetrías Personales';
                break;
            case 'higienereportemedicionesambientales':
                return 'Reporte de Mediciones Ambientales';
                break;
            /*
            case 'reportedeacciones':
                return 'Reporte de Acciones';
                break;
            case 'reporteinterBomberos':
                return 'Reporte de Intervenciones de Bomberos';
                break;
            */
            case 'equipos':
            case 'sismar_equipos':
            case 'sismareste_equipos':
            case 'me_equipos':
                return 'Maestro de Equipos';
                break;
            case 'sismar_plantillas':
            case 'sismareste_plantillas':
            case 'me_plantillas':
                return 'Maestro de Trabajos';
                break;
            case 'controldoc_documentos':
            case 'medioamb_documentos':
                return 'Maestro de Documentos Exigidos';
                break;
            case 'controldoc_empresas':
                /*case 'medioamb_empresas':*/
                return 'Maestro de Empresas';
                break;
            case 'controldoc_licitaciones':
            /*case 'medioamb_licitaciones':*/
                return 'Maestro de Licitaciones';
                break;
            default:
                return $Table;
                break;
        }
    }

    private function setSistemaAndRootIndex() {
        $this->modulo = (isset($_REQUEST['modulo']) ? $_REQUEST['modulo'] : '');
        if ($this->modulo == '') {
            $this->modulo = $this->container->get('request')->get('modulo');
        }
        $this->moduloAlias = (isset($_REQUEST['moduloAlias']) ? $_REQUEST['moduloAlias'] : '');
        if ($this->moduloAlias == '') {
            $this->moduloAlias = $this->container->get('request')->get('moduloAlias');
        }
        if (strval($this->moduloAlias) == '') {
            $this->moduloAlias = $this->modulo;
        }

        if (ConstantesEnum::$Modulos[strtolower($this->moduloAlias)]) {
            $this->Sistema = ConstantesEnum::$Modulos[strtolower($this->moduloAlias)]['Sistema'];
            $this->RootIndex = ConstantesEnum::$Modulos[strtolower($this->moduloAlias)]['RootIndex'];
        } else {
            $this->Sistema = 'ZZ';
            $this->RootIndex = 'fc_index';
        }
        return $this->Sistema;
    }

    private function getFieldPrimaryKey() {
        $lsField = '';
        foreach ($this->Parameters['Fields'] as $field) {
            if ($field['PrimaryKey']==true) {
                $lsField .= $field['Name'];
                break;
            }
        }
        return $lsField;
    }

    private function setTitles($Title) {
        $this->Parameters['Title'] = $Title;
    }

    private function setSecurityStatusProfile($Show = false, $Create = false, $Update = false, $Delete = false) {
        $this->Parameters['SecurityShow'] = $Show;
        $this->Parameters['SecurityCreate'] = $Create;
        $this->Parameters['SecurityUpdate'] = $Update;
        $this->Parameters['SecurityDelete'] = $Delete;
    }

    private function addField($Caption, $PlaceHolder, $Name, $PrimaryKey = false, $Required = true, $AutoIncrement = false, $Type = MaestrosSimplesFieldTypeEnum::ftString, $MaxLength =50, $Value = null, $ListValues = '', $ColVisible = true, $FieldVisible = true, $TransformChars = true, $FilePath = 'upload', $FileType = FileTypesEnum::Imagenes) {
        $this->Parameters['Fields'][] =
            array(
                'Caption' => $Caption,
                'PlaceHolder' => $PlaceHolder,
                'Name' => ($TransformChars ? strtolower($Name) : $Name),
                'PrimaryKey' => $PrimaryKey,
                'Required' => $Required,
                'AutoIncrement' => $AutoIncrement,
                'Type' => $Type,
                'MaxLength' => $MaxLength,
                'Value' => $Value,
                'ListValues' => $ListValues,
                'ColVisible'=>$ColVisible,
                'Visible' => $FieldVisible,
                'FilePath' => $FilePath,
                'FileType' => $FileType
            );
    }

    private function setOrderBy($Value) {
        $this->Parameters['OrderBy'] = $Value;
    }

    private function setIDRegistrosNoModificables($Value = array()) {
        $this->Parameters['IDRegistrosNoModificables'] = $Value;
    }

    private function setWhere($Value) {
        $this->Parameters['Where'] = $Value;
    }

    private function addFiltro($Caption, $PlaceHolder, $Name, $EntityAlias, $EntityLoaded = null, $AutoPost = false, $TypeFilter = ListadosTypesFilterEnum::tUnique) {
        $this->Parameters['Filtros'][] =
            array(
                'Caption' => $Caption,
                'PlaceHolder' => $PlaceHolder,
                'Name' => $Name,
                'EntityAlias' => $EntityAlias,
                'EntityLoaded' => $EntityLoaded,
                'AutoPost' => $AutoPost,
                'TypeFilter' => $TypeFilter
            );
    }

    private function setEntity($Name, $Alias = 'a', $TransformChars = true) {
        switch (strtolower($this->container->getParameter('database_name'))) {
            case 'sistemaseg':
            case 'sistemaism':
            case 'contratistas':
            default:
                $Bundle = 'FCMainBundle';
                break;
        }
        $lsName = ($TransformChars ? $this->getTransformChars($Name) : $Name);
        $this->Parameters['EntityBundle'] = $Bundle;
        $this->Parameters['EntityName'] = $lsName;
        $this->Parameters['EntityAlias'] = $Alias;
    }

    private function addForeignTable($Table, $Field, $TransformChars = true) {
        switch (strtolower($this->container->getParameter('database_name'))) {
            case 'sistemaseg':
            case 'sistemaism':
            case 'contratistas':
            default:
                $Bundle = 'FCMainBundle';
                break;
        }
        $lsTable = ($TransformChars ? $this->getTransformChars($Table) : $Table);
        $lsField = ($TransformChars ? strtolower($Field) : $Field);
        $this->Parameters['ForeignTable'][] =
            array(
                'Bundle' => $Bundle,
                'Table' => $lsTable,
                'Field' => $lsField,
                'Title' => $this->getTitleForeignTable($Table)
            );
    }
}
