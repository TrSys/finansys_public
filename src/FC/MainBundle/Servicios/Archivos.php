<?php

namespace FC\MainBundle\Servicios;

use FC\MainBundle\Constantes\FileTypesEnum;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Archivos extends Controller {
    const Error = '#ERROR#';

    public function UploadFile($poFileNew = null, $psFileOld = null, $pbFileDelete = false, $psPath = 'upload', $paFileTypes = FileTypesEnum::Imagenes) {
        // $poFileNew -> es un objeto de input file
        // $psFileOld -> es un string con el nombre de archivo

        $lsPath = __DIR__ . DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'web'. DIRECTORY_SEPARATOR . $psPath . DIRECTORY_SEPARATOR;

        $lsRetorna = '';
        try {
            if ($poFileNew != null) {
                if (($poFileNew instanceof UploadedFile) && ($poFileNew->getError() == '0')) {
                    if (($poFileNew->getSize() < 20971520)) { //20MB
                        $filename = $poFileNew->getClientOriginalName();
                        $name_array = explode('.', $filename);
                        $file_type = $name_array[sizeof($name_array) - 1];

                        // Si es de tipo imágen valida las extensiones permitidas sino si es de tipo Archivos la 2da. condición permite Todos los Archivos.
                        if (in_array(strtolower($file_type), FileTypesEnum::$Extensiones[$paFileTypes]) || $paFileTypes == FileTypesEnum::Archivos ) {
                            if ($psFileOld != null) {
                                if (file_exists($lsPath . $psFileOld)) {
                                    unlink($lsPath . $psFileOld);
                                }
                            }

                            $NameEncodeFriendly = $this->EncodeFriendly($filename);
                            $filename = $NameEncodeFriendly;

                            $aux = 1;
                            while (file_exists($lsPath . $filename)) {
                                $filename = str_replace(".".$file_type, "", $NameEncodeFriendly) .'_'. $aux .'.'. $file_type;
                                $aux++;
                            }

                            $lfile = $poFileNew->move($lsPath, $filename); //Ver como resolver el tema de permisos
                            //chmod($lsPath . $filename, 0777); //Ver como resolver el tema de permisos
                            //chown($lsPath . $filename, 'lcoscia'); //Ver como resolver el tema de permisos

                            $lsRetorna = $filename;
                        } else {
                            $lsRetorna = $this::Error;
                            $this->get('session')->getFlashBag()->add('warning',  'Tipo de Archivo invalido');
                        }
                    } else {
                        $lsRetorna = $this::Error;
                        $this->get('session')->getFlashBag()->add('warning',  'El peso del Archivo excede el limite permitido');
                    }
                } else {
                    $lsRetorna = $this::Error;
                    $this->get('session')->getFlashBag()->add('warning',  'Error en el Archivo');
                }
            } else {
                if ($pbFileDelete==true) {
                    if ($psFileOld != null) {
                        if (file_exists($lsPath . $psFileOld)) {
                            unlink($lsPath . $psFileOld);
                        }
                    }
                    $lsRetorna = '';
                } else {
                    $lsRetorna = ''.$psFileOld;
                }
            }
        } catch (\Exception $e) {
            $lsRetorna = $this::Error;
            $this->get('session')->getFlashBag()->add('danger', get_class($this) .' - Linea: '. __LINE__ .' - Descripción: '. $e->getMessage());
        }
        return $lsRetorna;
    }

    public function MultiUploadFile($poFiles = null, $psPath = 'upload', $paFileTypes = FileTypesEnum::Imagenes) {
        $lsRetorna = array();
        $i = 0;
        foreach ($poFiles as $File) {
            if($File){
                $lsRetorna[$i] = $this->UploadFile($File, null, false, $psPath, $paFileTypes);
                if ($lsRetorna[$i]==$this::Error) {
                    $lsRetorna = $this::Error;
                    break;
                }
                $i++;
            }
        }
        return $lsRetorna;
    }

    public function DeleteFile($psFile = null, $psPath = 'upload') {
        // $psFile -> es un string con el nombre de archivo

        $lsPath = __DIR__ . DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'web'. DIRECTORY_SEPARATOR . $psPath . DIRECTORY_SEPARATOR;

        $lsRetorna = '';
        try {
            if ($psFile != null) {
                if (file_exists($lsPath . $psFile)) {
                    unlink($lsPath . $psFile);
                    $lsRetorna = $psFile;
                }
            }
        } catch (\Exception $e) {
            $lsRetorna = $this::Error;
            $this->get('session')->getFlashBag()->add('danger', get_class($this) .' - Linea: '. __LINE__ .' - Descripción: '. $e->getMessage());
        }
        return $lsRetorna;
    }

    public function MultiDeleteFile($poFiles = null, $psPath = 'upload') {
        $lsRetorna = array();
        $i = 0;
        foreach ($poFiles as $File) {
            if($File){
                $lsRetorna[$i] = $this->DeleteFile($File, $psPath);
                if ($lsRetorna[$i]==$this::Error) {
                    $lsRetorna = $this::Error;
                    break;
                }
                $i++;
            }
        }
        return $lsRetorna;
    }

    public static function getUploadedFiles(&$em, $fieldId, $fieldName, $Entity, $EntityAlias, $WhereWithAlias, $Path) {
        $i = 0;
        $Retorna = null;
        $archivos = $em->createQuery('SELECT '. $EntityAlias .'.'. $fieldId .' id, '. $EntityAlias .'.'. $fieldName .' archivo FROM '. $Entity .' '. $EntityAlias . ($WhereWithAlias != '' ? ' WHERE ' . $WhereWithAlias : '') .' ORDER BY ' . $EntityAlias .'.'. $fieldId)->getResult();
        foreach ($archivos as $item){
            $Retorna[$i] = array(
                'id' => $item['id'],
                'name' => $item['archivo'],
                'path' => $Path,
                'extension' => substr($item['archivo'], strrpos($item['archivo'], '.')+1),
            );
            $i++;
        };
        return $Retorna;
    }

    public function getFileName($psFile){
        $filename = '';
        if($psFile){
            $filename = $psFile->getClientOriginalName();
            $NameEncodeFriendly = $this->EncodeFriendly($filename);
            $filename = $NameEncodeFriendly;
        }
        return $filename;
    }

    private function EncodeFriendly($value) {
        $value = trim($value);
        $value = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä', 'é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë', 'í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î', 'ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô', 'ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü', 'ñ', 'Ñ', 'ç', 'Ç'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A', 'e', 'e', 'e', 'e', 'E', 'E', 'E', 'E', 'i', 'i', 'i', 'i', 'I', 'I', 'I', 'I', 'o', 'o', 'o', 'o', 'O', 'O', 'O', 'O', 'u', 'u', 'u', 'u', 'U', 'U', 'U', 'U', 'n', 'N', 'c', 'C'),
            $value
        );
        $value = str_replace(
            array("\\", "¨", "º", "~","#", "@", "|", "!", "\"","·", "$", "%", "&", "/","(", ")", "?", "'", "¡","¿", "[", "^", "`", "]","+", "}", "{", "¨", "´",">", "<", ";", ",", ":", " "),
            '_',
            $value
        );
        return $value;
    }
}