<?php

namespace FC\FinansysBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;

class DefaultController extends Controller {
	public function indexAction(Request $request) {
        if (! $this->get('seguridad')->getUsuario()) {
            return $this->redirect($this->generateUrl('fc_login'));
        }

        return $this->redirect($this->generateUrl('transacciones_seleccioncuenta'));
        /*
        return $this->render('FCFinansysBundle:Default:index.html.twig');
        */
    }

    public function renderMenuAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $cuentas = $em->createQuery('SELECT a.id, a.nombre, a.icon FROM FCMainBundle:Cuentas a ORDER BY a.id')->getResult();
        return $this->render('FCFinansysBundle:Default:menudinamico.html.twig', array(
            'cuentas' => $cuentas,
        ));
    }
}
