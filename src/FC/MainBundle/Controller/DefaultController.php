<?php

namespace FC\MainBundle\Controller;

use FC\MainBundle\Constantes\ConstantesEnum;
use FC\MainBundle\Servicios\Seguridad;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;

class DefaultController extends Controller {
	public function loginAction(Request $request) {
		// INTEGRACIÓN DE SEGURIDAD IIS
		/*
		if (strtoupper($this->container->get('twig')->getGlobals()['AppSecurity']) == 'IIS') {
			echo 'GET_CURRENT_USER: '. get_current_user().'<br>';
			echo 'LOGON_USER: ' . $_SERVER['LOGON_USER'].'<br>';
			echo 'AUTH_USER: ' . $_SERVER['AUTH_USER'].'<br>';

			$params['usuario'] = get_current_user();
			$params['clave'] = '';
			$params['pathOrigen'] = '';
			die();
		}
		*/

		$params = $request->request->all();
		
		if (! empty($params)) {
            $usuarioPost = trim($params['usuario']);
			$clavePost = strtolower(trim($params['clave']));
            $pathOrigen = trim(isset($params['pathOrigen']) ? $params['pathOrigen'] : '');

			$result = $this->get('seguridad')->validarUsuario($usuarioPost, $clavePost);
			if ($result) {
				if ($pathOrigen != '') {
					return $this->redirect($request->getUriForPath($pathOrigen));
				}
				return $this->redirect($this->generateUrl('fc_index'));
			}

			return $this->render('FCMainBundle:Default:login.html.twig', array(
					'error' => '1'
			));
		}
		if ($this->get('seguridad')->getUsuario()) {
			return $this->redirect($this->generateUrl('fc_index'));
		}
		return $this->render('FCMainBundle:Default:login.html.twig');
	}

	public function logoutAction(Request $request) {
		$this->get('seguridad')->ClearSession();
		return $this->redirect($this->generateUrl('fc_login'));
	}

	public function indexAction(Request $request) {
        if (! $this->get('seguridad')->getUsuario()) {
            return $this->redirect($this->generateUrl('fc_login'));
        }

        $this->get('Auditoria')->RegistrarAuditoria('Start', 'indexAction');

        $modulos = null;
        $bundles = $this->container->getParameter('kernel.bundles');
        foreach ($bundles as $bundleName => $bundleClass) {
            $refClass = new \ReflectionClass($bundleClass);
            if ($refClass->getConstant('MenuTitle')) {
                $result = $this->get('seguridad')->getValueOfEntities('CASE WHEN p.perfil>0 THEN 1 ELSE 0 END', 'FCMainBundle:Seguridadusuariossistemasperfil p', "p.usuario='". $this->get('seguridad')->getUsuario() ."' AND p.sistema='". $refClass->getConstant('Sistema') ."'", '', 0);
                $modulos[] = array(
                    'MenuTitle' => $refClass->getConstant('MenuTitle'),
                    'MenuIcon' => $refClass->getConstant('MenuIcon'),
                    'MenuPath' => $refClass->getConstant('MenuPath'),
                    'MenuColor' => $refClass->getConstant('MenuColor'),
                    'MenuEnabled' => ($result != 0 ? true : false),
                );
            }
        }

        return $this->render('FCMainBundle:Default:index.html.twig', array(
                'modulos' => $modulos
		));
	}
	public function permisoInvalidoAction() {
		return $this->render('FCMainBundle:Default:permiso_invalido.html.twig');
	}

	public function resetpassAction(Request $request) {
		$params = $request->request->all();
		
		if (! empty($params)) {
            $usuarioPost = trim($params['usuario']);
			$clavePost = strtolower(trim($params['clave']));
			$nuevaclavePost = strtolower(trim($params['nueva']));

            if ($this->get('seguridad')->cambiarClaveUsuario($usuarioPost, $clavePost, $nuevaclavePost) == true) {
				return $this->redirect($this->generateUrl('fc_login'));
			}
			return $this->redirect($this->generateUrl('fc_login'));
		}
		if ($this->get('seguridad')->getUsuario()) {
			return $this->redirect($this->generateUrl('fc_index'));
		}
		return $this->redirect($this->generateUrl('fc_login'));
	}
}
