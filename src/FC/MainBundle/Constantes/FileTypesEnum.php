<?php

namespace FC\MainBundle\Constantes;

class FileTypesEnum {
    const Imagenes = 0;
    const Archivos = 1;

    static $Extensiones = array(
        /* Imagenes */ array('jpg', 'png', 'gif'),
        /* Archivos */ array('*') //Ahora permite todos los archivos.
	    );
}
