<?php

namespace FC\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use FC\MainBundle\Entity\Recomendaciones;
use FC\MainBundle\Servicios\DocumentosNumeradores;
use FC\MainBundle\Servicios\Notificaciones;

class RecomendacionesController extends Controller {
	public function renderRecomendacionesAction($idDocumento, $idReporte, $idObservacion = 0, $ajax = true, Request $request)
	{				
		$em              = $this->getDoctrine()->getManager();
		$tratamientosSAP = $em->getRepository('FCMainBundle:Recomendacionestratamientosap')->findBy(array(), array('idtratamientosap' => 'ASC'));;
		$grupos = $em->getRepository('FCMainBundle:Recomendacionesgrupos')->findBy(array(), array('descripcion' => 'ASC'));
		$recomendaciones=NULL;
        $recomendacionesdb = $em->getRepository('FCMainBundle:Recomendaciones')->findBy( array( 'iddocumento'=> $idDocumento,'idobservacion'=> $idObservacion,'idreporte' => $idReporte));
		$i=0;
		if ($recomendacionesdb) {
	        foreach($recomendacionesdb as $recomendaciondb) {
                $objetoPersonal = $em->getRepository('FCMainBundle:Personal')->findByCedula($recomendaciondb->getIdresponsable());
                if($objetoPersonal){
                    $personal                                 = $objetoPersonal[0];
                    $recomendaciones[$i]['responsableApenom'] = $personal->getApenom();
                    $recomendaciones[$i]['responsableEmail']  = $personal->getEmail();
                }else{
                    $recomendaciones[$i]['responsableApenom'] = "";
                    $recomendaciones[$i]['responsableEmail']  = "";
                }

                $plazo="";
                if($recomendaciondb->getPlazo()){
                	$plazo = $recomendaciondb->getPlazo()->format('Y-m-d');
                }
				$arr = array(
						'id'        			 => $recomendaciondb->getIdrecomendacion(),		
						'rec_iddocumento'        => $recomendaciondb->getIddocumento(),		
						'rec_idreporte'          => $recomendaciondb->getIdreporte(),		
						'rec_idobservacion'      => $recomendaciondb->getIdobservacion(),		
						'rec_idgrupo'            => $recomendaciondb->getIdgrupo(),							
						'rec_nombre_responsable' => $personal->getApenom(), 
						'rec_mail'               => $personal->getEmail(), 
						'rec_nro_responsable'    => $recomendaciondb->getIdresponsable(), 
						'rec_userlog'            => $recomendaciondb->getUserlog(), 
						'fechaProc'              => $recomendaciondb->getFechaProc(),					
						'rec_recomendacion'      => $recomendaciondb->getRecomendacion(),
						'rec_plazo'              => $plazo,
						'rec_tratamientoSAP'     => intval($recomendaciondb->getIdtratamientosap()),
						'rec_iddocrela'          => $recomendaciondb->getIddocrela(),
						'rec_notificacion'       => 0);
				$recomendaciones[$i] = json_encode($arr);
				$i++;
			}
        }

		return $this->render('FCMainBundle:Default:recomendaciones.html.twig', array(
			'uniqid'          => uniqid(),
			'idreporte'       => $idReporte,
			'iddocumento'     => $idDocumento,			
			'idobservacion'   => $idObservacion,						
			'recomendaciones' => $recomendaciones,
			'ajax'			  => $ajax,
			'userlog'         => $this->get('seguridad')->getUsuario(),
			'tratamientosSAP' => $tratamientosSAP,
			'grupos' => $grupos,
		));
	}

	public function agregarRecomendacionesAction($recomendaciones = null, $idDocumento = null, $idReporte = null, $idObservacion = 0, Request $request)
	{     

		$params = $request->request->all();


		if(isset($params ['idDocumento']) && isset($params ['idReporte'])){
			$recomendaciones = json_decode($params['recomendaciones'],true);
			$idDocumento = intval($params ['idDocumento']);
			$idReporte = intval($params ['idReporte']);
			$idObservacion = intval($params ['idObservacion']);
			$recomendacionesNotif = null;
		}

		if($idDocumento !=null && $idReporte != null){
			$em      = $this->getDoctrine()->getManager();
			$em->getConnection()->prepare("DELETE FROM Recomendaciones WHERE idDocumento = " .$idDocumento. " AND idReporte = ".$idReporte . " AND idObservacion = ".$idObservacion )->execute();
			$dt      = new \DateTime('NOW');

			if($recomendaciones){
		        foreach ($recomendaciones as $item)
		        {
	                $recomendacion = new Recomendaciones();
	                $recomendacion->setIddocumento($idDocumento);
	                $recomendacion->setIdreporte($idReporte);
	                $recomendacion->setIdobservacion($idObservacion);
	                $recomendacion->setIdrecomendacion($item['id']);
	                $recomendacion->setIdgrupo($item['rec_idgrupo']);
	                $recomendacion->setRecomendacion($item['rec_recomendacion']);
	                if($item['rec_plazo'] != null){
	                	$recomendacion->setPlazo(new \DateTime(str_ireplace('/', '-', $item['rec_plazo'])));
	            	}else{
	            		$recomendacion->setPlazo( null);
	            	}
	                $recomendacion->setIdresponsable($item['rec_nro_responsable']);
	                $recomendacion->setIdtratamientosap($item['rec_tratamientoSAP']);
	                $recomendacion->setIddocrela($item['rec_iddocrela']);
	                $recomendacion->setFechaProc($dt);
	                $recomendacion->setUserLog($this->get('seguridad')->getUsuario());
	                $em->persist($recomendacion);
	                $em->flush();
	                if($item['rec_mail'] != "") {
	                    $em->getConnection()->prepare("UPDATE Personal SET Email ='" . $item['rec_mail'] . "' WHERE cedula = " . $item['rec_nro_responsable'] . ";")->execute();
	                }
	                if($item['rec_notificacion'] != 0)
	                {
	                	$recomendacionesNotif[] = $item;
	                }
		        }
		        if ($recomendacionesNotif) {
		        	$notificacion = $this->get('notificaciones');
					$notificacion->gGenerarNotificacionRecomendarionesAResponsables($idDocumento, $idReporte, $recomendacionesNotif, $idObservacion);
		        }
	    	}
    	}
		$response = new Response(json_encode(array("success" => true)));
		return $response;
	}
}
