//jquery para cosas generales
$(document).ready(function(){
	//JS GENERICO PARA VALIDAR FORMs
	$(document).on('click', 'input[type="submit"], button[type="submit"]', function(e) {
		e.preventDefault();
		var error = 0;
		//var msg = 'An Error Has Occured.\n\nRequired Fields missed are :\n';
		var form = $(this).attr('form'); //busca el atributo 'form='
		if(undefined == form){ //si no encuenta el 'form=' busca el formulario mas cercano.
			var formOBJ = $(this).closest('form');
			form = formOBJ.attr('id');
		}
		if(undefined != form){
			$(':input[required]', '#'+form).each(function(){
				if(($(this).val() == '') && ($(this).is(':disabled')==false)){
					//msg += '\n' + $(this).attr('name') + ' Is A Required Field..';
					$(this).css('border','2px solid red');
					if(error == 0){
						$(this).focus();
						var tab = $(this).closest('.tab-pane').attr('id');
						if(undefined != tab) {
							$('a[href="#' + tab + '"]').tab('show');
						}
					}
					error = 1;
				}
			});
			if(error == 1) {
				//alert(msg);
				return false;
			} else {
				if($(this).hasClass('validacionextendida')){
					validacionextendida(); //esta funcion esta definida en la respectiva vista para sumar condiciones al submit y realizarlo desde la plantilla
				}else{
					$('#'+form).submit();
				}
				return true;
			}
		}

	});
	//JS GENERICO PARA BORRAR
	/*
	$(document).on('click','.confirm-js',function(){ //lo hago de esta manera por si hay algun elemento generado por ajax '$(document)'
        return confirm('Esta seguro que desea realizar esta acción?');
    });
	*/

	$(document).on('click', '.confirm-js', function(e){
		e.preventDefault();
		if ($('#ConfirmJSModal').length <= 0 ) {
			$('body').append("<div id='ConfirmJSModal' class='modal fade' role='dialog'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'><button type='button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title'>Confirmación de Eliminación</h4></div><div class='modal-body'><p><label>Está seguro que desea eliminar el registro?</label></p></div><div class='modal-footer'><a class='btn btn-success js-btn-ConfirmJS'>Aceptar</a><a class='btn btn-default' data-dismiss='modal'>Cancelar</a></div></div></div></div>");
		}

		$('.js-btn-ConfirmJS').attr('href', $(this).attr('href'));
		$('#ConfirmJSModal').modal('show');
	});

	$('#ConfirmJSModal').on('shown.bs.modal', function (e) {
		$('.js-btn-ConfirmJS').focus();
	});

	$('.js-btn-ConfirmJS').click(function () {
		$('#ConfirmJSModal').modal('hide');
	});

	navigator.browserInfo= (function(){
	    var ua= navigator.userAgent, tem,
	    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	    if(/trident/i.test(M[1])){
	        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
	        return 'IE '+(tem[1] || '');
	    }
	    if(M[1]=== 'Chrome'){
	        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
	        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
	    }
	    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
	    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
	    return M.join(' ');
	})();

	brwsr = navigator.browserInfo.toLowerCase();
	if (brwsr.indexOf('chrome') == -1){
		$('#browserAlert').show();
	}

});