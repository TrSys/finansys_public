<?php

namespace FC\MainBundle\Servicios;

class DocumentosNumeradores {
    /*Incidentes e Inspecciones*/
    const DocRepIrr = 1;
    const DocRepAcc = 2;
    const DocRepInc = 3;
    const DocReporteDeInspeccion = 4;
    const DocReporteDePeligros = 5;
    const DocReporteDeIncidentes = 6;
    const DocReporteDeIncidentesClasificacion = 7;

    /*Higiene*/
    const DocReporteDeDosimetriasPersonales = 61;
    const DocReporteDeMedicionesAmbientales = 62;

    /*Capacitación y Asesoramiento*/
    const DocReporteDeAsesoramiento = 200;

    /*Enfermería*/
    const DocAtenOtra = 11;
    const DocAtenIncidente = 12;

    /*EPP y Vestuario*/
    const DocPedidoMAT = 21;
    const DocPedidoROPA = 22;
    const DocValeMAT = 23;
    const DocValeROPA = 24;
    const DocValeMAT_ENTREGA = 25;
    const DocValeROPA_ENTREGA = 26;

    const DocPreventivoAnual = 41;

    /*Permisos de Trabajo*/
    const DocCIS = 50;
    const DocSolicitudCIS = 51;

    /*Mensajería*/
    const DocMsgClient = 31;
    const DocMsgServer = 32;

    /*Seguridad y Auditoria*/
    const DocAudi = 100;

    /*Protección contra Incendios*/
    const DocPlanoCambios = 100000;

    const DocReporteInterBomberos = 1001;
    const DocReporteDeMinutas = 1002;
    const DocReporteDeAcciones = 1003;

    const DocSMPlantilla = 10000;
    const DocSMRealizada = 10001;
    const DocSMHistoricoAsignacion = 10002;
    const DocSMRegistraciones = 10003;

    const DocSMPlantillaEste = 10100;
    const DocSMRealizadaEste = 10101;
    const DocSMHistoricoAsignacionEste = 10102;
    const DocSMRegistracionesEste = 10103;

    const DocCDDocumento = 10200;
    const DocCDRealizada = 10201;
    const DocCDHistoricoAsignacion = 10202;
    const DocCDRegistraciones = 10203;

    const DocICPlantilla = 1100;
    const DocICRealizada = 1101;
    const DocICHistoricoAsignacion = 1102;
    const DocICRegistraciones = 1103;
    const DocReporteDeRecomendaciones = 1200;

    const DocReporteClasificadoDeIncendios = 1300;

    /*Contratistas*/
    const DocControlDoc = 70;
}