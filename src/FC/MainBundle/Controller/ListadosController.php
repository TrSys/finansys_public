<?php

namespace FC\MainBundle\Controller;

use FC\MainBundle\Servicios\Listados;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ListadosController extends Controller {
    public function UniversalExportarExcelAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $dl = new Listados($em, $request);
        $Parametros = json_decode($request->request->get('Parametros'), true);
        $dl->setParametros($Parametros);

        // Write the spreadsheet column titles / labels
        $ColumnsTitle = null;
        foreach ($dl->getParametros()['Campos'] as $field) {
            $ColumnsTitle[] = $field['Caption'];
        }
        $ColumnsTitle = array_map("utf8_decode", $ColumnsTitle);

        $result = $em->createQuery($dl->getQuery())->getResult();

        header( 'Content-Type: text/csv' );
        header( 'Content-Disposition: attachment;filename="'. $dl->getParametros()['Title'] .'.xls"');
        $handle = fopen("php://output", 'w');
        fputcsv($handle, $ColumnsTitle, chr(9));

        // Write all the user records to the spreadsheet
        foreach($result as $item) {
            $Row = null;
            foreach ($dl->getParametros()['Campos'] as $field) {
                if (is_object($item[$field['FieldAlias']])==1) {
                    $Row[] = '' . ($item[$field['FieldAlias']] ? str_replace(chr(9), ' ', date_format($item[$field['FieldAlias']], 'd-m-Y')) : '');
                } else {
                    $Row[] = '' . ($item[$field['FieldAlias']] ? str_replace(chr(9), ' ', $item[$field['FieldAlias']]) : '');
                }

            }
            $Row = array_map("utf8_decode", $Row);
            fputcsv($handle, $Row, chr(9));
        }
        fclose($handle);
        die();
    }
}
