<?php
namespace FC\FinansysBundle\Controller;

use FC\MainBundle\Constantes\ConstantesEnum;
use FC\MainBundle\Entity\Transacciones;
use FC\MainBundle\Servicios\Listados;
use FC\MainBundle\Servicios\ListadosTypesFilterEnum;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class TransaccionesController extends Controller {
    public function indexCuentasAction($page = 1, Request $request) {
        $seguridad = $this->get('seguridad')->verificarPermisoOperacion('ACCEDE','FINANSYS');
        if ($seguridad != "true") {
            return $seguridad;
        }

        $em = $this->getDoctrine()->getManager();
        $dl = new Listados($em, $request);
        $dl->setTitle('Cuentas');
        $dl->setEntity('FCMainBundle:Cuentas');
        $dl->setQuery(
            'SELECT a, a.id, a.nombre,
                SUM(
                    CASE WHEN d.idtipo='. ConstantesEnum::idTipoGasto .' THEN -c.importe
                    WHEN d.idtipo='. ConstantesEnum::idTipoIngreso .' THEN c.importe
                    WHEN d.idtipo='. ConstantesEnum::idTipoTransferencia .' THEN
                        CASE WHEN c.idcuentaorigen=a.id THEN -c.importe
                        WHEN c.idcuentadestino=a.id THEN c.importe
                        ELSE 0
                        END
                    ELSE 0
                    END
                ) AS saldo
            FROM FCMainBundle:Cuentas a
                LEFT JOIN FCMainBundle:Transacciones c WITH c.idcuentaorigen=a.id OR c.idcuentadestino=a.id
                LEFT JOIN FCMainBundle:Categorias d WITH c.idcategoria=d.id
            GROUP BY a, a.id, a.nombre
            ');

        $dl->addCampo('Nº', 'id', null, null, null, null, false);
        $dl->addCampo('Cuentas', 'nombre');
        $dl->addCampo('Saldo', 'saldo');

        $dl->setOrderBy('a.id ASC');

        $dl->setPathIndex('transacciones_seleccioncuenta');
        $dl->setPathShow('transacciones', array(array('idcuenta', 'id')));

        return $this->render('FCMainBundle:ListadosSimples:index.html.twig',
            array(
                'Parametros' => $dl->getParametros(),
                'pagination' => $dl->getPagination($this->get('knp_paginator'), $page))
        );
    }

    public function indexAction($idcuenta, $page = 1, Request $request) {
        $seguridad = $this->get('seguridad')->verificarPermisoOperacion('ACCEDE','FINANSYS');
        if ($seguridad != "true") {
            return $seguridad;
        }

        $em = $this->getDoctrine()->getManager();
        $title = $em->getRepository('FCMainBundle:Cuentas')->findOneById($idcuenta)->getNombre();

        $dl = new Listados($em, $request);
        $dl->setTitle($title);
        $dl->setEntity('FCMainBundle:Transacciones');
        $dl->setQuery('
            SELECT
                a, a.id, a.fecha, b.nombre, b.idtipo, a.descripcion, a.importe,
                (
                    SELECT
                        SUM(
                            CASE WHEN d.idtipo='. ConstantesEnum::idTipoGasto .' THEN -c.importe
                            WHEN d.idtipo='. ConstantesEnum::idTipoIngreso .' THEN c.importe
                            WHEN d.idtipo='. ConstantesEnum::idTipoTransferencia .' THEN
                                CASE WHEN c.idcuentaorigen=' . $idcuenta . ' THEN -c.importe
                                WHEN c.idcuentadestino=' . $idcuenta . ' THEN c.importe
                                ELSE 0
                                END
                            ELSE 0
                            END
                        ) AS resultado
                    FROM FCMainBundle:Transacciones c
                        LEFT JOIN FCMainBundle:Categorias d WITH c.idcategoria=d.id
                    WHERE c.fecha<=a.fecha
                        AND (c.idcuentaorigen=' . $idcuenta . ' OR c.idcuentadestino='. $idcuenta . ')
                ) AS saldo
            FROM FCMainBundle:Transacciones a
                LEFT JOIN FCMainBundle:Categorias b WITH a.idcategoria=b.id
            WHERE (a.idcuentaorigen=' . $idcuenta . ' OR a.idcuentadestino='. $idcuenta . ')
            ');

        $dl->addCampo('Código', 'id', null, null, null, null, false);
        $dl->addCampo('Fecha', 'fecha');
        $dl->addCampo('Categoría', 'nombre');//, 'categoria', 'FCMainBundle:Categorias', 'b', 'a.idcategoria=b.id');
        $dl->addCampo('Tipo', 'idtipo', null, null, null, null, false);//, 'idtipo', 'FCMainBundle:Categorias', 'c', 'a.idcategoria=c.id', false);
        $dl->addCampo('Descripción', 'descripcion');
        $dl->addCampo('Importe', 'importe');
        $dl->addCampo('Saldo', 'saldo');

        $dl->setOrderBy('a.fecha DESC');

        $dl->addFiltro('Fecha', 'DD/MM/YYYY', 'fecha', 'a', null, true, ListadosTypesFilterEnum::tFromTo);
        $dl->addFiltro('Categoria', '', 'idcategoria', 'a', $em->createQuery('SELECT a.id AS Id, a.nombre AS Des FROM FCMainBundle:Categorias a ORDER BY a.id')->getResult());
        $dl->addFiltro('Descripción', '', 'descripcion', 'a', null, true, ListadosTypesFilterEnum::tLike);

        $dl->setPathIndex('transacciones', array(array('idcuenta', null, $idcuenta)));
        $dl->setPathShow('transacciones_show', array(array('idcuenta', null, $idcuenta), array('idtipo', 'idtipo'), array('id', 'id')));

        $dl->addActions('Editar', 'transacciones_edit', 'glyphicon-pencil', true, 'btn btn-warning', array(array('idcuenta', null, $idcuenta), array('idtipo', 'idtipo'), array('id', 'id')));
        $dl->addActions('Eliminar', 'transacciones_delete', 'glyphicon-remove', true, 'btn btn-danger btn-delete', array(array('idcuenta', null, $idcuenta), array('idtipo', 'idtipo'), array('id', 'id')));

        $dl->addHeaderActions('Gasto', 'transacciones_new', 'glyphicon-minus-sign', true, 'btn2 btn2-danger', array(array('idcuenta', null, $idcuenta), array('idtipo', null, ConstantesEnum::idTipoGasto)));
        $dl->addHeaderActions('Ingreso', 'transacciones_new', 'glyphicon-plus-sign', true, 'btn2 btn2-success', array(array('idcuenta', null, $idcuenta), array('idtipo', null, ConstantesEnum::idTipoIngreso)));
        $dl->addHeaderActions('Transferencia', 'transacciones_new', 'glyphicon-transfer', true, 'btn2 btn2-primary', array(array('idcuenta', null, $idcuenta), array('idtipo', null, ConstantesEnum::idTipoTransferencia)));

        return $this->render('FCMainBundle:ListadosSimples:index.html.twig',
            array(
                'Parametros' => $dl->getParametros(),
                'pagination' => $dl->getPagination($this->get('knp_paginator'), $page))
        );
    }

    public function newEditAction($idcuenta, $idtipo, $id=null, Request $request) {
        $seguridad = $this->get('seguridad')->verificarPermisoOperacion('ACCEDE','FINANSYS');
        if ($seguridad != "true") {
            return $seguridad;
        }

        $em = $this->getDoctrine()->getManager();
        $usuario = $this->get('seguridad')->getUsuario();
        $entity=null;

        //DATOS QUE  NECESITO SIEMPRE
        $cuentas = $em->getRepository('FCMainBundle:Cuentas')->findAll();
        $categorias = $em->getRepository('FCMainBundle:Categorias')->findBy(array('idtipo' => $idtipo));

        if ($id != null) {
            $entity = $em->getRepository('FCMainBundle:Transacciones')->findOneById($id);
        } else {
            $id = 0;
        }

        return $this->render('FCFinansysBundle:Transacciones:form.html.twig', array(
            'entity'        => $entity,
            'usuario'       => $usuario,
            'categorias'    => $categorias,
            'cuentas'       => $cuentas,
            'idcuenta'      => $idcuenta,
            'idtipo'        => $idtipo,
            'id'            => $id)
        );
    }

    public function createUpdateAction($idcuenta, $idtipo, Request $request) {
        $seguridad = $this->get('seguridad')->verificarPermisoOperacion('ACCEDE','FINANSYS');
        if ($seguridad != "true") {
            return $seguridad;
        }

        $em = $this->getDoctrine()->getManager();
        $params = $request->request->all();
        $id = (isset($params["id"]) ? intval($params["id"]) : null);
        $fecha = (isset($params["fecha"]) || isset($params["hora"]) ?  new \DateTime($params["fecha"].$params["hora"]) : null) ;
        $idcuentaorigen = (isset($params["idcuentaorigen"]) ? trim($params["idcuentaorigen"]) : $idcuenta);
        $idcuentadestino = (isset($params["idcuentadestino"]) ? trim($params["idcuentadestino"]) : null);
        $idcategoria = (isset($params["idcategoria"]) ? trim($params["idcategoria"]) : null);
        $descripcion = (isset($params["descripcion"]) ? trim($params["descripcion"]) : null);
        if ($descripcion==null or trim($descripcion)=="") {
            $descripcion = $em->getRepository('FCMainBundle:Categorias')->findOneById($idcategoria)->getNombre();
            $descripcion.= ($idcuentadestino!=null ? ' a ' . $em->getRepository('FCMainBundle:Cuentas')->findOneById($idcuentadestino)->getNombre() : '');
        }
        $importe = (isset($params["importe"]) ? floatval($params["importe"]) : null);

        if ($id) {
            $transaccion = $em->getRepository('FCMainBundle:Transacciones')->findOneById($id);
            $leyenda = "editó";
        } else {
            $id = $this->generarId();
            $transaccion = new Transacciones();
            $transaccion->setId($id);
            $leyenda = "creó";
        }
        if ($transaccion) {
            $transaccion->setFecha($fecha);
            $transaccion->setIdcuentaorigen($idcuentaorigen);
            $transaccion->setIdcuentadestino($idcuentadestino);
            $transaccion->setIdcategoria($idcategoria);
            $transaccion->setDescripcion($descripcion);
            $transaccion->setImporte($importe);
            $em->persist($transaccion);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'La Operación N° '.$id.' se '. $leyenda .' con éxito.');
        } else {
            $this->get('session')->getFlashBag()->add('danger', 'Hubo un error y no se '.$leyenda.' la Operación N° '.$id);
        }
        return $this->redirect($this->generateUrl('transacciones', array('idcuenta'=> $idcuenta)));
    }

    public function deleteAction($idcuenta, $idtipo, $id) {
        $seguridad = $this->get('seguridad')->verificarPermisoOperacion('ACCEDE','FINANSYS');
        if ($seguridad != "true") {
            return $seguridad;
        }

        if (intval($id) > 0) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FCMainBundle:Transacciones')->find($id);
            if (!$entity) {
                throw $this->createNotFoundException('No se ha encontrado la Operación.');
            }
            $em->remove($entity);
            $em->flush();
        }
        $this->get('session')->getFlashBag()->add('notice', 'La transacción se eliminó con éxito.');
        return $this->redirect($this->generateUrl('transacciones', array('idcuenta'=> $idcuenta)));
    }

    private function generarId(){
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery('SELECT MAX(a.id) AS MaxID FROM FCMainBundle:Transacciones a')->getResult();
        return ( int ) ($result[0]['MaxID'])+1;
    }
}