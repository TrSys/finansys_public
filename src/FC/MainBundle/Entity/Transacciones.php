<?php

namespace FC\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transacciones
 *
 * @ORM\Table(name="Transacciones")
 * @ORM\Entity
 */
class Transacciones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var integer
     *
     * @ORM\Column(name="idCuentaOrigen", type="integer", nullable=false)
     */
    private $idcuentaorigen;

    /**
     * @var integer
     *
     * @ORM\Column(name="idCuentaDestino", type="integer", nullable=true)
     */
    private $idcuentadestino;

    /**
     * @var integer
     *
     * @ORM\Column(name="idCategoria", type="integer", nullable=false)
     */
    private $idcategoria;

    /**
     * @var string
     *
     * @ORM\Column(name="Descripcion", type="string", length=50, nullable=true)
     */
    private $descripcion;

    /**
     * @var decimal
     *
     * @ORM\Column(name="Importe", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $importe;


    /**
     * Set id
     *
     * @param integer $id
     * @return Transacciones
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Transacciones
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set idcuentaorigen
     *
     * @param integer $idcuentaorigen
     * @return Transacciones
     */
    public function setIdcuentaorigen($idcuentaorigen)
    {
        $this->idcuentaorigen = $idcuentaorigen;

        return $this;
    }

    /**
     * Get idcuentaorigen
     *
     * @return integer
     */
    public function getIdcuentaorigen()
    {
        return $this->idcuentaorigen;
    }

    /**
     * Set idcuentadestino
     *
     * @param integer $idcuentadestino
     * @return Transacciones
     */
    public function setIdcuentadestino($idcuentadestino)
    {
        $this->idcuentadestino = $idcuentadestino;

        return $this;
    }

    /**
     * Get idcuentadestino
     *
     * @return integer
     */
    public function getIdcuentadestino()
    {
        return $this->idcuentadestino;
    }

    /**
     * Set idcategoria
     *
     * @param integer $idcategoria
     * @return Transacciones
     */
    public function setIdcategoria($idcategoria)
    {
        $this->idcategoria = $idcategoria;

        return $this;
    }

    /**
     * Get idcategoria
     *
     * @return integer
     */
    public function getIdcategoria()
    {
        return $this->idcategoria;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Transacciones
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set importe
     *
     * @param decimal $importe
     * @return Transacciones
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return decimal
     */
    public function getImporte()
    {
        return $this->importe;
    }
}