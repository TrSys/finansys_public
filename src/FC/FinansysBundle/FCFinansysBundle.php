<?php

namespace FC\FinansysBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FCFinansysBundle extends Bundle {
    const MenuTitle="FinanSys";
    const MenuIcon="fa fa-th-large";
    const MenuPath="finansys";
    const MenuColor="background-color: #34AADC; color: #FFF;";
}
