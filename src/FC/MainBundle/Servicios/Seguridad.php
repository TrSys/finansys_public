<?php

namespace FC\MainBundle\Servicios;

use Doctrine\ORM\EntityManager;
use FC\MainBundle\Constantes\RolesEnum;
use FC\MainBundle\Constantes\ConstantesEnum;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\Routing\Router;

class Seguridad {
    private $em;
    private $request;
    private $router;
    private $session;
    private $AppSecurity;
    private $AppName;

    const LDAP_host = 'extranet.ancap.com.uy';
    const LDAP_port = 389; // Conecta al servidor ldap (LDAP: 389, LDAPS: 636, LDAP Global: 3268)
    const LDAP_dn = "ou=Gestion_contratistas,ou=Aplicaciones,dc=extranet,dc=ancap,dc=com,dc=uy";
    const LDAP_rdn  = 'gest_contr_test@extranet.ancap.com.uy';
    const LDAP_pass = 'c0ntr4t15t45';

    public function __construct(EntityManager $entityManager, Request $request, Router $router, ContainerInterface $container) {
        $this->em = $entityManager;
        $this->request = $request;
        $this->router = $router;
        $this->session = $request->getSession();
        $this->AppSecurity = $container->get('twig')->getGlobals()['AppSecurity'];
        $this->AppName = $container->get('twig')->getGlobals()['AppName'];
    }

    private function validarpermiso($proceso,$sistema) {
        if ($this->getUsuario() == 'ADMIN') {
			return true;
		}
		$dql = "SELECT
                    1 AS Retorna
                FROM FCMainBundle:Seguridadusuariossistemasperfil a
                    LEFT JOIN FCMainBundle:Seguridadperfilesusuario b WITH a.sistema=b.sistema AND a.perfil=b.perfil
                WHERE
                    a.usuario='". $this->getUsuario() ."'
                    AND b.sistema='". $sistema ."'
                    AND b.procesocodigo='". $proceso ."'
                    AND b.funcion1='SI'";
        $result = $this->em->createQuery($dql)->getResult();
        if ($result) {
            return true;
        }
        return false;
    }

    public function verificarPermisoOperacion($proceso = NULL,$sistema = NULL) {
        $usuario = $this->getUsuario();
        // Si esta seteado todos los parametros verifica que este permitido
        if (isset ($usuario) && isset ($sistema) && isset ($proceso)) {
            $result = $this->validarpermiso($proceso, $sistema);
            if ($result) {
                return true;
            }
            return new RedirectResponse($this->router->generate('fc_permiso_invalido', array(
                'modulo' => $this->getModulo($sistema),
                'moduloAlias' => $this->getModuloAlias($sistema)
            )));
        }
        // Si solo se consulta por el usuario logueado sin codigo, permite acceder
        if ($usuario) {
            return true;
        }

        $path = array();
        if ($this->request->getBaseUrl() != $this->request->getRequestUri()) {
            $aux = str_replace($this->request->getBaseUrl(), "", $this->request->getRequestUri());
            $path = array('pathOrigen' => $aux);
        }

        // Si no tiene ninguno seteado, muestra el login
        return new RedirectResponse($this->router->generate('fc_login', $path));
    }

    public function validarUsuario($usuario, $clave) {
        if ($usuario == 'ADMIN' && $clave == 'catyas00') {
			return $this->StartSession(array('usuario' => $usuario, 'nombre' => 'Administrador', 'cedula' => 0));
		}
        if (isset($usuario) && isset($clave)) {
            switch (strtoupper($this->AppSecurity)) {
                case 'SQL':
                    return $this->StartSession($this->validarUsuarioSQL($usuario, $clave));
                    break;
                case 'LDAP':
                    return $this->StartSession($this->validarUsuarioLDAP($usuario, $clave));
                    break;
                case 'IIS':
                    return $this->StartSession($this->validarUsuarioLDAP($usuario, $clave));
                    break;
            }
        }
        return false;
    }

    public function ClearSession() {
        $this->session->remove($this->AppName .'_'. 'usuario');
        $this->session->remove($this->AppName .'_'. 'cedula');
        $this->session->remove($this->AppName .'_'. 'nombre');
        //$this->session->clear();
        //$this->session->invalidate();
    }

    private function StartSession($result) {
        if ($result) {
            $usuario = $result['usuario'];
            $nombre = $result['nombre'];
            $cedula = $result['cedula'];

            $storage = new NativeSessionStorage(array(), new NativeFileSessionHandler());
            $this->session = new Session($storage);

            $this->session->start();
            $this->session->set($this->AppName .'_'. 'usuario', $usuario);
            $this->session->set($this->AppName .'_'. 'cedula', $cedula);
            $this->session->set($this->AppName .'_'. 'nombre', $nombre);
        }
        return $result;
    }

    public function getUsuario() {
        return $this->session->get($this->AppName .'_'. 'usuario');
    }

    public function getCedula() {
        return $this->session->get($this->AppName .'_'. 'cedula');
    }

    public function getNombre() {
        return $this->session->get($this->AppName .'_'. 'nombre');
    }

    public function cambiarClaveUsuario($usuario, $clave, $clavenueva) {
        switch (strtoupper($this->AppSecurity)) {
            case 'SQL':
                return $this->cambiarClaveUsuarioSQL($usuario, $clave, $clavenueva);
                break;
            case 'LDAP':
                return $this->cambiarClaveUsuarioLDAP($usuario, $clave, $clavenueva);
                break;
            case 'IIS':
                return $this->cambiarClaveUsuarioLDAP($usuario, $clave, $clavenueva);
                break;
        }
    }

    private function getModulo($sistema) {
        if (ConstantesEnum::$Sistemas[strtoupper($sistema)]) {
            return ConstantesEnum::$Sistemas[strtoupper($sistema)];
        } else {
            return strtolower($sistema);
        }
    }

    private function getModuloAlias($sistema) {
        if (ConstantesEnum::$SistemasAlias[strtoupper($sistema)]) {
            return ConstantesEnum::$SistemasAlias[strtoupper($sistema)];
        } else {
            return strtolower($sistema);
        }
    }

    private function validarUsuarioSQL($usuario, $clave) {
        $dql = "SELECT a.cedula, a.usuario, a.nombre FROM FCMainBundle:Seguridadusuarios a WHERE a.usuario='". $usuario ."' AND a.clave='". $clave ."'";
        $result = $this->em->createQuery($dql)->getResult();
        if ($result) {
            $dql = "SELECT a FROM FCMainBundle:Personal a WHERE a.cedula=". $result[0]['cedula'];
            $resultPersonal = $this->em->createQuery($dql)->getResult();
            if($resultPersonal){
                $result[0]['Personal'] = $resultPersonal[0];
            }else{
                $result[0]['Personal'] = NULL;
            }
            return $result[0];
        }
        return false;
    }

    private function validarUsuarioLDAP($LDAPUser, $LDAPPassword) {
        $bind = null;
        try {
            if ($ds = @ldap_connect($this::LDAP_host, $this::LDAP_port)) { // Conecta al servidor ldap
                ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
                ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);
                if (!($bind = @ldap_bind($ds, $LDAPUser, $LDAPPassword))) { // Esta es la validación sin @host
                    if (!($bind = @ldap_bind($ds, $LDAPUser .'@'. $this::LDAP_host, $LDAPPassword))) { // Esta es la validación con @host
                        return false;
                    }
                }
                ldap_unbind($ds);
            }
            //ldap_close($ds);
        } catch (\Exception $e) {
        }

        if ($bind) {
            $LDAPUser = (strpos($LDAPUser, '@')>0 ? substr($LDAPUser, 0, strpos($LDAPUser, '@')) : $LDAPUser);
            $dql = "SELECT a.cedula, a.usuario, a.nombre FROM FCMainBundle:Seguridadusuarios a WHERE a.usuario='" . $LDAPUser . "'";
            $result = $this->em->createQuery($dql)->getResult();
            if ($result) {
                $dql = "SELECT a FROM FCMainBundle:Personal a WHERE a.cedula=" . $result[0]['cedula'];
                $resultPersonal = $this->em->createQuery($dql)->getResult();
                if ($resultPersonal) {
                    $result[0]['Personal'] = $resultPersonal[0];
                } else {
                    $result[0]['Personal'] = NULL;
                }
                return $result[0];
            }
            $result = null;
            $result[0]['cedula'] = 0;
            $result[0]['usuario'] = $LDAPUser;
            $result[0]['nombre'] = $LDAPUser;
            return $result[0];
        }
        return false;
    }

    private function cambiarClaveUsuarioSQL($usuario, $clave, $clavenueva) {
        $usuarioObj = $this->em->getRepository('FCMainBundle:Seguridadusuarios')->findOneByUsuario($usuario);
        if ($usuarioObj) {
            if (strtolower($usuarioObj->getClave()) == strtolower($clave)) {
                $usuarioObj->setClave($clavenueva);
            	$this->em->persist($usuarioObj);
            	$this->em->flush();
            	return true;
            }
        }
        $this->session->getFlashBag()->add('danger', 'La contrase�a no ha podido ser modificada');
        return false;
    }

    private function cambiarClaveUsuarioLDAP($LDAPUser, $LDAPPassword, $LDAPPasswordNew) {
        $message = array();
        try {
            if (!($ds = @ldap_connect($this::LDAP_host, $this::LDAP_port))) { // Conecta al servidor ldap
                $message[] = "Could not connect to ldap server";
            }
            ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);

            if (!($bind = @ldap_bind($ds, $this::LDAP_rdn, $this::LDAP_pass))) { // Esta es la validación
                $message[] = "Could not bind ldap server";
            }

            $user_search = ldap_search($ds,$this::LDAP_dn,"(|(samaccountname=$LDAPUser)(mail=$LDAPUser))"); // find user
            $user_get = ldap_get_entries($ds, $user_search);
            $user_entry = ldap_first_entry($ds, $user_search);
            $user_dn = ldap_get_dn($ds, $user_entry);
            $user_samaccountname = $user_get[0]["samaccountname"][0];
            $user_search_arry = array(); //array( "*", "ou", "mail", "passwordretrycount", "passwordhistory" );
            $user_search_filter = "(|(samaccountname=$LDAPUser)(mail=$LDAPUser))";
            $user_search_opt = ldap_search($ds,$user_dn,$user_search_filter,$user_search_arry);
            $user_get_opt = ldap_get_entries($ds, $user_search_opt);
            //$passwordretrycount = $user_get_opt[0]["badpwdcount"][0];//$user_get_opt[0]["passwordretrycount"][0];
            //$passwordhistory = $user_get_opt[0]["passwordhistory"][0];


            $message[] = "Username: " . $user_samaccountname;
            $message[] = "DN: " . $user_dn;

            //$message[] = "Current Pass: " . $LDAPPassword;
            //$message[] = "New Pass: " . $LDAPPasswordNew;

            /* Start the testing */
            /*
            if ( $passwordretrycount == 3 ) {
                $message[] = "Error E101 - Your Account is Locked Out!!!";
                $this->session->getFlashBag()->add('danger', implode(" / ", $message));
                return false;
            }
            */
            /*
            if (ldap_bind($ds, $user_dn, $LDAPPassword) === false) {
                $message[] = "Error E101 - Current Username or Password is wrong.";
                $this->session->getFlashBag()->add('danger', implode(" / ", $message));
                return false;
            }
            if ($LDAPPasswordNew != $LDAPPasswordNewConfirm ) {
                $message[] = "Error E102 - Your New passwords do not match!";
                $this->session->getFlashBag()->add('danger', implode(" / ", $message));
                return false;
            }
            */
            //$history_arr = ldap_get_values($ds,$user_dn,"passwordhistory");
            //if ( $history_arr ) {
            //    $message[] = "Error E102 - Your new password matches one of the last 10 passwords that you used, you MUST come up with a new password.";
            //    $this->session->getFlashBag()->add('danger', implode(" / ", $message));
            //    return false;
            //}

            /* === Ini - Validación de Políticas de Passwords ====================================================================
            if (strlen($LDAPPasswordNew) < 8 ) {
                $message[] = "Error E103 - Your new password is too short.<br/>Your password must be at least 8 characters long.";
                $this->session->getFlashBag()->add('danger', implode(" / ", $message));
                return false;
            }
            if (!preg_match("/[a-zA-Z]/",$LDAPPasswordNew)) {
                $message[] = "Error E105 - Your new password must contain at least one letter.";
                $this->session->getFlashBag()->add('danger', implode(" / ", $message));
                return false;
            }
            if (!preg_match("/[0-9]/",$LDAPPasswordNew)) {
                $message[] = "Error E104 - Your new password must contain at least one number.";
                $this->session->getFlashBag()->add('danger', implode(" / ", $message));
                return false;
            }
            if (!preg_match("/[A-Z]/",$LDAPPasswordNew)) {
                $message[] = "Error E106 - Your new password must contain at least one uppercase letter.";
                $this->session->getFlashBag()->add('danger', implode(" / ", $message));
                return false;
            }
            if (!preg_match("/[a-z]/",$LDAPPasswordNew)) {
                $message[] = "Error E107 - Your new password must contain at least one lowercase letter.";
                $this->session->getFlashBag()->add('danger', implode(" / ", $message));
                return false;
            }
            /* === Fin - Validación de Políticas de Passwords ==================================================================== */

            if (!$user_get) {
                $message[] = "Error E200 - Unable to connect to server, you may not change your password at this time, sorry.";
                $this->session->getFlashBag()->add('danger', implode(" / ", $message));
                return false;
            }

            /*
			$auth_entry = ldap_first_entry($ds, $user_search);
            $mail_addresses = ldap_get_values($ds, $auth_entry, "mail");
            $given_names = ldap_get_values($ds, $auth_entry, "givenname");
            //$password_history = ldap_get_values($ds, $auth_entry, "passwordhistory");
            $mail_address = $mail_addresses[0];
            $first_name = $given_names[0];
			*/

            /* And Finally, Change the password */
            $newPassw = "";
            $newPassword = $LDAPPasswordNew;
            $newPassword = "\"" . $newPassword . "\"";
            $len = strlen($newPassword);
            for ($i = 0; $i < $len; $i++)
                $newPassw .= "{$newPassword{$i}}\000";
            $newPassword = $newPassw;
            $userdata["unicodePwd"] = $newPassword;
            $result = ldap_mod_replace($ds, $user_dn, $userdata);
            if ($result) {
                return true;
            } else {
                $errno = ldap_errno($ds);
                $error = ldap_error($ds);
                $message[] = "There was a problem!";
                $message[] = "$errno - $error";
                $this->session->getFlashBag()->add('danger', implode(" / ", $message));
                return false;
            }

            $entry = array();
            /* --- OPTION 1 --- *///$entry["userpassword"] = "{SHA}" . base64_encode(pack("H*", sha1($LDAPPasswordNew)));
            /* --- OPTION 2 --- *///$entry['userpassword'] = "{md5}". base64_encode(pack("H*", md5($LDAPPasswordNew)));
            /* --- OPTION 3 --- *///$entry["unicodePwd"] = mb_convert_encoding($LDAPPasswordNew, 'UTF-8', 'ASCII');
            $entry["unicodePwd"] = "{md5}". base64_encode(pack("H*", md5($LDAPPasswordNew)));

            if (!($modify = @ldap_modify($ds,$user_dn,$entry))) {
                $errno = ldap_errno($ds);
                $error = ldap_error($ds);
                $message[] = "E201 - Your password cannot be change, please contact the administrator.";
                $message[] = "$errno - $error";
                $this->session->getFlashBag()->add('danger', implode(" / ", $message));
                return false;
            } else {
                return true;
            }
        } catch (\Exception $e) {
            $this->session->getFlashBag()->add('danger', get_class($this) .' - Linea: '. __LINE__ .' - Descripción: '. $e->getMessage() .'. '. implode(" / ", $message));
            return false;
        }
    }

    public function listadoUsuariosLDAP () {
        /*$users[0]['usuario'] = 'ADMIN';
        $users[0]['cedula'] = 999999; //$entries[$i][""][0];
        $users[0]['nombre'] = 'Administrador del Sistema';
        $users[0]['email'] = 'soporte@ferrarocamacho.com.ar';
        $users[0]['modulo'] = null;
        $users[0]['perfil'] = null;
        return $users;
        */
        $users = null;
        $where = '';
        $i = 0;

        try {
            if ($ds = @ldap_connect($this::LDAP_host, $this::LDAP_port)) { // Conecta al servidor ldap
                ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
                ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);
                if ($bind = @ldap_bind($ds, $this::LDAP_rdn, $this::LDAP_pass)) { // Esta es la validación
                    // Especifico los parámetros que quiero que me regrese la consulta
                    $attrs = array(); //Ej. array("displayname", "email", "samaccountname", "telephonenumber", "ou", "sn", "displayname", "mail");
                    // Creo el filtro para la busqueda. En este caso todos los usuarios
                    $filter = "(samaccountname=*)";
                    $search = ldap_search($ds, $this::LDAP_dn, $filter, $attrs);
                    $entries = ldap_get_entries($ds, $search);

                    if ($entries["count"] > 0) {
                        for ($i = 0; $i < $entries["count"]; $i++) {
                            if ($entries[$i]["objectclass"][1] == "person") {
                                $where .= ($where <> '' ? ', ' : '') . "'" . $entries[$i]["samaccountname"][0] . "'";
                                $users[$i]['usuario'] = strtoupper($entries[$i]["samaccountname"][0]);
                                $users[$i]['cedula'] = null;
                                try {
                                    $users[$i]['nombre'] = $entries[$i]["displayname"][0];
                                } catch (\Exception $e) {
                                    $users[$i]['nombre'] = '';
                                }
                                try {
                                    $users[$i]['email'] = $entries[$i]["userprincipalname"][0];
                                } catch (\Exception $e) {
                                    $users[$i]['email'] = '';
                                }
                                $users[$i]['modulo'] = null;
                                $users[$i]['perfil'] = null;
                            }
                        }
                    }
                }
                ldap_unbind($ds);
            }
            //ldap_close($ds);
        } catch (\Exception $e) {
            $where .= ($where <>'' ? ', ' : '') . "'ADMIN'";
            $users[0]['usuario'] = 'ADMIN';
            $users[0]['cedula'] = 99999999;
            $users[0]['nombre'] = 'Administrador del Sistema';
            $users[0]['email'] = 'soporte@ferrarocamacho.com.ar';
            $users[0]['modulo'] = null;
            $users[0]['perfil'] = null;
        }

        $result = $this->em->createQuery('SELECT a.usuario, u.cedula, u.nombre, b.descripcion AS modulo, c.des AS perfil, u.email FROM FCMainBundle:Seguridadusuariossistemasperfil a LEFT JOIN FCMainBundle:Seguridadusuarios u WITH a.usuario=u.usuario LEFT JOIN FCMainBundle:Seguridadsistemas b WITH a.sistema=b.sistema LEFT JOIN FCMainBundle:Seguridaddescperfiles c WITH a.sistema=c.sistema AND a.perfil=c.cod WHERE a.usuario IN ('. $where .') ORDER BY a.usuario, a.sistema, a.perfil')->getResult();
        foreach ($result as $item) {
            foreach ($users as &$user) {
                if (strtolower($item['usuario']) == strtolower($user['usuario'])) {
                    if ($user['modulo'] == null) {
                        $user['cedula'] = $item['cedula'];
                        $user['nombre'] = $item['nombre'];
                        $user['modulo'] = $item['modulo'];
                        $user['perfil'] = $item['perfil'];
                        $user['email'] = $item['email'];
                    } else {
                        $i++;
                        $users[$i]['usuario'] = $user['usuario'];
                        $users[$i]['cedula'] = $user['cedula'];
                        $users[$i]['nombre'] = $user['nombre'];
                        $users[$i]['email'] = $item['email'];
                        $users[$i]['modulo'] = $item['modulo'];
                        $users[$i]['perfil'] = $item['perfil'];
                    }
                    break;
                }
            }
        }
        sort($users);
        return $users;
    }

    //Si pertenece al centro de costo
    public function verificarSistemasParametrizacion($idParameter){
        $result = $this->em->createQuery("SELECT a.centrocosto AS Retorna FROM FCMainBundle:Personal a WHERE a.cedula=". $this->getCedula())->getResult();
        $centrocosto = ($result ? strval($result[0]['Retorna']) : null);

        /* Solucion parseando consulta */
        $result = $this->em->createQuery("SELECT a.value AS Retorna FROM FCMainBundle:Seguridadsistemasparametrizacion a WHERE a.idparameter='".$idParameter."'")->getResult();
        if($result){
            $position = strpos($result[0]['Retorna'], $centrocosto);
            if($position){
                return true;
            }
        }
        return false;
    }

    public function getFuncion($PrefijoTabla) {
        $cedula = $this->getCedula();
        $result = $this->em->createQuery("SELECT a.idfuncion AS Retorna FROM FCMainBundle:" . $this->getTransformChars($PrefijoTabla . "FuncionesAsignaciones") ." a WHERE a.cedula = ".$cedula)->getResult();
        return ($result ? $result[0]['Retorna'] : 0);
    }

    public function getFuncionesAsignaciones($PrefijoTabla, $idFuncion) {
        $result = $this->em->createQuery("SELECT a.idfuncion, a.descripcion, a.funcionespecial FROM FCMainBundle:" . $this->getTransformChars($PrefijoTabla . "Funciones") ." a WHERE a.idfuncion IN (SELECT b.iditem FROM FCMainBundle:" . $this->getTransformChars($PrefijoTabla . "FuncionesEstructura") ." b WHERE b.idfuncion=".$idFuncion.") OR a.idfuncion=".$idFuncion ." ORDER BY a.descripcion")->getResult();
        return $result;
    }

    public function getValueOfEntities($Field, $Entity, $Where = null, $OrderBy = null, $DefaultValue = null) {
        try {
            $result = $this->em->createQuery("SELECT " . $Field . " AS Retorna FROM " . $Entity . ($Where != null ? " WHERE " . $Where : "") . ($OrderBy != null ? " ORDER BY " . $OrderBy : ""))->getResult();
        } catch (\Exception $e) {
            $this->session->getFlashBag()->add('danger', get_class($this) .' - Linea: '. __LINE__ .' - {{F: '. $Field .'}, {E: '. $Entity .'}, {W: '. $Where .'}, {O: '. $OrderBy .'}, {D: '. $DefaultValue .'}}');
            $result = null;
        }
        return ($result ? $result[0]['Retorna'] : $DefaultValue);
    }

    public function getTransformChars($Table) {
        $array = str_split(strtolower($Table));

        $lsReturn = '';
        $nextUpper = true;
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i] == '_') {
                $nextUpper = true;
                // Omite este carater y pone en mayúsculas para el próximo
            } else {
                if ($nextUpper) {
                    $nextUpper=false;
                    $lsReturn .= strtoupper($array[$i]);
                } else {
                    $lsReturn .= $array[$i];
                }
            }
        }
        return $lsReturn;
    }

    //funciones para SARP Libro de novedades
    public function getPlantilla(Request $request, $PrefijoEntity) {
        $cedula = $this->getCedula();
        $result = $this->em->createQuery("SELECT a.idplantilla AS Retorna FROM FCMainBundle:" . $PrefijoEntity . "Maestroplantillasasignaciones a WHERE a.cedula = ".$cedula)->getResult();
        return ($result ? $result[0]['Retorna'] : 0);
    }

    public function getPlantillasEstructura($idPlantilla, $PrefijoEntity) {
        $result = $this->em->createQuery("SELECT a.id, a.descripcion FROM FCMainBundle:" .  $PrefijoEntity . "Maestroplantillas a WHERE a.plantilla is not NULL AND (a.id IN (SELECT b.iditem FROM FCMainBundle:" . $PrefijoEntity . "Maestroplantillasestructura b WHERE b.idplantilla=".$idPlantilla.") OR a.id=".$idPlantilla . ") ORDER BY a.descripcion")->getResult();
        return $result;
    }

    public function getRol($sistema) {
        switch (true) {
            case $this->validarpermiso('ROLSEGIND', $sistema):
                return RolesEnum::RolSegInd;
                break;
            case $this->validarpermiso('ROLNEXO', $sistema):
                return RolesEnum::RolNexo;
                break;
            case $this->validarpermiso('ROLCONTRAT', $sistema):
                return RolesEnum::RolContratista;
                break;
            default:
                return null;
        }
    }

    public function getRoles($sistema) {
        $Return = null;
        if ($this->validarpermiso('ROLSEGIND', $sistema)) {
            $Return[] = RolesEnum::RolSegInd;
        }
        if ($this->validarpermiso('ROLNEXO', $sistema)) {
            $Return[] = RolesEnum::RolNexo;
        }
        if ($this->validarpermiso('ROLCONTRAT', $sistema)) {
            $Return[] = RolesEnum::RolContratista;
        }
        return $Return;
    }
}