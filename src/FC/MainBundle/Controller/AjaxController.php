<?php

namespace FC\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

class AjaxController extends Controller {
	public function ajaxPersonalAction(Request $request) {
		$params        = $request->request->all();
		$cedula = intval($params['idsolicitante']);
		$em            = $this->getDoctrine()->getManager();
		$usuario       = $em->createQuery('SELECT a.apenom, a.email FROM FCMainBundle:Personal a WHERE a.cedula='.$cedula)->getResult();
		if ($usuario) {
			$nombre        = $usuario[0]['apenom'];
			$mail          = $usuario[0]['email'];
			//$nombre        = $usuario[0]->getApenom();
			//$mail          = $usuario[0]->getEmail();
			$success       = true;
		} else {
			$nombre        = "";
			$mail          = "";
			$success       = false;
		}
		$responseArray = array(
			"nombre"  => $nombre,
			"mail"    => $mail,
			"success" => $success 
			);
		$response = new Response(json_encode($responseArray));
		return $response;
	}

	public function ajaxPersonalCompletoAction(Request $request) {
		$params        = $request->request->all();
		$cedula = intval($params['cedula']);
		$em            = $this->getDoctrine()->getManager();
		$usuario       = $em->getRepository('FCMainBundle:Personal')->findByCedula($cedula);

		$nroregistro="";
		$nombre     ="";
        $idempresa  ="";
		$fechanac   ="";
		$idcargo    ="";
		$descargo   ="";
		$fecascenso ="";
		$idinstall  ="";
		$idarea     ="";
		$edad       ="";
		$antiguedad ="";
		$email      ="";
		$cel        ="";
		$cc         ="";
        $foto       =false;
		$success    =false;

		if ($usuario) {
			$nroregistro	= $usuario[0]->getNroregistro();
			$nombre			= $usuario[0]->getApenom();
            $idempresa		= $usuario[0]->getIdempresa();
			$fechanac		= $usuario[0]->getFechanac();
			$idcargo		= $usuario[0]->getIdcargo();
			$fecascenso		= $usuario[0]->getFecascenso();
			$idinstall		= $usuario[0]->getIdinstall();
			$idarea			= $usuario[0]->getIdarea();
			$email			= $usuario[0]->getEmail();
			$cel			= $usuario[0]->getCel();
			$cc				= $usuario[0]->getCentrocosto();
			$now = new \DateTime();
            if($fechanac){
                $edad = $now->diff($fechanac);
                $edad = $edad->y;
                $fechanac = $fechanac->format('d/m/Y');
            }
            if($fecascenso){
                $antiguedad = $now->diff($fecascenso);
                $antiguedad = $antiguedad->y;
                $fecascenso = $fecascenso->format('d/m/Y');
            }
            if($idcargo){
                $descargo = $this->get('seguridad')->getValueOfEntities('a.descripcion', 'FCMainBundle:Cargos a', 'a.idcargo='.$idcargo);
			}
            $baseurl = __DIR__ . DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR .'web'. DIRECTORY_SEPARATOR .'upload'. DIRECTORY_SEPARATOR .'Personal'. DIRECTORY_SEPARATOR;
            $foto = file_exists($baseurl.$cedula.'.jpg');
			$success = true;
		}
		$responseArray = array(
			'nroregistro'=>$nroregistro, 
			'nombre'=>$nombre,
            'idempresa'=>$idempresa,
			'fechanac'=>$fechanac,
			'idcargo'=>$idcargo,
			'descargo'=>$descargo,
			'fecascenso'=>$fecascenso, 
			'idinstall'=>$idinstall, 
			'idarea'=>$idarea, 
			'edad'=>$edad, 
			'antiguedad'=>$antiguedad, 
			'email'=>$email,
			'cel'=>$cel,
			'cc'=>$cc,
            'foto'=>$foto,
			'success' => $success 
			);
		$response = new Response(json_encode($responseArray));
		return $response;
	}

	public function ajaxPersonalValidationAction(Request $request) {
		$params        = $request->request->all();
		$cedula = intval($params['idsolicitante']);
		$firmaelectro  = trim($params['firmaelectro']);
		$em            = $this->getDoctrine()->getManager();
		$usuario       = $em->getRepository('FCMainBundle:Personal')->findByCedula($cedula);
		
		if ($usuario) {
			if (strtolower($usuario[0]->getFirmaelectro()) == strtolower($firmaelectro)) {
				$success = true;
			} else {
				$success = false;
			}
		} else {
			$success = false;
		}
		$responseArray = array(
			"success" => $success 
			);
		$response = new Response(json_encode($responseArray));
		return $response;
	}

    public function ajaxUserPassValidationAction(Request $request) {
        $params = $request->request->all();
        $username =  strtolower(trim($params['usuario']));
        $password  = strtolower(trim($params['password']));
        $em            = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('FCMainBundle:Seguridadusuarios')->findOneBy(array(
            'usuario' => $username,
            'clave' => $password
        ));

        if ($usuario) {
            $success = true;
        } else {
            $success = false;
        }

        $responseArray = array(
            "success" => $success
        );
        $response = new Response(json_encode($responseArray));
        return $response;
    }

    public function ajaxPersonalValidationSaveAction(Request $request) {
        $params        = $request->request->all();
        $cedula = intval($params['idsolicitante']);
        $firmaelectro  = trim($params['firmaelectro']);
        $em            = $this->getDoctrine()->getManager();
        $usuario       = $em->getRepository('FCMainBundle:Personal')->findByCedula($cedula);
        $tipofirma =  trim($params['tipofirma']);
        $idcis=  trim($params['idcis']);
        $hora=  new \DateTime (\Date("d-m-Y")." ".$params ['hora']);
        $fecha=  new \DateTime ($params["fecha"]);

        if ($usuario) {
            if (strtolower($usuario[0]->getFirmaelectro()) == strtolower($firmaelectro)) {
                $entity = $em->getRepository('FCMainBundle:Cis')->findOneByIdcis($idcis);
                switch ($tipofirma) {
                    case "e1":
                    case "e2":
                    case "e3":
                    case "xe":
                        $codigo = "FIRMA_EMIS";
                        break;
                    case "rt1":
                    case "rt2":
                    case "rt3":
                    case "xrt":
                        $codigo = "FIRMA_TRAB";
                        break;
                    case "ra1":
                    case "ra2":
                    case "ra3":
                    case "xra":
                        $codigo = "FIRMA_AREA";
                        break;
                    default:
                        $codigo = "FIRMA_BOOL";
                        break;
                }
                if($codigo != "FIRMA_BOOL"){
                    $seguridad = $this->get('seguridad')->verificarPermisoOperacion($codigo, 'CI');
                }else{
                    $seguridad = "true";
                }
                if ($seguridad == "true")
                {
                    switch ($tipofirma) {
                        case "e1":
                            $entity->setHoraemi1($hora);
                            $entity->setEmi1($cedula);
                            $entity->setFirmaemi1(1);
                            break;
                        case "e2":
                            $entity->setHoraemi2($hora);
                            $entity->setEmi2($cedula);
                            $entity->setFirmaemi2(1);
                            break;
                        case "e3":
                            $entity->setHoraemi3($hora);
                            $entity->setEmi3($cedula);
                            $entity->setFirmaemi3(1);
                            break;
                        case "rt1":
                            $entity->setHoraresptrabajo1($hora);
                            $entity->setResptrabajo1($cedula);
                            $entity->setFirmaresptrabajo1(1);
                            break;
                        case "rt2":
                            $entity->setHoraresptrabajo2($hora);
                            $entity->setResptrabajo2($cedula);
                            $entity->setFirmaresptrabajo2(1);
                            break;
                        case "rt3":
                            $entity->setHoraresptrabajo3($hora);
                            $entity->setResptrabajo3($cedula);
                            $entity->setFirmaresptrabajo3(1);
                            break;
                        case "ra1":
                            $entity->setHoraresparea1($hora);
                            $entity->setResparea1($cedula);
                            $entity->setFirmaresparea1(1);
                            break;
                        case "ra2":
                            $entity->setHoraresparea2($hora);
                            $entity->setResparea2($cedula);
                            $entity->setFirmaresparea2(1);
                            break;
                        case "ra3":
                            $entity->setHoraresparea3($hora);
                            $entity->setResparea3($cedula);
                            $entity->setFirmaresparea3(1);
                            break;
                        case "xe":
                            $entity->setExtnuevafecha($fecha);
                            $entity->setExtnuevahora($hora);
                            $entity->setExtcedemi($cedula);
                            $entity->setFirmaextcedemi(1);
                            break;
                        case "xrt":
                            $entity->setExtnuevafecha($fecha);
                            $entity->setExtnuevahora($hora);
                            $entity->setExtcedresptrabajo($cedula);
                            $entity->setFirmaextcedresptrabajo(1);
                            break;
                        case "xra":
                            $entity->setExtnuevafecha($fecha);
                            $entity->setExtnuevahora($hora);
                            $entity->setExtcedresparea($cedula);
                            $entity->setFirmaextcedresparea(1);
                            break;
                        default:
                            break;
                    }
                    $em->persist($entity);
                    $em->flush();
                    $success = true;
                }
                else
                {
                    $success = false; // No paso los permisos
                }
            } else {
                $success = false; //Constraseña incorrecta
            }
        } else {
            $success = false; // No encontro el usuario
        }
        $responseArray = array(
            "success" => $success
        );
        $response = new Response(json_encode($responseArray));
        return $response;
    }
}
