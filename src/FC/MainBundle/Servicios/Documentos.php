<?php

namespace FC\MainBundle\Servicios;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class Documentos {
	private $em;
  	private $container;	

	public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
		$this->container = $container;
	}

    public function generarIdTabla($tabla, $campo, $planta = 1, $TransformChars = true) {
        $dql = 'SELECT
                    MAX(a.'. strtolower($campo) .') AS MaxID,
                    MIN(b.nroini) AS MinID
                FROM FCMainBundle:'. ($TransformChars ? $this->getTransformChars($tabla) : $tabla) .' a
                    LEFT JOIN FCMainBundle:Documentosnumeradores b WITH b.idplanta=' . $planta . ' AND b.iddocumento=-1
                WHERE
                    a.'. strtolower($campo) .' > (SELECT MIN(c.nroini) FROM FCMainBundle:Documentosnumeradores c
                                WHERE c.idplanta=' . $planta . ' AND c.iddocumento=-1)
                    AND a.'. strtolower($campo) .' < (SELECT d.nrofin FROM FCMainBundle:Documentosnumeradores d
								    WHERE d.idplanta=' . $planta . ' AND d.iddocumento=-1)';
        $Result = $this->em->createQuery($dql)->getResult();
        if ($Result) {
            if ($Result[0]['MaxID'] != null) {
                return (int) ($Result[0]['MaxID']) + 1;
            } else {
                return (int) ($Result[0]['MinID']) + 1;
            }
        } else {
            return null;
        }
    }

    private function getTransformChars($Table) {
        $array = str_split(strtolower($Table));

        $lsReturn = '';
        $nextUpper = true;
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i] == '_') {
                $nextUpper = true;
                // Omite este carater y pone en mayúsculas para el próximo
            } else {
                if ($nextUpper) {
                    $nextUpper=false;
                    $lsReturn .= strtoupper($array[$i]);
                } else {
                    $lsReturn .= $array[$i];
                }
            }
        }
        return $lsReturn;
    }
}