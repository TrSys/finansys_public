<?php

namespace FC\MainBundle\Servicios;

use Doctrine\ORM\EntityManager;
use FC\MainBundle\Entity\Seguridadenviodecorreo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class NotificacionesParametersType {
    public $Host;
    public $Port;
    public $SSL;
    public $User;
    public $Pass;
    public $Authentication;
    public $From;
    public $CCO;
}

class NotificacionesCuentasEnum {
    const Correo=1;
    const SMS=2;
}

class NotificacionesFormatsEnum {
    const HTML=0;
    const TextPlain=1;
}

class CDataFormatsField {
    const CDataFechaHora = 0;
    const CDataHora = 1;
    const CDataMoneda = 2;
    const CDataFecha = 3;
    const CDataFechaMes = 4;
    const CDataFechaMesAnio = 5;
    const CDataFechaAnio = 6;
    const CDataFechaTrimestre = 7;
    const CDataFechaTrimestreAnio = 8;
    const CDataTextoCorto = 9;
    const CDataTextoLargo = 10;
    const CDataFechaAnioMes = 11;
    const CDataNumberInteger = 12;
    const CDataNumberFloat = 13;
}

class ClasificacionOperativaEnum {
    const ClasifOperativo = 1;
    const ClasifNoOperativo = 2;
}

class TipoDeIncidenteEnum {
    const TipoGrave = 1;
    const TipoSignificativo = 2;
    const TipoMenor = 3;
}

class Notificaciones
{
    private $EmailParameters;
    private $em;
    private $request;
    private $container;
    private $IdPlanta;
    private $DesPlanta;
    private $ModoDeveloper;
    private $Sistema;
    const   idEmpresa='ANCAP';

    public function __construct(EntityManager $entityManager, ContainerInterface $container, Request $request) {
        $this->em = $entityManager;
        $this->request = $request;
        $this->container = $container;
        $this->EmailParameters = new NotificacionesParametersType();
        $this->IdPlanta = 1;
        switch ($container->get('kernel')->getEnvironment()) {
            case 'sirva':
            case 'sirva_dev':
                $this->Sistema = 'SIRVA';
                break;
            case 'sarp':
            case 'sarp_dev':
                $this->Sistema = 'SARP';
                break;
            default:
                $this->Sistema = $this->em->getConnection()->getDatabase();
                break;
        }
        $this->DesPlanta = $this->getValueOfEntities("a.descripcion", "FCMainBundle:Plantas a", "a.idplanta=" . $this->IdPlanta);
        $this->ModoDeveloper = (strpos($this->mTomarServerNamesAdmitidos(), $this->em->getConnection()->getHost()) === false ? true : false);
    }

    public function gRealizarEnvioDeCorreos() {
        try {
            if ($this->mCargarParametrosCorreo()) {
                $correos = $this->em->createQuery("SELECT a FROM FCMainBundle:Seguridadenviodecorreo a WHERE (a.estado=0 OR a.estado IS NULL)")->getResult();
                foreach ($correos as $data) {
                    $transport = \Swift_SmtpTransport::newInstance($this->EmailParameters->Host, $this->EmailParameters->Port, ($this->EmailParameters->SSL) ? 'ssl' : null);
                    if ($this->EmailParameters->Authentication) {
                        $transport->setUsername($this->EmailParameters->User);
                        $transport->setPassword($this->EmailParameters->Pass);
                    }
                    $mailer = \Swift_Mailer::newInstance($transport);

                    $receptores = null;
                    if (!($this->ModoDeveloper)) {
                        $aux = explode(",", trim($data->getReceptor()));
                        foreach ($aux as $email) {
                            if (trim($email) != '') {
                                $receptores[] = $email;
                            }
                        }
                    }
                    try {
                        $result = 0;
                        $message = \Swift_Message::newInstance();
                        $message->setFrom($this->EmailParameters->From);
                        if (!($this->ModoDeveloper)) {
                            $message->setTo($receptores);
                        }
                        $message->setBcc($this->EmailParameters->CCO);
                        $message->setContentType('text/html');
                        $message->setSubject(($this->ModoDeveloper ? "DEV - " : "") . $data->getAsunto());
                        $message->setBody($data->getMensaje());

                        $result = $mailer->send($message);
                        if ($result != 0) {
                            $data->setEstado(1);
                            $this->em->persist($data);
                        }
                    } catch (\Exception $e) {
                        $this->request->getSession()->getFlashBag()->add('danger', get_class($this) .' - Linea: '. __LINE__ .' - Descripción: '. $e->getMessage());
                    }
                }
                $this->em->flush();
            }
        } catch (\Exception $e) {
            $this->request->getSession()->getFlashBag()->add('danger', get_class($this) .' - Linea: '. __LINE__ .' - Descripción: '. $e->getMessage());
        }
    }

    public function gGenerarNotificacionSimple($psReceptor, $psAsunto, $psMensaje) {
        $llIdCorreo = $this->generarIdTabla("Seguridadenviodecorreo", "idcorreo");
        $entity = new Seguridadenviodecorreo();
        $entity->setIdcorreo($llIdCorreo);
        $entity->setEmisor('');
        $entity->setReceptor((!($this->ModoDeveloper) ? trim(substr($psReceptor, 0, 2000)) : ""));
        $entity->setAsunto(trim(substr($psAsunto, 0, 500)));
        $entity->setMensaje(trim(substr($psMensaje, 0, 5000)));
        $entity->setShowscreen(0);
        $entity->setShowmessages(0);
        $entity->setEstado(0);
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function gGenerarNotificacionDOC($plIDDoc, $plDocNro, $pbNuevoRegistro = true, $psAgrearAlAsunto = "", $psCustomMessage = "")
    {
        if ($this->mValidarEnvioDeNotificacion($plIDDoc, $pbNuevoRegistro) != true) {
            return false;
        }

        $lsFecha = $this->mTomarFechaDelReporte($plIDDoc, $plDocNro);
        $lsMensaje = $this->mTomarCuerpoMensaje($plIDDoc, $plDocNro) . $psCustomMessage;
        $this->mEnviarNotificacionEMAIL($plIDDoc, $plDocNro, $lsFecha, $lsMensaje, $psAgrearAlAsunto);
        $this->mEnviarNotificacionSMS($plIDDoc, $plDocNro, $lsFecha, $lsMensaje, $psAgrearAlAsunto);
        return true;
    }

    public function gTomarReceptoresDelDocumento($plIDDoc, $plDocNro, $psSeparador, $Cuenta) {
        $lsWHERE = "";
        $lsReceptores = "";
        switch ($plIDDoc) {
            case DocumentosNumeradores::DocAtenIncidente:
                $lsAux = "idDocumento=" . $plIDDoc;
                $lsAux = $lsAux . "|idCondicion=1";
                $lsAux = $lsAux . "|idInstall=" . $this->getValueOfEntities("a.idinstall", "FCMainBundle:Intervenciones a", "a.nroatencion=" . $plDocNro, null, 0);
                $lsWHERE = $lsWHERE . " AND u.usuario IN (SELECT a.usuario FROM FCMainBundle:Seguridadusuariosnotificacionesnew a WHERE a.condicion='" . $lsAux . "')";

                $lsAux = "idDocumento=" . $plIDDoc;
                $lsAux = $lsAux . "|idCondicion=2";
                $lsAux = $lsAux . "|idEmpresa=" . ($this->getValueOfEntities("p.idempresa", "FCMainBundle:Intervenciones r LEFT JOIN FCMainBundle:Personal p WITH r.idpersles=p.cedula", "r.nroatencion=" . $plDocNro . " AND p.idempresa='" . $this::idEmpresa . "'", null, "") == $this::idEmpresa ? $this::idEmpresa : "NULL");
                $lsWHERE = $lsWHERE . " AND u.usuario IN (SELECT b.usuario FROM FCMainBundle:Seguridadusuariosnotificacionesnew b WHERE b.condicion='" . $lsAux . "')";

                $lsAux = "idDocumento=" . $plIDDoc;
                $lsAux = $lsAux . "|idCondicion=3";
                $lsAux = $lsAux . "|idTratamiento=" . $this->getValueOfEntities("a.idtrataposterior", "FCMainBundle:Intervenciones a", "a.nroatencion=" . $plDocNro, null, 0);
                $lsWHERE = $lsWHERE . " AND u.usuario IN (SELECT c.usuario FROM FCMainBundle:Seguridadusuariosnotificacionesnew c WHERE c.condicion='" . $lsAux . "')";
                break;
            case DocumentosNumeradores::DocReporteDeIncidentes:
                $lsAux = "idDocumento=" . $plIDDoc;
                $lsAux = $lsAux . "|idCondicion=1";
                $lsAux = $lsAux . "|idInstall=" . $this->getValueOfEntities("a.idinstall", "FCMainBundle:Reportedeincidentescabecera a", "a.idreporte=" . $plDocNro, null, 0);
                $lsAux = $lsAux . "|idArea=" . $this->getValueOfEntities("a.idarea", "FCMainBundle:Reportedeincidentescabecera a", "a.idreporte=" . $plDocNro, null, 0);
                $lsWHERE = $lsWHERE . " AND u.usuario IN (SELECT a.usuario FROM FCMainBundle:Seguridadusuariosnotificacionesnew a WHERE a.condicion='" . $lsAux . "')";

                $lsAux = "idDocumento=" . $plIDDoc;
                $lsAux = $lsAux . "|idCondicion=2";
                $lsAux = $lsAux . "|ConLesionesPersonales=" . ($this->getValueOfEntities("a.conlesionespersonales", "FCMainBundle:Reportedeincidentescabecera a", "a.idreporte=" . $plDocNro, null, false)==true ? 1 : 0);
                $lsWHERE = $lsWHERE . " AND u.usuario IN (SELECT b.usuario FROM FCMainBundle:Seguridadusuariosnotificacionesnew b WHERE b.condicion='" . $lsAux . "')";

                $lsAux = "idDocumento=" . $plIDDoc;
                $lsAux = $lsAux . "|idCondicion=3";
                $lsAux = $lsAux . "|idEmpresa=" . ($this->getValueOfEntities("p.idempresa", "FCMainBundle:Reportedeincidentespersonas r LEFT JOIN Personal p WITH r.cedula=p.cedula", "r.idreporte=" . $plDocNro . " AND p.idempresa='" . $this::idEmpresa . "'", null, "") == $this::idEmpresa ? $this::idEmpresa : "NULL");
                $lsWHERE = $lsWHERE . " AND u.usuario IN (SELECT c.usuario FROM FCMainBundle:Seguridadusuariosnotificacionesnew c WHERE c.condicion='" . $lsAux . "')";
                break;
            Case DocumentosNumeradores::DocReporteDeIncidentesClasificacion:
                $lsAux = "idDocumento=" . $plIDDoc;
                $lsAux = $lsAux . "|idCondicion=1";
                $lsAux = $lsAux . "|idInstall=" . $this->getValueOfEntities("a.idinstall", "FCMainBundle:ReporteDeIncidentesCabecera a", "a.idreporte=" . $plDocNro, null, 0);
                $lsWHERE = $lsWHERE . " AND u.usuario IN (SELECT a.usuario FROM FCMainBundle:Seguridadusuariosnotificacionesnew a WHERE a.condicion='" . $lsAux . "')";

                $lsAux = "idDocumento=" . $plIDDoc;
                $lsAux = $lsAux . "|idCondicion=2";
                $lsAux = $lsAux . "|ClasificacionOperativa=" . $this->getValueOfEntities("a.clasificacionoperativa", "FCMainBundle:Reportedeincidentescabecera a", "a.idreporte=" . $plDocNro, null, 0);
                $lsWHERE = $lsWHERE . " AND u.usuario IN (SELECT b.usuario FROM FCMainBundle:Seguridadusuariosnotificacionesnew b WHERE b.condicion='" . $lsAux . "')";

                $lsAux = "idDocumento=" . $plIDDoc;
                $lsAux = $lsAux . "|idCondicion=3";
                $lsAux = $lsAux . "|TipoDeIncidente=" . $this->getValueOfEntities("a.tipodeincidente", "FCMainBundle:Reportedeincidentescabecera a", "a.idreporte=" . $plDocNro, null, 0);
                $lsWHERE = $lsWHERE . " AND u.usuario IN (SELECT c.usuario FROM FCMainBundle:Seguridadusuariosnotificacionesnew c WHERE c.condicion='" . $lsAux . "')";
                break;
            default:
                $this->mTomarCampoClaveYTablaDelDocumento($plIDDoc, $lsCampo, $lsTabla);
                $lsAux = "idDocumento=" . $plIDDoc;
                $lsAux = $lsAux . "|idCondicion=1";
                $lsAux = $lsAux . "|idInstall=" . $this->getValueOfEntities("a.idinstall", 'FCMainBundle:' . $lsTabla . ' a', 'a' . $lsCampo . "=" . $plDocNro, null, 0);
                $lsAux = $lsAux . "|idArea=" . $this->getValueOfEntities("a.idarea", 'FCMainBundle:' . $lsTabla . ' a', 'a' . $lsCampo . "=" . $plDocNro, null, 0);
                $lsWHERE = $lsWHERE . " AND u.usuario IN (SELECT a.usuario FROM FCMainBundle:Seguridadusuariosnotificacionesnew a WHERE a.condicion='" . $lsAux . "')";
                break;
        }

        $dql = "SELECT
                    DISTINCT n.cuenta, u.email, u.sms
                FROM FCMainBundle:Seguridadusuarios u
                    INNER JOIN FCMainBundle:Seguridadusuariosnotificacionesnew n WITH u.usuario=n.usuario
                WHERE n.iddocumento=" . $plIDDoc . " " . $lsWHERE . "
                ORDER BY n.cuenta, u.email, u.sms";
        $result = $this->em->createQuery($dql)->getResult();
        foreach ($result as $data) {
            switch (strtoupper('' . $data["cuenta"])) {
                case "EMAIL":
                    if ($Cuenta == NotificacionesCuentasEnum::Correo) {
                        $lsReceptores = ($data["email"] != "" ? $lsReceptores . $data["email"] . $psSeparador . " " : "");
                    }
                case "SMS":
                    if ($Cuenta == NotificacionesCuentasEnum::SMS) {
                        $lsReceptores = ($data["sms"] != "" ? $lsReceptores . $data["sms"] . $psSeparador . " " : "");
                    }
            }
        }

        /* Ini - Agrega receptores adicionales ========================================== */
        switch ($plIDDoc) {
            case DocumentosNumeradores::DocReporteDeDosimetriasPersonales:
                $lsAux = $this->getValueOfEntities('p.email', 'FCMainBundle:Personal p LEFT JOIN FCMainBundle:Higienereportedosimetriaspersonales r WITH p.cedula=r.cedula', 'r.idreporte=' . $plDocNro, null, '');
                $lsReceptores = ($lsAux != "" ? $lsReceptores . $lsAux . $psSeparador . " " : "");
        }
        /* Fin - Agrega receptores adicionales ========================================== */

        if ($lsReceptores != "") {
            $lsReceptores = trim(substr($lsReceptores, 0, -2));
        }
        return $lsReceptores;
    }

    private function generarIdTabla($tabla, $campo, $planta=1) {
        $dql = 'SELECT
                    MAX(a.' . $campo . ') AS MaxID,
                    MIN(b.nroini) AS MinID
                FROM FCMainBundle:' . $tabla . ' a
                    LEFT JOIN FCMainBundle:Documentosnumeradores b WITH b.idplanta=' . $planta . ' AND b.iddocumento=-1
                WHERE
                    a.' . $campo . ' > (SELECT MIN(c.nroini) FROM FCMainBundle:Documentosnumeradores c
                                WHERE c.idplanta=' . $planta . ' AND c.iddocumento=-1)
                    AND a.' . $campo . ' < (SELECT d.nrofin FROM FCMainBundle:Documentosnumeradores d
								    WHERE d.idplanta=' . $planta . ' AND d.iddocumento=-1)';
        $Result = $this->em->createQuery($dql)->getResult();
        if ($Result) {
            if ($Result[0]['MaxID'] != null) {
                return (int)($Result[0]['MaxID']) + 1;
            } else {
                return (int)($Result[0]['MinID']) + 1;
            }
        } else {
            return null;
        }
    }

    private function mTomarServerNamesAdmitidos() {
        $Return = "";
        try {
            $Servers = $this->em->createQuery("SELECT DISTINCT a.servername FROM FCMainBundle:Plantas a ORDER BY a.servername")->getResult();
            foreach ($Servers as $data) {
                $Return .= ($Return<>"" ? ", " : "") . "'" . $data['servername'] . "'";
            }
        } catch (\Exception $e) {
            $Return .= "'SQLCL9.ANCAP.COM.UY\\ANCAPDB03'";
        }
        return $Return;
    }

    private function mTomarCampoClaveYTablaDelDocumento($plDocumento, &$psCampo, &$psTabla){
        switch ($plDocumento) {
            case DocumentosNumeradores::DocReporteDePeligros:
                $psCampo = "idreporte";
                $psTabla = "Reportedepeligroscabecera";
                break;
            case DocumentosNumeradores::DocReporteDeIncidentes:
                $psCampo = "idreporte";
                $psTabla = "Reportedeincidentescabecera";
                break;
            case DocumentosNumeradores::DocReporteDeInspeccion:
                $psCampo = "idinspeccion";
                $psTabla = "Inspeccionescabecera";
                break;
            case DocumentosNumeradores::DocAtenIncidente:
                $psCampo = "nroatencion";
                $psTabla = "Intervenciones";
                break;
            case DocumentosNumeradores::DocAtenOtra:
                $psCampo = "nroatencion";
                $psTabla = "Intervencionesotras";
                break;
            case DocumentosNumeradores::DocAudi:
                $psCampo = "idaudi";
            $psTabla = "Seguridadauditor";
            break;
            case DocumentosNumeradores::DocMsgClient:
                $psCampo = "idnromsg";
                $psTabla = "Msgclient";
                break;
            case DocumentosNumeradores::DocMsgServer:
                $psCampo = "idnromsg";
            $psTabla = "Msgserver";
            break;
            case DocumentosNumeradores::DocPedidoMAT:
                $psCampo = "idpedido";
                $psTabla = "Pedidoscabecera";
                break;
            case DocumentosNumeradores::DocPedidoROPA:
                $psCampo = "idretiro";
            $psTabla = "Retirosderopacabecera";
            break;
            case DocumentosNumeradores::DocRepAcc:
                $psCampo = "idacc";
                $psTabla = "Accidentes";
                break;
            case DocumentosNumeradores::DocRepInc:
                $psCampo = "idinc";
                $psTabla = "Incidentes";
                break;
            case DocumentosNumeradores::DocRepIrr:
                $psCampo = "idirr";
                $psTabla = "Reporteirregularidades";
                break;
            case DocumentosNumeradores::DocReporteDeDosimetriasPersonales:
                $psCampo = "idreporte";
                $psTabla = "Higienereportedosimetriaspersonales";
                break;
            case DocumentosNumeradores::DocReporteDeMedicionesAmbientales:
                $psCampo = "idreporte";
                $psTabla = "Higienereportemedicionesambientales";
                break;
            case DocumentosNumeradores::DocCIS:
                $psCampo = "idcis";
                $psTabla = "Cis";
                break;
            case DocumentosNumeradores::DocSolicitudCIS:
                $psCampo = "idsolicitud";
                $psTabla = "Solicitudcis";
                break;
            case DocumentosNumeradores::DocValeMAT:
                $psCampo = "idvalemat";
                $psTabla = "Valesderetiro";
                break;
            case DocumentosNumeradores::DocValeROPA:
                $psCampo = "idvaleretiro";
                $psTabla = "Valesderetiroderopa";
                break;
            case DocumentosNumeradores::DocValeMAT_ENTREGA:
                $psCampo = "identrega";
                $psTabla = "Pedidosentregas";
                break;
            case DocumentosNumeradores::DocValeROPA_ENTREGA:
                $psCampo = "identrega";
                $psTabla = "Retirosderopaentregas";
                break;
            case DocumentosNumeradores::DocReporteInterBomberos:
                $psCampo = "idreporte";
                $psTabla = "Reporteinterbomberos";
                break;
            case DocumentosNumeradores::DocReporteDeAcciones:
                $psCampo = "idreporte";
                $psTabla = "Reportedeacciones";
                break;
            case DocumentosNumeradores::DocPreventivoAnual:
                $psCampo = "idprev";
                $psTabla = "Preventivoanualcabecera";
                break;
            case DocumentosNumeradores::DocReporteDeMinutas:
                $psCampo = "idreporte";
                $psTabla = "Reportedeminutas";
                break;
            case DocumentosNumeradores::DocSMPlantilla:
                $psCampo = "idplantilla";
                $psTabla = "SismarPlantillas";
                break;
            case DocumentosNumeradores::DocICPlantilla:
                $psCampo = "idplantilla";
                $psTabla = "Plantillas";
                break;
            case DocumentosNumeradores::DocSMHistoricoAsignacion:
                $psCampo = "idhistorico";
                $psTabla = "SismarFuncionesasignacioneshistorico";
                break;
            case DocumentosNumeradores::DocICHistoricoAsignacion:
                $psCampo = "idhistorico";
                $psTabla = "Funcionesasignacioneshistorico";
                break;
            case DocumentosNumeradores::DocSMRealizada:
                $psCampo = "idrealizada";
                $psTabla = "SismarPlantillasrealizadas";
                break;
            case DocumentosNumeradores::DocICRealizada:
                $psCampo = "idrealizada";
                $psTabla = "Plantillasrealizadas";
                break;
            case DocumentosNumeradores::DocSMRegistraciones:
                $psCampo = "idregistracion";
                $psTabla = "SismarEquiposregistraciones";
                break;
            case DocumentosNumeradores::DocICRegistraciones:
                $psCampo = "idregistracion";
                $psTabla = "Equiposregistraciones";
                break;
            case DocumentosNumeradores::DocReporteDeRecomendaciones:
                $psCampo = "idreporte";
                $psTabla = "RecomendacionesReportecabecera";
                break;
            Default:
                $psCampo = "";
                $psTabla = "";
                return false;
                break;
        }
    }

    private function mDevolverMensajeFormateado($mensaje, $pFormato = NotificacionesFormatsEnum::TextPlain){
        $lsAux = $mensaje;
        switch ($pFormato) {
            case NotificacionesFormatsEnum::HTML:
                $lsAux = $lsAux . "<br /><br />" . 'Usuario: ' . $this->container->get('seguridad')->getUsuario() . "<br />";
                $lsAux = $lsAux . "Estación de Trabajo: Web Application" . "<br />";
                //$lsAux = $lsAux . "Versión: " . $this->request->getParameter('AppVersion') . "\r\n";
                break;
            case NotificacionesFormatsEnum::TextPlain:
                $lsAux = $this->mReemplazarCaracteresEspeciales($lsAux, $pFormato);
                $lsAux = str_replace('<br />', "\r\n", $lsAux);
                $lsAux = str_replace('<a>', '', $lsAux);
                $lsAux = str_replace('</a>', '', $lsAux);
                $lsAux = str_replace('<b>', '', $lsAux);
                $lsAux = str_replace('</b>', '', $lsAux);
                $lsAux = str_replace('<p>', '', $lsAux);
                $lsAux = str_replace('</p>', '', $lsAux);
                $lsAux = str_replace('<span>', '', $lsAux);
                $lsAux = str_replace('</span>', '', $lsAux);

                $lsAux = $lsAux . "\r\n\r\n" . 'Usuario: ' . $this->container->get('seguridad')->getUsuario() . "\r\n";
                $lsAux = $lsAux . "Estacion de Trabajo: Web Application" . "\r\n";
                //$lsAux = $lsAux . "Versión: " . $this->request->getParameter('AppVersion') . "\r\n";
                break;
            default:
                $lsAux = $lsAux . "<br /><br />" . 'Usuario: ' . $this->container->get('seguridad')->getUsuario() . "<br />";
                $lsAux = $lsAux . "Estación de Trabajo: Web Application" . "<br />";
                //$lsAux = $lsAux . "Versión: " . $this->request->getParameter('AppVersion') . "\r\n";
                break;
        }
        return $lsAux;
    }

    private function UCase($value) {
        $lsAux = strtoupper($value);
        $lsAux = str_replace(strtoupper('acute;'), "acute;", $lsAux);
        $lsAux = str_replace(strtoupper("tilde;"), "tilde;", $lsAux);
        $lsAux = str_replace(strtoupper("&iexcl;"), "&iexcl;", $lsAux);
        $lsAux = str_replace(strtoupper("&iquest;"), "&iquest", $lsAux);
        $lsAux = str_replace(strtoupper("&ordf;"), "&ordf;", $lsAux);
        $lsAux = str_replace(strtoupper("&ordm;"), "&ordm;", $lsAux);
        return $lsAux;
    }

    private function mReemplazarCaracteresEspeciales($pHTML, $pFormato = NotificacionesFormatsEnum::TextPlain){
        $lsAux = $pHTML;
        switch ($pFormato) {
            case NotificacionesFormatsEnum::HTML:
                $lsAux = str_replace('á', "&aacute;", $lsAux);
                $lsAux = str_replace('é', "&eacute;", $lsAux);
                $lsAux = str_replace('í', "&iacute;", $lsAux);
                $lsAux = str_replace('ó', "&oacute;", $lsAux);
                $lsAux = str_replace('ú', "&uacute;", $lsAux);
                $lsAux = str_replace('ñ', "&ntilde;", $lsAux);

                $lsAux = str_replace("Á", "&Aacute;", $lsAux);
                $lsAux = str_replace("É", "&Eacute;", $lsAux);
                $lsAux = str_replace("Í", "&Iacute;", $lsAux);
                $lsAux = str_replace("Ó", "&Oacute;", $lsAux);
                $lsAux = str_replace("Ú", "&Uacute;", $lsAux);
                $lsAux = str_replace("Ñ", "&Ntilde;", $lsAux);

                $lsAux = str_replace("¡", "&iexcl;", $lsAux);
                $lsAux = str_replace("¿", "&iquest;", $lsAux);
                $lsAux = str_replace("ª", "&ordf;", $lsAux);
                $lsAux = str_replace("º", "&ordm;", $lsAux);
                break;
            case NotificacionesFormatsEnum::TextPlain:
                $lsAux = str_replace("&aacute;", "a", $lsAux);
                $lsAux = str_replace("&eacute;", "e", $lsAux);
                $lsAux = str_replace("&iacute;", "i", $lsAux);
                $lsAux = str_replace("&oacute;", "o", $lsAux);
                $lsAux = str_replace("&uacute;", "u", $lsAux);
                $lsAux = str_replace("&ntilde;", "n", $lsAux);

                $lsAux = str_replace("&Aacute;", "A", $lsAux);
                $lsAux = str_replace("&Eacute;", "E", $lsAux);
                $lsAux = str_replace("&Iacute;", "I", $lsAux);
                $lsAux = str_replace("&Oacute;", "O", $lsAux);
                $lsAux = str_replace("&Uacute;", "U", $lsAux);
                $lsAux = str_replace("&Ntilde;", "N", $lsAux);

                $lsAux = str_replace("&iexcl;", "¡", $lsAux);
                $lsAux = str_replace("&iquest;", "¿", $lsAux);
                $lsAux = str_replace("&ordf;", "ª", $lsAux);
                $lsAux = str_replace("&ordm;", "º", $lsAux);
                break;
            default:
                $lsAux = $lsAux;
                break;
        }
        return $lsAux;
    }

    private function mValidarEnvioDeNotificacion($plIDDoc, $pbNuevoRegistro) {
        if ($pbNuevoRegistro==true) {
            return true;
        }
        else {
            $lsParameter = "";
            switch ($plIDDoc) {
                case DocumentosNumeradores::DocReporteDePeligros:
                    $lsParameter = "ReporteDePeligros";
                    break;
                case DocumentosNumeradores::DocReporteDeIncidentes:
                    $lsParameter = "ReporteDeIncidentes";
                    break;
                case DocumentosNumeradores::DocReporteDeInspeccion:
                    $lsParameter = "ReporteDeInspecciones";
                    break;
                case DocumentosNumeradores::DocAtenIncidente:
                    $lsParameter = "AtencionesIncidentes";
                    break;
                case DocumentosNumeradores::DocReporteDeRecomendaciones:
                    $lsParameter = "ReporteDeRecomendaciones";
                    break;
                case DocumentosNumeradores::DocReporteDeAcciones:
                    $lsParameter = "ReporteDeAcciones";
                    break;
                case DocumentosNumeradores::DocReporteInterBomberos:
                    $lsParameter = "ReporteInterBomberos";
                    break;
                case DocumentosNumeradores::DocReporteDeMinutas:
                    $lsParameter = "ReporteDeMinutas";
                    break;
                default:
                    return false;
                    break;
            }

            if ($lsParameter != "") {
                if (strtoupper($this->getValueOfEntities("a.value", "FCMainBundle:Seguridadsistemasparametrizacion a", "a.typeparameter='RE:Notify' AND a.idparameter='" . $lsParameter . "'")) == "SI") {
                    return true;
                } else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
    }

    private function FormatFieldForQry($psField, $pFormat = CDataFormatsField, $psAliasField = "", $pbAplicarAlias = true) {
        if ($psAliasField=="") {
            if (strpos($psField, '.')>0) {
                $psAliasField = substr($psField,strpos($psField, '.')+1);
            }
            else {
                $psAliasField = $psField;
            }
        }

        switch ($pFormat) {
            Case CDataFormatsField::CDataFechaHora:
                $lsAux = "CONVERT(VARCHAR(10), " . $psField . ", 103) + ' ' + CONVERT(VARCHAR(5), " . $psField . ", 108)";
                break;
            Case CDataFormatsField::CDataHora:
                $lsAux = "CONVERT(VARCHAR(5), " . $psField . ", 108)";
                break;
            Case CDataFormatsField::CDataMoneda:
                $lsAux = "('$ ' + REPLACE(CAST(CAST(" . $psField . " AS DECIMAL(20,2)) AS VARCHAR(30)), '.', ','))";
                break;
            Case CDataFormatsField::CDataFecha:
                $lsAux = "CONVERT(VARCHAR(10), " . $psField . ", 103)";
                break;
            Case CDataFormatsField::CDataFechaMes:
                $lsAux = "DATEPART(mm, " . $psField . ")";
                break;
            Case CDataFormatsField::CDataFechaMesAnio:
                $lsAux = "RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(mm, " . $psField . ")), 2) + '/' + CONVERT(VARCHAR(4), DATEPART(yyyy, " . $psField . "))";
                break;
            Case CDataFormatsField::CDataFechaAnioMes:
                $lsAux = "CONVERT(VARCHAR(4), DATEPART(yyyy, " . $psField . ")) + '/' + RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(mm, " . $psField . ")), 2)";
                break;
            Case CDataFormatsField::CDataFechaAnio:
                $lsAux = "DATEPART(yyyy, " . $psField . ")";
                break;
            Case CDataFormatsField::CDataFechaTrimestre:
                $lsAux = "DATEPART(qq, " . $psField . ")";
                break;
            Case CDataFormatsField::CDataFechaTrimestreAnio:
                $lsAux = "'0' + CONVERT(VARCHAR(1), DATEPART(qq, " . $psField . ")) + '/' + CONVERT(VARCHAR(4), DATEPART(yyyy, " . $psField . "))";
                break;
            Case CDataFormatsField::CDataTextoCorto:
                $lsAux = "CAST(" . $psField . " AS VARCHAR(8))";
                break;
            Case CDataFormatsField::CDataTextoLargo:
                $lsAux = "CAST(" . $psField . " AS VARCHAR(255))";
                break;
        }

        if ($pbAplicarAlias) {
            $lsAux = $lsAux . " AS " . $psAliasField;
        }
        return $lsAux;
    }

    private function mTomarFechaDelReporte($plIDDoc, $plDocNro) {
        switch ($plIDDoc) {
            Case DocumentosNumeradores::DocReporteDePeligros:
                return date_format($this->getValueOfEntities("a.fecha", "FCMainBundle:Reportedepeligroscabecera a", "a.idreporte=" . $plDocNro), "d/m/Y");
                break;
            Case DocumentosNumeradores::DocReporteClasificadoDeIncendios:
                return date_format($this->getValueOfEntities("a.fecha", "FCMainBundle:Reportedepeligroscabecera a", "a.idreporte=" . $plDocNro), "d/m/Y");
                break;
            Case DocumentosNumeradores::DocReporteDeIncidentes:
                return date_format($this->getValueOfEntities("a.fecha", "FCMainBundle:Reportedeincidentescabecera a", "a.idreporte=" . $plDocNro), "d/m/Y H:i");
                break;
            Case DocumentosNumeradores::DocReporteDeIncidentesClasificacion:
                return date_format($this->getValueOfEntities("a.fecha", "FCMainBundle:Reportedeincidentescabecera a", "a.idreporte=" . $plDocNro), "d/m/Y H:i");
                break;
            Case DocumentosNumeradores::DocReporteDeInspeccion:
                return date_format($this->getValueOfEntities("a.fecha", "FCMainBundle:Inspeccionescabecera a", "a.idinspeccion=" . $plDocNro), "d/m/Y");
                break;
            Case DocumentosNumeradores::DocAtenIncidente:
                return date_format($this->getValueOfEntities("CONCAT(a.fecha, ' ', a.hora)", "FCMainBundle:Intervenciones a", "a.nroatencion=" . $plDocNro), "d/m/Y H:i");
                break;
            case DocumentosNumeradores::DocReporteDeDosimetriasPersonales:
                return date_format($this->getValueOfEntities("a.fecha", "FCMainBundle:Higienereportedosimetriaspersonales a", "a.idreporte=" . $plDocNro), "d/m/Y");
                break;
            case DocumentosNumeradores::DocReporteDeMedicionesAmbientales:
                return date_format($this->getValueOfEntities("a.fechahora", "FCMainBundle:Higienereportemedicionesambientales a", "a.idreporte=" . $plDocNro), "d/m/Y H:i");
                break;
            default:
                return "";
                break;
        }
    }

    private function mTomarCuerpoMensaje($plIDDoc, $plDocNro) {
        $Retorna = "";
        switch ($plIDDoc) {
            Case DocumentosNumeradores::DocReporteDePeligros:
                $sql="SELECT
                          RC.UserLog, SU.Nombre AS DesUserLog, " . $this->FormatFieldForQry("RC.FechaProc", CDataFormatsField::CDataFechaHora) . ", MI.Descripcion AS DesInstall, MA.Descripcion AS DesArea, MC.Descripcion AS DesClasif, RC.LugarEquipo, RC.Observacion
                      FROM ReporteDePeligrosCabecera AS RC
                          LEFT JOIN SeguridadUsuarios AS SU ON SU.Usuario=RC.UserLog
                          LEFT JOIN Instalaciones AS MI ON MI.idInstall=RC.idInstall
                          LEFT JOIN Areas AS MA ON MA.idArea=RC.idArea
                          LEFT JOIN ReporteDePeligrosClasificaciones AS MC ON MC.idClasif=RC.idClasif
                      WHERE RC.idReporte=$plDocNro";
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                foreach($result as $data) {
                    $Retorna .= "Planta Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($this->IdPlanta . ' - ' . $this->DesPlanta, NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Usuario Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["UserLog"] . ' - ' . $data["DesUserLog"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Fecha Reportada: " . $data["FechaProc"] . "<br /><br />";
                    $Retorna .= "Instalaci&oacute;n: " . ($this->mReemplazarCaracteresEspeciales($data["DesInstall"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "&Aacute;rea: " . ($this->mReemplazarCaracteresEspeciales($data["DesArea"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Clasificaci&oacute;n: " . ($this->mReemplazarCaracteresEspeciales($data["DesClasif"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Lugar / Equipo: " . ($this->mReemplazarCaracteresEspeciales($data["LugarEquipo"], NotificacionesFormatsEnum::HTML)) . "<br /><br />";
                    $Retorna .= "Descripci&oacute;n de los hechos: <br />" . ($this->mReemplazarCaracteresEspeciales($data["Observacion"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    break;
                }
                return $Retorna;
                break;
            Case DocumentosNumeradores::DocReporteDeIncidentes:
                $sql="SELECT
                       RC.UserLog, SU.Nombre AS DesUserLog, " . $this->FormatFieldForQry("RC.FechaProc", CDataFormatsField::CDataFechaHora) . ", MI.Descripcion AS DesInstall, MA.Descripcion AS DesArea, RC.LugarEquipo, RC.DescripcionIncidente, RT.ApeNom AS DesRespTrabajo, T1.ApeNom AS DesTestigo1, T2.ApeNom AS DesTestigo2, ME.Descripcion AS DesEvento
                    FROM ReporteDeIncidentesCabecera AS RC
                       LEFT JOIN SeguridadUsuarios AS SU ON SU.Usuario=RC.UserLog
                       LEFT JOIN Instalaciones AS MI ON MI.idInstall=RC.idInstall
                       LEFT JOIN Areas AS MA ON MA.idArea=RC.idArea
                       LEFT JOIN Eventos AS ME ON ME.idEvento=RC.idEvento
                       LEFT JOIN Personal AS RT ON RT.Cedula=RC.idRespTrabajo
                       LEFT JOIN Personal AS T1 ON T1.Cedula=RC.idTestigo1
                       LEFT JOIN Personal AS T2 ON T2.Cedula=RC.idTestigo2
                    WHERE RC.idReporte=".$plDocNro;
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                foreach($result as $data) {
                    $Retorna .= "Planta Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($this->IdPlanta . ' - ' . $this->DesPlanta, NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Usuario Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["UserLog"] . ' - ' . $data["DesUserLog"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Fecha Reportada: " . $data["FechaProc"] . "<br /><br />";
                    $Retorna .= "Instalaci&oacute;n: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesInstall"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "&Aacute;rea: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesArea"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Lugar / Equipo: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["LugarEquipo"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Evento: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesEvento"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Responsable del Trabajo: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesRespTrabajo"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Testigo 1: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesTestigo1"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Testigo 2: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesTestigo2"], NotificacionesFormatsEnum::HTML)) . "<br /><br />";
                    $Retorna .= "Descripci&oacute;n de los hechos:<br />" . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DescripcionIncidente"], NotificacionesFormatsEnum::HTML)) . "<br /><br />";
                    break;
                }

                $sql="SELECT
                       RD.Cedula, MP.ApeNom AS Nombre, ME.Nombre AS DesEmpresa, MC.Descripcion AS DesClasif, RD.Tarea
                    FROM ReporteDeIncidentesPersonas AS RD
                       LEFT JOIN Personal AS MP ON MP.Cedula=RD.Cedula
                       LEFT JOIN Empresas AS ME ON ME.idEmpresa=MP.idEmpresa
                       LEFT JOIN Clasificaciones AS MC ON MC.idClasif=RD.idClasif
                    WHERE RD.idReporte=".$plDocNro."
                    ORDER BY RD.idItem";
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                if ($result) {
                    $Retorna .= "<u><b>Personas Lesionadas:</b></u><br />";
                    foreach($result as $data) {
                        $Retorna .= "   C&eacute;dula: " . $this->mReemplazarCaracteresEspeciales($data["Cedula"], NotificacionesFormatsEnum::HTML) . "<br />";
                        $Retorna .= "   Apellido y Nombre: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["Nombre"], NotificacionesFormatsEnum::HTML)) . "<br />";
                        $Retorna .= "   Relaci&oacute;n Laboral: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesEmpresa"], NotificacionesFormatsEnum::HTML)) . "<br />";
                        $Retorna .= "   Clasificaci&oacute;n: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesClasif"], NotificacionesFormatsEnum::HTML)) . "<br />";
                        $Retorna .= "   Tarea: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["Tarea"], NotificacionesFormatsEnum::HTML)) . "<br /><br />";
                    }
                }

                $sql="SELECT
                       RD.Dano AS DesDano, ME.Nombre AS DesPropietario, MV.Descripcion AS DesValorEstimado
                    FROM ReporteDeIncidentesMateriales AS RD
                       LEFT JOIN Empresas AS ME ON ME.idEmpresa=RD.idPropietario
                       LEFT JOIN EstMatIni AS MV ON MV.idEstMatIni=RD.idValorEstimado
                    WHERE RD.idReporte=".$plDocNro."
                    ORDER BY RD.idItem";
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                if ($result) {
                    $Retorna .= "<b>Perdidas Materiales:</b><br />";
                    foreach($result as $data) {
                        $Retorna .= "   Bien Da&ntilde;ado: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesDano"], NotificacionesFormatsEnum::HTML)) . "<br />";
                        $Retorna .= "   Propietario: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesPropietario"], NotificacionesFormatsEnum::HTML)) . "<br />";
                        $Retorna .= "   Valor Estimado: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesValorEstimado"], NotificacionesFormatsEnum::HTML)) . "<br /><br />";
                    }
                }
                return $Retorna;
                break;
            case DocumentosNumeradores::DocReporteDeIncidentesClasificacion:
                $sql =
                "SELECT
                    RC.UserLog,
                    SU.Nombre AS DesUserLog, " . $this->FormatFieldForQry("RC.FechaProc", CDataFormatsField::CDataFechaHora) . ", MI.Descripcion AS DesInstall, MA.Descripcion AS DesArea, RC.LugarEquipo,
                    ME.Descripcion AS DesEvento,
                    CASE WHEN RC.ClasificacionOperativa=" . ClasificacionOperativaEnum::ClasifOperativo . " THEN 'Operativo' WHEN RC.ClasificacionOperativa=" . ClasificacionOperativaEnum::ClasifNoOperativo . " THEN 'No Operativo' ELSE '' END AS DesClasificacionOperativa,
                    CASE WHEN RC.TipoDeIncidente=" . TipoDeIncidenteEnum::TipoGrave . " THEN 'Grave' WHEN RC.TipoDeIncidente=" . TipoDeIncidenteEnum::TipoSignificativo . " THEN 'Significativo'  WHEN RC.TipoDeIncidente=" . TipoDeIncidenteEnum::TipoMenor . " THEN 'Menor' ELSE '' END AS DesTipoDeIncidente,
                    RC.DescripcionIncidente
                FROM ReporteDeIncidentesCabecera AS RC
                    LEFT JOIN SeguridadUsuarios AS SU ON SU.Usuario=RC.UserLog
                    LEFT JOIN Instalaciones AS MI ON MI.idInstall=RC.idInstall
                    LEFT JOIN Areas AS MA ON MA.idArea=RC.idArea
                    LEFT JOIN Eventos AS ME ON ME.idEvento=RC.idEvento
                    LEFT JOIN Personal AS RT ON RT.Cedula=RC.idRespTrabajo
                WHERE RC.idReporte=" . $plDocNro;
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                foreach($result as $data) {
                    $Retorna .= "Planta Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($this->IdPlanta . ' - ' . $this->DesPlanta, NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Usuario Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["UserLog"] . " - " . $data["DesUserLog"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Fecha Reportada: " . $this->mReemplazarCaracteresEspeciales($data["FechaProc"], NotificacionesFormatsEnum::HTML) . "<br /><br />";
                    $Retorna .= "Usted tiene pendiente realizar la investigación del siguiente incidente: " . "<br />";
                    $Retorna .= "Instalación: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesInstall"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Area: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesArea"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Lugar / Equipo: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["LugarEquipo"], NotificacionesFormatsEnum::HTML)) . "<br /><br />";
                    $Retorna .= "Evento: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesEvento"], NotificacionesFormatsEnum::HTML)) . "<br /><br />";
                    $Retorna .= "Clasificación: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesClasificacionOperativa"] . ", " . $data["DesTipoDeIncidente"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Descripción de los hechos:" . "<br />" . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DescripcionIncidente"], NotificacionesFormatsEnum::HTML)) . "<br /><br />";
                    break;
                }
                return $Retorna;
                break;
            Case DocumentosNumeradores::DocReporteDeInspeccion:
                $sql="SELECT
                       RC.UserLog, SU.Nombre AS DesUserLog, " . $this->FormatFieldForQry("RC.FechaProc", CDataFormatsField::CDataFechaHora) . ", MI.Descripcion AS DesInstall, MA.Descripcion AS DesArea, RC.MotivoInspDescripcion AS DesMotivoInsp
                    FROM InspeccionesCabecera AS RC
                       LEFT JOIN SeguridadUsuarios AS SU ON SU.Usuario=RC.UserLog
                       LEFT JOIN Instalaciones AS MI ON MI.idInstall=RC.idInstall
                       LEFT JOIN Areas AS MA ON MA.idArea=RC.idArea
                    WHERE RC.idInspeccion=".$plDocNro;
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                foreach($result as $data) {
                    $Retorna .= "Planta Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($this->IdPlanta . ' - ' . $this->DesPlanta, NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Usuario Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["UserLog"] . ' - ' . $data["DesUserLog"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Fecha Reportada: " . $data["FechaProc"] . "<br /><br />";
                    $Retorna .= "Instalaci&oacute;n: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesInstall"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Area: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesArea"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "M&oacute;tivo: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesMotivoInsp"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    break;
                }
                return $Retorna;
                break;
            Case DocumentosNumeradores::DocSolicitudCIS:
                $sql="SELECT
                       RC.WinUser, " . $this->FormatFieldForQry("RC.FechaProc", CDataFormatsField::CDataFechaHora) . ", RC.idSolicitante, PE.ApeNom AS DesSolicitante, " . $this->FormatFieldForQry("RC.Comienza", CDataFormatsField::CDataFechaHora) . ", " . $this->FormatFieldForQry("RC.Finaliza", CDataFormatsField::CDataFechaHora) . ", RC.NroSAP, RC.TituloSAP, MI.Descripcion AS DesInstall, MA.Descripcion AS DesArea, RC.LugarEquipo, RC.Herramientas, RC.Comentarios
                    FROM SolicitudCIS AS RC
                       LEFT JOIN Personal AS PE ON PE.Cedula=RC.idSolicitante
                       LEFT JOIN Instalaciones AS MI ON MI.idInstall=RC.idInstall
                       LEFT JOIN Areas AS MA ON MA.idArea=RC.idArea
                    WHERE RC.idSolicitud=$plDocNro";
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                foreach($result as $data) {
                    $Retorna .= "Planta Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($this->IdPlanta . ' - ' . $this->DesPlanta, NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Usuario Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["WinUser"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Fecha Reportada: " . $data["FechaProc"] . "<br /><br />";
                    $Retorna .= "Solicitante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["idSolicitante"] . " " . $data["DesSolicitante"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Comienza: " . $data["Comienza"] . "<br />";
                    $Retorna .= "Finaliza: " . $data["Finaliza"] . "<br /><br />";
                    $Retorna .= "Nro SAP: " . $data["NroSAP"] . "<br />";
                    $Retorna .= "Tarea a Realizar: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["TituloSAP"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Instalaci&oacute;n: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesInstall"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "&Aacute;rea: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["DesArea"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Lugar / Equipo: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["LugarEquipo"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Herramientas: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["Herramientas"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Comentarios: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["Comentarios"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    break;
                }
                return $Retorna;
                break;
            Case DocumentosNumeradores::DocReporteDeDosimetriasPersonales:
                $sql="SELECT
                          RC.UserLog, SU.Nombre AS DesUserLog, " . $this->FormatFieldForQry("RC.FechaProc", CDataFormatsField::CDataFechaHora) . ", MI.Descripcion AS DesInstall, MA.Descripcion AS DesArea, RC.idDosimetro, RC.Cedula, MP.ApeNom, MC.Descripcion AS DesCargo 
                      FROM HigieneReporteDosimetriasPersonales AS RC
                          LEFT JOIN SeguridadUsuarios AS SU ON SU.Usuario=RC.UserLog
                          LEFT JOIN Instalaciones AS MI ON MI.idInstall=RC.idInstall
                          LEFT JOIN Areas AS MA ON MA.idArea=RC.idArea
                          LEFT JOIN Personal AS MP ON MP.Cedula=RC.Cedula
                          LEFT JOIN Cargos AS MC ON MC.idCargo=RC.idCargo
                      WHERE RC.idReporte=$plDocNro";
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                foreach($result as $data) {
                    $Retorna .= "Planta Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($this->IdPlanta . ' - ' . $this->DesPlanta, NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Usuario Reportante: " . $this->UCase($this->mReemplazarCaracteresEspeciales($data["UserLog"] . ' - ' . $data["DesUserLog"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Fecha Reportada: " . $data["FechaProc"] . "<br /><br />";
                    $Retorna .= "Instalaci&oacute;n: " . ($this->mReemplazarCaracteresEspeciales($data["DesInstall"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "&Aacute;rea: " . ($this->mReemplazarCaracteresEspeciales($data["DesArea"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Id. Dos&iacute;metro: " . ($this->mReemplazarCaracteresEspeciales($data["idDosimetro"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Persona que utiliz&oacute; el Dos&iacute;metro: " . ($data["Cedula"] . ' - '. $this->mReemplazarCaracteresEspeciales($data["ApeNom"], NotificacionesFormatsEnum::HTML)) . "<br />";
                    $Retorna .= "Cargo: " . ($this->mReemplazarCaracteresEspeciales($data["DesCargo"], NotificacionesFormatsEnum::HTML)) . "<br /><br />";
                    break;
                }

                $sql="SELECT
                       MC.Descripcion, RD.Valor
                    FROM HigieneMaestroCondicionesAmbientales AS MC
                       LEFT JOIN HigieneReporteDosimetriasPersonalesCondAmb AS RD ON MC.idCondicionAmbiental=RD.idItem
                    WHERE RD.idReporte=".$plDocNro."
                    ORDER BY MC.idCondicionAmbiental";
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                if ($result) {
                    $Retorna .= "<u><b>Condiciones Ambientales:</b></u><br />";
                    foreach($result as $data) {
                        $Retorna .= "   ". $this->mReemplazarCaracteresEspeciales($data["Descripcion"], NotificacionesFormatsEnum::HTML) .": " . $this->mReemplazarCaracteresEspeciales($data["Valor"], NotificacionesFormatsEnum::HTML) . "<br />";
                    }
                }

                $sql="SELECT
                       MR.idResultado, MR.Descripcion, RD.Valor
                    FROM HigieneMaestroTiposResultados AS MR
                       LEFT JOIN HigieneReporteDosimetriasPersonalesResultados AS RD ON MR.idResultado=RD.idItem
                    WHERE RD.idReporte=".$plDocNro."
                    ORDER BY MR.idResultado";
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();

                $tlv = "";
                $ldHexano = 0;
                $ldBenceno = 0;
                $ldTolueno = 0;
                $ldXileno = 0;
                if ($result) {
                    $Retorna .= "<u><b>Resultados:</b></u><br />";
                    foreach($result as $data) {
                        $Retorna .= "   ". $this->mReemplazarCaracteresEspeciales($data["Descripcion"], NotificacionesFormatsEnum::HTML) .": " . $this->mReemplazarCaracteresEspeciales($data["Valor"], NotificacionesFormatsEnum::HTML) . "<br />";
                        switch (true) {
                            case $data['idResultado'] == 1: //Benceno
                                $ldBenceno = floatval(str_replace(",", ".", $data['Valor']))/0.5;
                                break;
                            case $data['idResultado'] == 2: //Hexano
                                $ldHexano = floatval(str_replace(",", ".", $data['Valor']))/50;
                                break;
                            case $data['idResultado'] == 3: //Tolueno
                                $ldTolueno = floatval(str_replace(",", ".", $data['Valor']))/20;
                                break;
                            case $data['idResultado'] == 4: //Xileno
                                $ldXileno = floatval(str_replace(",", ".", $data['Valor']))/100;
                                break;
                        }
                    }
                }
                if ($ldHexano != 0 || $ldBenceno != 0 || $ldTolueno != 0 || $ldXileno != 0){
                    $tlv = $ldHexano + $ldBenceno + $ldTolueno + $ldXileno;
                }
                $Retorna .= "   TLV Mezcla: " . $this->mReemplazarCaracteresEspeciales($tlv, NotificacionesFormatsEnum::HTML) . "<br />";

                return $Retorna;
                break;
            default:
                return "";
                break;
        }
    }
    public function gGenerarNotificacionRecomendarionesAResponsables($plIDDoc, $plDocNro, $pGrid, $plObsNro=0) {
        $lsMensajeReporte = null;
        switch ($plIDDoc) {
            case DocumentosNumeradores::DocReporteDePeligros:
                $lsParameter = "ReporteDePeligros";
                $lsMensajeReporte = $this->mTomarCuerpoMensajeRecomendacion($plIDDoc, $plDocNro);
                $lsAsunto = "Reporte de Peligros - Recomendaciones";
                break;
            case DocumentosNumeradores::DocReporteDeIncidentes:
                $lsParameter = "ReporteDeIncidentes";
                $lsMensajeReporte = $this->mTomarCuerpoMensajeRecomendacion($plIDDoc, $plDocNro);
                $lsAsunto = "Reporte de Incidentes - Recomendaciones";
                break;
            case DocumentosNumeradores::DocReporteDeInspeccion:
                $lsParameter = "ReporteDeInspecciones";
                $lsMensajeReporte = $this->mTomarCuerpoMensajeRecomendacion($plIDDoc, $plDocNro, $plObsNro);
                $lsAsunto = "SistemaSEG - Recomendaciones de Seguridad Industrial";
                break;
            default:
                break;
        }
        if (strtoupper($this->getValueOfEntities("a.value", "FCMainBundle:Seguridadsistemasparametrizacion a", "a.typeparameter='Responsable:Notify' AND a.idparameter='".$lsParameter."'")) == "SI") {
            //ordeno por mails
            $mailSorted = [];
            while ($pGrid) {
                $mailVar = $pGrid[0]['rec_mail'];
                foreach ($pGrid as $key => $rec1) {
                    if($rec1['rec_mail'] == $mailVar){
                        $mailSorted[$mailVar][] = $rec1;
                        unset($pGrid[$key]);
                    }
                }
            }
            
            //imapacto los mails
            foreach ($mailSorted as $mail => $arrRecs) {
                $receptor = $mail;
                $lsMensaje = $lsMensajeReporte."<br>";
                $lsMensaje .= "<b><u>Recomendaciones (por favor realizar los avisos SAP que correspondan):</u></b><br>";
                foreach ($arrRecs as $rec) {
                    if($rec['rec_notificacion']==1){
                        $lsMensaje .= "(NUEVA)<br>";
                    }else{
                        $lsMensaje .= "(MODIFICACIÓN)<br>";
                    }
                    $lsMensaje .= "Recomendación: ".$rec['rec_recomendacion']."<br>";
                    $lsMensaje .= "Responsable: ".$rec['rec_nro_responsable']." - ".$rec['rec_nombre_responsable']."<br>";
                    $plazo = new \DateTime ($rec['rec_plazo']);
                    $lsMensaje .= "Fecha Comprometida: ".$plazo->format('d/m/Y')."<br><br>";
                }

                $this->gGenerarNotificacionSimple($receptor, $lsAsunto, $lsMensaje);
            }
            $this->gRealizarEnvioDeCorreos();
        }
    }
    
    private function mTomarCuerpoMensajeRecomendacion($plIDDoc, $plDocNro, $plObsNro=0){
        $retorna = "";
        switch ($plIDDoc) {
            case DocumentosNumeradores::DocReporteDePeligros:
                $sql="SELECT RC.Fecha, RC.UserLog, SU.Nombre AS DesUserLog, RC.UserLogInsp, SI.Nombre AS DesUserLogInsp, RC.EvRiesgoKBC, RC.FechaProc, MI.Descripcion AS DesInstall, MA.Descripcion AS DesArea, MC.Descripcion AS DesClasif, RC.LugarEquipo, RC.Observacion 
                    FROM ReporteDePeligrosCabecera AS RC 
                        LEFT JOIN SeguridadUsuarios AS SU ON SU.Usuario=RC.UserLog 
                        LEFT JOIN SeguridadUsuarios AS SI ON SI.Usuario=RC.UserLogInsp 
                        LEFT JOIN Instalaciones AS MI ON MI.idInstall=RC.idInstall 
                        LEFT JOIN Areas AS MA ON MA.idArea=RC.idArea 
                        LEFT JOIN ReporteDePeligrosClasificaciones AS MC ON MC.idClasif=RC.idClasif 
                    WHERE RC.idReporte=".$plDocNro;
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $data = $result[0];

                $retorna = "Estimada/o:<br />";
                $retorna .= "Se ha generado un Reporte de Peligro/Observación de Seguridad correspondiente a su área de trabajo. Se adjunta detalle de la situación detectada y recomendaciones correspondientes. En caso que desee realizar alguna observación, agradecemos comunicarse con Seguridad Industrial.<br /><br />";
                $retorna .= "<b><u>Datos del Reporte</u></b><br />";
                $retorna .= "Fecha: ".$data["Fecha"]."<br />";
                $retorna .= "Número: ".$plDocNro."<br />";
                $retorna .= "Planta Reportante: ".strtoupper($this->IdPlanta.' - '.$this->DesPlanta)."<br />";
                $retorna .= "Usuario Reportante: ".strtoupper($data["UserLog"].' - '.$data["DesUserLog"])."<br />";
                $retorna .= "Fecha Reportada: ".$data["FechaProc"]."<br /><br />";
                $retorna .= "Instalación: ".strtoupper(''.$data["DesInstall"])."<br />";
                $retorna .= "Area: ".strtoupper(''.$data["DesArea"])."<br />";
                $retorna .= "Clasificación: ".strtoupper(''.$data["DesClasif"])."<br />";
                $retorna .= "Lugar / Equipo: ".strtoupper(''.$data["LugarEquipo"])."<br /><br />";
                $retorna .= "Descripción de los hechos:<br />".strtoupper(''.$data["Observacion"])."<br /><br />";
                $retorna .= "<b><u>Datos de la Evaluación</u></b><br />";
                $retorna .= "Analizado por: ".strtoupper($data["UserLogInsp"].' - '.$data["DesUserLogInsp"])."<br />";
                $retorna .= "Matriz de Riesgo: ".$data["EvRiesgoKBC"]."<br />";
                $retorna .= "Color: ".$this->mTomarColorEvRiesgoMatriz(''.$data["EvRiesgoKBC"])."<br />";
                break;
            case DocumentosNumeradores::DocReporteDeIncidentes:
                $sql="SELECT RC.Fecha, RC.UserLog, SU.Nombre AS DesUserLog, RC.UserLogInsp, SI.Nombre AS DesUserLogInsp, RC.EvRiesgoKBC, RC.FechaProc, MI.Descripcion AS DesInstall, MA.Descripcion AS DesArea, RC.LugarEquipo, RC.DescripcionIncidente 
                    FROM ReporteDeIncidentesCabecera AS RC 
                        LEFT JOIN SeguridadUsuarios AS SU ON SU.Usuario=RC.UserLog 
                        LEFT JOIN SeguridadUsuarios AS SI ON SI.Usuario=RC.UserLogInsp 
                        LEFT JOIN Instalaciones AS MI ON MI.idInstall=RC.idInstall 
                        LEFT JOIN Areas AS MA ON MA.idArea=RC.idArea 
                    WHERE RC.idReporte=".$plDocNro;
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll();
                $data = $result[0];

                $retorna = "Estimada/o:<br />";
                $retorna .= "Se ha generado un Reporte de Peligro/Observación de Seguridad correspondiente a su área de trabajo. Se adjunta detalle de la situación detectada y recomendaciones correspondientes. En caso que desee realizar alguna observación, agradecemos comunicarse con Seguridad Industrial.<br /><br />";
                $retorna .= "<b><u>Datos del Reporte</u></b><br />";
                $retorna .= "Fecha: ".$data["Fecha"]."<br />";
                $retorna .= "Número: ".$plDocNro."<br />";
                $retorna .= "Planta Reportante: ".strtoupper($this->IdPlanta.' - '.$this->DesPlanta)."<br />";
                $retorna .= "Usuario Reportante: ".strtoupper($data["UserLog"].' - '.$data["DesUserLog"])."<br />";
                $retorna .= "Fecha Reportada: ".$data["FechaProc"]."<br /><br />";
                $retorna .= "Instalación: ".strtoupper(''.$data["DesInstall"])."<br />";
                $retorna .= "Area: ".strtoupper(''.$data["DesArea"])."<br />";
                $retorna .= "Lugar / Equipo: ".strtoupper(''.$data["LugarEquipo"])."<br /><br />";
                $retorna .= "Descripción de los hechos:<br />".strtoupper(''.$data["DescripcionIncidente"])."<br /><br />";
                $retorna .= "<b><u>Datos de la Evaluación</u></b><br />";
                $retorna .= "Analizado por: ".strtoupper($data["UserLogInsp"].' - '.$data["DesUserLogInsp"])."<br />";
                $retorna .= "Matriz de Riesgo: ".$data["EvRiesgoKBC"]."<br />";
                $retorna .= "Color: ".$this->mTomarColorEvRiesgoMatriz(''.$data["EvRiesgoKBC"])."<br />";
                break;
            case DocumentosNumeradores::DocReporteDeInspeccion:
                $retorna = "";
                $idinstall = $this->getValueOfEntities('a.idinstall', 'FCMainBundle:Inspeccionescabecera a', 'a.idinspeccion = '.$plDocNro);
                $idarea = $this->getValueOfEntities('a.idarea', 'FCMainBundle:Inspeccionescabecera a', 'a.idinspeccion = '.$plDocNro);
                $idmotivo = $this->getValueOfEntities('a.idmotivoinsp', 'FCMainBundle:Inspeccionescabecera a', 'a.idinspeccion = '.$plDocNro);
                $install = $this->getValueOfEntities('a.descripcion', 'FCMainBundle:Instalaciones a', 'a.idinstall = '.$idinstall);
                $area = $this->getValueOfEntities('a.descripcion', 'FCMainBundle:Areas a', 'a.idarea = '.$idarea);
                $motivo = $this->getValueOfEntities('a.descripcion', 'FCMainBundle:Motivosinspeccion a', 'a.idmotivoinsp = '.$idmotivo);
                $observacion = $this->getValueOfEntities('a.descripobs', 'FCMainBundle:Inspeccionesdetalle a', 'a.idinspeccion = '.$plDocNro.' AND a.idobservacion = '.$plObsNro);
                $retorna .= "Reporte de Inspección Nro.: ".$plDocNro."<br>";
                $retorna .= "Instalación: ".$install."<br>";
                $retorna .= "Area: ".$area."<br>";
                $retorna .= "Motivo: ".$motivo."<br><br>";
                $retorna .= "Observación:<br> ".$observacion."<br><br>";
                break;
            default:
                break;
        }
        return $retorna;
    }
    private function mTomarColorEvRiesgoMatriz($value){
        switch ($value) {
            case "A3":
            case "A4":
            case "A5":
            case "B4":
            case "B5":
            case "C5":
                $color = "Rojo (alto)";
                break;
            case "A1":
            case "A2":
            case "B2":
            case "B3":
            case "C3":
            case "C4":
            case "D4":
            case "D5":
            case "E5":
                $color = "Azul (medio)";
                break;
            default:
                $color = "Amarillo (bajo)";
                break;
        }
        return $color;
    }

    private function mEnviarNotificacionEMAIL($plIDDoc, $plDocNro, $psFechaHora, $psDocDes, $psAgrearAlAsunto = '') {
        if (!($this->ModoDeveloper)) {
            $lsSeparador = $this->getValueOfEntities("a.unirconseparador", "FCMainBundle:Seguridadenviodecorreoparametrizacion a", "a.sistema='" . $this->Sistema . "'");
            $lsReceptores = $this->gTomarReceptoresDelDocumento($plIDDoc, $plDocNro, $lsSeparador, NotificacionesCuentasEnum::Correo);
            If (strlen($lsReceptores) > 2000) {
                $lbUnirReceptores = false;
            }
            else {
                $lbUnirReceptores = $this->getValueOfEntities("a.unirreceptores", "FCMainBundle:Seguridadenviodecorreoparametrizacion a", "a.sistema='" . $this->Sistema . "'", null, false);
            }
            $lsArrayReceptor = explode($lsSeparador, $lsReceptores);
        }
        else {
            $lbUnirReceptores = true;
            $lsReceptores = "";
        }

        $lsDocName = $this->getValueOfEntities("a.descripcion", "FCMainBundle:Documentos a", "a.iddocumento=" . $plIDDoc);
        $lsAsunto = $lsDocName . (($psAgrearAlAsunto != "") ? " / " . trim(strtoupper($psAgrearAlAsunto)): "");
        $fecha = ($psFechaHora != '' ? "Fecha: ". $psFechaHora ."<br />" : "");
        $lsMensaje = $lsDocName . "<br />". $fecha ."N&uacute;mero: " . $plDocNro . "<br />" . $psDocDes;


        $llIdCorreo = $this->generarIdTabla("Seguridadenviodecorreo", "idcorreo");
        If ($lbUnirReceptores==true) {
            $entity = new Seguridadenviodecorreo();
            $entity->setIdcorreo($llIdCorreo);
            $entity->setEmisor('');
            $entity->setReceptor($lsReceptores);
            $entity->setAsunto($lsAsunto);
            $entity->setMensaje($lsMensaje);
            $entity->setShowscreen(0);
            $entity->setShowmessages(0);
            $entity->setEstado(0);
            $this->em->persist($entity);
            $this->em->flush();
        } else {
            for ($llPos = 0; $llPos <= count($lsArrayReceptor); $llPos++) {
                $entity = new Seguridadenviodecorreo();
                $entity->setIdcorreo($llPos + $llIdCorreo);
                $entity->setEmisor('');
                $entity->setReceptor($lsArrayReceptor[$llPos]);
                $entity->setAsunto($lsAsunto);
                $entity->setMensaje($lsMensaje);
                $entity->setShowscreen(0);
                $entity->setShowmessages(0);
                $entity->setEstado(0);
                $this->em->persist($entity);
                $this->em->flush();
            }
        }
    }

    private function mEnviarNotificacionSMS($plIDDoc, $plDocNro, $psFechaHora, $psDocDes, $psAgrearAlAsunto="") {
        if (!($this->ModoDeveloper)) {
            $lsSeparador = $this->getValueOfEntities("a.unirconseparador", "FCMainBundle:Seguridadenviodecorreoparametrizacion a", "a.sistema='" . $this->Sistema . "'");
            $lsReceptores = $this->gTomarReceptoresDelDocumento($plIDDoc, $plDocNro, $lsSeparador, NotificacionesCuentasEnum::SMS);

            if (strlen($lsReceptores) > 2000) {
                $lbUnirReceptores = false;
            }
            else {
                $lbUnirReceptores = $this->getValueOfEntities("a.unirreceptores", "FCMainBundle:Seguridadenviodecorreoparametrizacion a", "a.sistema='" . $this->Sistema . "'", null, false);
            }
            $lsArrayReceptor = explode($lsSeparador, $lsReceptores);
        }
        else {
            $lbUnirReceptores = true;
            $lsReceptores = "";
        }

        if ($lsReceptores == "") {
            return false;
        }
        if (count($lsArrayReceptor) < 0)  {
            return false;
        }
        if ($lsArrayReceptor[0] = "") {
            return false;
        }

        $lsDocName = $this->getValueOfEntities("a.descripcion", "FCMainBundle:Documentos a", "a.iddocumento=" . $plIDDoc);
        $lsAsunto = $lsDocName . (($psAgrearAlAsunto != "") ? " / " . trim(strtoupper($psAgrearAlAsunto)): "");
        $fecha = ($psFechaHora != '' ? "Fecha: ". $psFechaHora ."\r\n" : "");
        $lsMensaje = $lsDocName . "\r\n" .$fecha. "Numero: " . $plDocNro . "\r\n" . $this->mDevolverMensajeFormateado($psDocDes, NotificacionesFormatsEnum::TextPlain);

        $llCantSMS = 4;
        $lArraySMS = $this->mSepararMensajeEnSMS($lsMensaje, 100);

        $llIdCorreo = $this->generarIdTabla("Seguridadenviodecorreo", "idcorreo");
        for ($llPosSMS = 0; $llPosSMS <= count($lArraySMS); $llPosSMS++) {
            if ($llPosSMS > $llCantSMS - 1) {
                if ($lbUnirReceptores==true) {
                    $count = (count($lArraySMS) > $llCantSMS) ? $llCantSMS : count($lArraySMS);
                    $entity = new Seguridadenviodecorreo();
                    $entity->setIdcorreo($llPosSMS + $llIdCorreo);
                    $entity->setEmisor('');
                    $entity->setReceptor($lsReceptores);
                    $entity->setAsunto("SMS(" . ($llPosSMS + 1) . "/" . $count . ")");
                    $entity->setMensaje('Para continuar viendo el Mensaje verifique su casilla de correo.');
                    $entity->setShowscreen(0);
                    $entity->setShowmessages(0);
                    $entity->setEstado(0);
                    $this->em->persist($entity);
                    $this->em->flush();
                }
                else {
                    for ($llPosReceptor = 0; $llPosReceptor <= count($lsArrayReceptor); $llPosReceptor++) {
                        $count = (count($lArraySMS) > $llCantSMS) ? $llCantSMS : count($lArraySMS);
                        $entity = new Seguridadenviodecorreo();
                        $entity->setIdcorreo($llPosSMS + $llPosReceptor + $llIdCorreo);
                        $entity->setEmisor('');
                        $entity->setReceptor($lsArrayReceptor[$llPosReceptor]);
                        $entity->setAsunto("SMS(" . ($llPosSMS + 1) . "/" . $count . ")");
                        $entity->setMensaje('Para continuar viendo el Mensaje verifique su casilla de correo.');
                        $entity->setShowscreen(0);
                        $entity->setShowmessages(0);
                        $entity->setEstado(0);
                        $this->em->persist($entity);
                        $this->em->flush();
                    }
                }
                break;
            }
            else {
                if ($lbUnirReceptores==true) {
                    $count = (count($lArraySMS) > $llCantSMS) ? $llCantSMS : count($lArraySMS);
                    $entity = new Seguridadenviodecorreo();
                    $entity->setIdcorreo($llPosSMS + $llIdCorreo);
                    $entity->setEmisor('');
                    $entity->setReceptor($lsReceptores);
                    $entity->setAsunto("SMS(" . ($llPosSMS + 1) . "/" . $count . ")");
                    $entity->setMensaje($lArraySMS[$llPosSMS]);
                    $entity->setShowscreen(0);
                    $entity->setShowmessages(0);
                    $entity->setEstado(0);
                    $this->em->persist($entity);
                    $this->em->flush();
                }
                else {
                    for ($llPosReceptor = 0; $llPosReceptor <= count($lsArrayReceptor); $llPosReceptor++) {
                        $count = (count($lArraySMS) > $llCantSMS) ? $llCantSMS : count($lArraySMS);
                        $entity = new Seguridadenviodecorreo();
                        $entity->setIdcorreo($llPosSMS + $llPosReceptor + $llIdCorreo);
                        $entity->setEmisor('');
                        $entity->setReceptor($lsArrayReceptor[$llPosReceptor]);
                        $entity->setAsunto("SMS(" . ($llPosSMS + 1) . "/" . $count . ")");
                        $entity->setMensaje($lArraySMS[$llPosSMS]);
                        $entity->setShowscreen(0);
                        $entity->setShowmessages(0);
                        $entity->setEstado(0);
                        $this->em->persist($entity);
                        $this->em->flush();
                    }
                }
            }
        }
    }

    private function mSepararMensajeEnSMS($psText, $plMaxLenght){
        $lsCharacter="";
        $lsTexto = $psText;
        $lsCadena = "";
        $lsPalabra = "";
        $lvVectorOUTPUT = array();
        $countArray=0;

        If ($plMaxLenght <= 0 || trim($psText) == "")
        {
            return false;
        }
        for($llPos=0; $llPos < strlen($lsTexto); $llPos++)
        {
            $lsCharacter = substr($lsTexto, $llPos, 1);
            if (strpos("aáäâbcdeéëêfghiíïîjklmñnoóöôpqrstuúüûvwxyz0123456789:/-()[]{}", strtolower($lsCharacter))===false)
            {
                $lsCadena = trim(trim($lsCadena) . " " . trim($lsPalabra ."". $lsCharacter));
                $lsPalabra = "";
            }
            else
            {
                $lsPalabra = $lsPalabra ."". $lsCharacter;
            }
            if (strlen(trim($lsCadena) . " " . trim($lsPalabra)) >= $plMaxLenght)
            {
                if (trim($lsCadena) != "")
                {
                    $lvVectorOUTPUT[$countArray] = $lsCadena;
                    $lsCadena = "";
                    $countArray++;

                }
            }
        }
        $lsCadena = trim(trim($lsCadena) . " " . trim($lsPalabra));
        if ($lsCadena != "")
        {
            $lvVectorOUTPUT[$countArray] = $lsCadena;
            $lsCadena = "";
            $countArray++;
        }
        if (empty($lvVectorOUTPUT))
        {
            $lvVectorOUTPUT = "";
            $lsCadena = "";
        }
        return $lvVectorOUTPUT;
    }

    private function mCargarParametrosCorreo() {
        $data = $this->em->createQuery("SELECT a.correo, a.autentificacion, a.servidor, a.puerto, a.usuario, a.clave FROM FCMainBundle:Seguridadenviodecorreoparametrizacion a WHERE a.sistema='" . $this->Sistema . "'")->getResult();
        if($data){
            $CorreoAlias = '' . $data[0]['correo'];
            $CorreoEmail = '' . $data[0]['correo'];
            if (strpos($CorreoEmail, '<')>0 and strpos($CorreoEmail, '>')>0){
                $CorreoAlias=substr($CorreoAlias, 0, strpos($CorreoAlias, '<')-1);
                $CorreoEmail=substr($CorreoEmail, strpos($CorreoEmail, '<')+1, -1);
            }

            $this->EmailParameters->Authentication = $data[0]['autentificacion'];
            $this->EmailParameters->Host = '' . $data[0]['servidor'];
            $this->EmailParameters->Port = intval('' . $data[0]['puerto']);
            $this->EmailParameters->SSL = ($this->EmailParameters->Authentication ? true : false);
            $this->EmailParameters->User = '' . $data[0]['usuario'];
            $this->EmailParameters->Pass = '' . $data[0]['clave'];

            $this->EmailParameters->From = array($CorreoEmail => $CorreoAlias);
            If ($this->EmailParameters->From == '' or $CorreoEmail == '') {
                $this->EmailParameters->From = array('sistemas@ferrarocamacho.com.ar' => 'Sistemas');
            }
            $this->EmailParameters->CCO = $this->EmailParameters->From;
            return true;
        }
        return false;
    }

    private function getValueOfEntities($Field, $Entity, $Where = null, $OrderBy = null, $DefaultValue = null) {
        try {
            $result = $this->em->createQuery("SELECT " . $Field . " AS Retorna FROM " . $Entity . ($Where != null ? " WHERE " . $Where : "") . ($OrderBy != null ? " ORDER BY " . $OrderBy : ""))->getResult();
        } catch (\Exception $e) {
            $result = null;
        }
        return ($result ? $result[0]['Retorna'] : $DefaultValue);
    }
}