function Coleccion()
    {
        this.items = [];
        this.camposListar = [];
        this.idListadoHTML = [];
        this.editActivado = true;
        this.buttonSubmit = "#";
    }

    Coleccion.prototype.agregar = function (objeto) {
        if(objeto.id == 0){
            maxId = JSLINQ(this.items).OrderByDescending(function(item){ return item.id; })
            if(maxId.items[0] == null){
                id = 1;
            }else{
                id = parseInt(maxId.items[0].id)+1;
            }
            objeto.id = id;
        }
        this.items.push(objeto);        
    };

    Coleccion.prototype.setEditActivado = function (activado) {
        this.editActivado = activado;
    };

    Coleccion.prototype.setidListadoHTML = function (idListadoHTML) {
        this.idListadoHTML = idListadoHTML;
    };

    Coleccion.prototype.setCamposListar = function (camposListar) {
        this.camposListar = camposListar;
    };

    Coleccion.prototype.setButtonSubmit = function (idButtonSubmit) {
        this.buttonSubmit = idButtonSubmit;
    };


    Coleccion.prototype.quitar = function (objetoId) {
        c = this.items;
        $.each(this.items, function(index, result) 
           {
              if(result.id == objetoId) {                 
                 c.splice(index, 1);   
                 return false;             
              }    
        });
    };

    Coleccion.prototype.editar = function (objeto) {
        c = this.items;
        $.each(this.items, function(index, result)
           {
              if(result.id == objeto.id) {                 
                c[index] = objeto;
                return false;             
              }    
        });
    };

    Coleccion.prototype.setEnviados = function () { //solo para recomendaciones
        $.each(this.items, function(index, result)
           {
              result.rec_notificacion = 0;
        });
    };

    Coleccion.prototype.listar = function () {  
        html = "";
        c = this.camposListar;
        edit = this.editActivado;
        buttonSubmit = this.buttonSubmit;
        $(this.idListadoHTML).html(html);
        recomendacionesLista = JSLINQ(this.items).OrderByDescending(function(item){ return item.id; });            
        recomendacionesLista.items.forEach(function(rec){
            html =  html + '<tr>';            
            c.forEach(function(entry){     
                html = html +'<td>' + rec[entry] + '</td>';
            }); 
            html = html +'<td class="text-center"><div class="btn-group">';
            html = html +'<a href="'+buttonSubmit+'" data-id="' + rec.id + '" class="borrarColeccion btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span></a> ';

            if(edit == true)
            {
                html = html +'<a href="'+buttonSubmit+'" data-id="' + rec.id + '" class="editarColeccion btn btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>';
            }
            html = html +'</div></td></tr>';
        }); 
       $(this.idListadoHTML).append(html);           
    };


    Coleccion.prototype.jsonString = function () {  
       jsonString = JSON.stringify(this.items);
       return jsonString;
    };

    Coleccion.prototype.getItems = function () {
        return this.items;
    };