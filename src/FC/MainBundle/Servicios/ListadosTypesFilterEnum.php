<?php

namespace FC\MainBundle\Servicios;

class ListadosTypesFilterEnum {
    const tUnique = 0;
    const tFromTo = 1;
    const tLike = 2;
    const tMultiSelect = 3;
}
