CREATE TABLE Transacciones (
	id INT,
	Fecha DATETIME,
	idCuentaOrigen INT,
	idCuentaDestino INT,
	idCategoria INT,
	Descripcion VARCHAR(50),
	Importe FLOAT,
    PRIMARY KEY (id)
);

CREATE TABLE Categorias (
	id INT,
	Nombre VARCHAR(50),
	idTipo INT,
	PRIMARY KEY (id)
);

CREATE TABLE Cuentas (
	id INT,
	Nombre VARCHAR(50),
	Icon VARCHAR(100),
	PRIMARY KEY (id)
);