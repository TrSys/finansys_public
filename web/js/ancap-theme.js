jQuery(document).ready(function($){
	var $lateral_menu_trigger = $('#cd-menu-trigger'),
		$lateral_menu_trigger_big = $('#cd-menu-trigger-bigscreen'),
		$content_wrapper = $('.cd-main-content'),
		$footer = $('footer');

	//open-close lateral menu clicking on the menu icon
	$lateral_menu_trigger.on('click', function(event){
		event.preventDefault();
		//if(!($lateral_menu_trigger.hasClass('is-clicked'))){
		//	$lateral_menu_trigger_big.addClass('is-clicked');
		//	$('#cd-lateral-nav').removeClass('menu-closed');
		//	$content_wrapper.removeClass('menu-closed');
		//	$footer.removeClass('menu-closed');
		//}
		$lateral_menu_trigger.toggleClass('is-clicked');
		$('#cd-lateral-nav').toggleClass('lateral-menu-is-open');
	});


	$lateral_menu_trigger_big.on('click', function(event){
		event.preventDefault();

		$lateral_menu_trigger_big.toggleClass('is-clicked');
		$('#cd-lateral-nav').toggleClass('menu-closed');
		$content_wrapper.toggleClass('menu-closed');
		$footer.toggleClass('menu-closed');
	});

	//close lateral menu clicking outside the menu itself
	$content_wrapper.on('click', function(event){
		if( !$(event.target).is('#cd-menu-trigger, #cd-menu-trigger span') ) {
			$lateral_menu_trigger.removeClass('is-clicked');
			$('#cd-lateral-nav').removeClass('lateral-menu-is-open');
		}
	});

	//open (or close) submenu items in the lateral menu. Close all the other open submenu items->agregar esto:".parent('.item-has-children2').siblings('.item-has-children2').children('a').removeClass('submenu-open').next('.sub-menu').slideUp(200);".
	$('.item-has-children').children('a').on('click', function(event){
		event.preventDefault();
		$(this).toggleClass('submenu-open').next('.sub-menu').slideToggle(200).end();
	});
	$('.item-has-children2').children('a').on('click', function(event){
		event.preventDefault();
		$(this).toggleClass('submenu-open').next('.sub-menu').slideToggle(200).end();
	});
});