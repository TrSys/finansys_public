<?php
namespace FC\MainBundle\Controller;

use FC\MainBundle\Constantes\MaestrosSimplesFieldTypeEnum;
use FC\MainBundle\Constantes\MaestrosSimplesParameters;
use FC\MainBundle\Servicios\Archivos;
use FC\MainBundle\Servicios\Documentos;
use FC\MainBundle\Servicios\Listados;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MaestrosSimplesController extends Controller {
    public function indexAction($tableName = null, $page = 1, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $Maestro = new MaestrosSimplesParameters($em, $this->container, $this->get('seguridad'));
        if ($Maestro->SetParameters($tableName)==false) {
            $this->get('session')->getFlashBag()->add('warning', 'No se ha logrado establecer parametrización del Maestro.');
            return $this->redirect($this->generateUrl ($Maestro->getRootIndex()));
        }
        if ($Maestro->getParameters()['SecurityShow']==false) {
            $this->get('session')->getFlashBag()->add('info', 'No tiene los permisos necesarios para la acción solicitada.');
            return $this->redirect($this->generateUrl ($Maestro->getRootIndex()));
        }

        $dl = new Listados($em, $request);
        $dl->setTitle('Listado de ' . $Maestro->getParameters()['Title']);
        $dl->setEntity($Maestro->getParameters()['EntityBundle'] . ':' . $Maestro->getParameters()['EntityName']);
        foreach ($Maestro->getParameters()['Fields'] as $item) {
            $dl->addCampo($item['Caption'], $item['Name'], null, null, null, null, $item['ColVisible']);
        }
        if (isset($Maestro->getParameters()['Where'])) {
            $dl->setWhere($Maestro->getParameters()['Where']);
        }
        if (isset($Maestro->getParameters()['OrderBy'])) {
            $dl->setOrderBy($Maestro->getParameters()['OrderBy']);
        }
        foreach($Maestro->getParameters()['Filtros'] as $item) {
            $dl->addFiltro($item['Caption'], $item['PlaceHolder'], $item['Name'], $item['EntityAlias'], $item['EntityLoaded'], $item['AutoPost'], $item['TypeFilter']);
        }
        if (isset($Maestro->getParameters()['IDRegistrosNoModificables'])) {
            $dl->setIDRegistrosNoModificables($Maestro->getParameters()['IDRegistrosNoModificables']);
        }

        $dl->setPathIndex('maestrossimples', array(array('modulo', null, $Maestro->getmodulo()), array('moduloAlias', null, $Maestro->getmoduloAlias()), array('tableName', null, $tableName)));
        $dl->setPathShow('maestrossimples_show_y_edit', array(array('modulo', null, $Maestro->getmodulo()), array('moduloAlias', null, $Maestro->getmoduloAlias()), array('tableName', null, $tableName), array('id', $Maestro->getParameters()['Fields'][0]['Name'])));

        $dl->addActions('Editar', 'maestrossimples_show_y_edit', 'glyphicon-pencil', $Maestro->getParameters()['SecurityUpdate'], '', array(array('modulo', null, $Maestro->getmodulo()), array('moduloAlias', null, $Maestro->getmoduloAlias()), array('tableName', null, $tableName), array('id', $Maestro->getParameters()['Fields'][0]['Name'])));
        $dl->addActions('Eliminar', 'maestrossimples_delete', 'glyphicon-remove', $Maestro->getParameters()['SecurityDelete'], 'btn-delete', array(array('modulo', null, $Maestro->getmodulo()), array('moduloAlias', null, $Maestro->getmoduloAlias()), array('tableName', null, $tableName), array('id', $Maestro->getParameters()['Fields'][0]['Name'])));
        $dl->addHeaderActions('Nuevo', 'maestrossimples_new', 'glyphicon-plus', $Maestro->getParameters()['SecurityCreate'], 'btn2 btn2-primary', array(array('modulo', null, $Maestro->getmodulo()), array('moduloAlias', null, $Maestro->getmoduloAlias()), array('tableName', null, $tableName)));

        $this->get('Auditoria')->RegistrarAuditoria($Maestro->getParameters()['Title'], 'Listado');

        return $this->render('FCMainBundle:ListadosSimples:index.html.twig', array(
                'modulo' => $Maestro->getmodulo(),
                'moduloAlias' => $Maestro->getmoduloAlias(),
                'Parametros' => $dl->getParametros(),
                'pagination' => $dl->getPagination($this->get('knp_paginator'), $page)
        ));
    }

    public function showAction($tableName = null, $id, Request $request){
        $Leyenda = 'Visualización';
        $params = $request->request->all();
        $RutaRetorno = (isset($params["RutaRetorno"]) ? strval($params["RutaRetorno"]) : 'maestrossimples');
        unset($params);

        $em = $this->getDoctrine()->getManager();
        $Maestro = new MaestrosSimplesParameters($em, $this->container, $this->get('seguridad'));
        if ($Maestro->SetParameters($tableName, $id)==false) {
            $this->get('session')->getFlashBag()->add('warning', 'No se ha logrado establecer parametrización del Maestro.');
            return $this->redirect($this->generateUrl($Maestro->getRootIndex()));
        }
        if ($Maestro->getParameters()['SecurityShow']==false) {
            $this->get('session')->getFlashBag()->add('info', 'No tiene los permisos necesarios para la acción solicitada.');
            return $this->redirect($this->generateUrl($RutaRetorno, array(
                'modulo' => $Maestro->getmodulo(),
                'moduloAlias' => $Maestro->getmoduloAlias(),
                'tableName' => $tableName,
            )));
        }

        $this->get('Auditoria')->RegistrarAuditoria($Maestro->getParameters()['Title'], 'Código: '. $id .' - ' . $Leyenda);

        $Maestro->addActions('Volver', $RutaRetorno, 'glyphicon-ban-circle', $Maestro->getParameters()['SecurityShow'], 'btn2 btn2-default', true);
        return $this->render('FCMainBundle:MaestrosSimples:form.html.twig', array(
            'modulo' => $Maestro->getmodulo(),
            'moduloAlias' => $Maestro->getmoduloAlias(),
            'tableName' => $tableName,
            'Title' => 'Detalle de ' . $Maestro->getParameters()['Title'],
            'Parameters'  => $Maestro->getParameters()
        ));
    }

    public function newAction($tableName = null, Request $request) {
        $params = $request->request->all();
        $RutaRetorno = (isset($params["RutaRetorno"]) ? strval($params["RutaRetorno"]) : 'maestrossimples');
        unset($params);

        $em = $this->getDoctrine()->getManager();
        $Maestro = new MaestrosSimplesParameters($em, $this->container, $this->get('seguridad'));
        if ($Maestro->SetParameters($tableName)==false) {
            $this->get('session')->getFlashBag()->add('warning', 'No se ha logrado establecer parametrización del Maestro.');
            return $this->redirect($this->generateUrl ($Maestro->getRootIndex()));
        }
        if ($Maestro->getParameters()['SecurityCreate']==false) {
            $this->get('session')->getFlashBag()->add('info', 'No tiene los permisos necesarios para la acción solicitada.');
            return $this->redirect($this->generateUrl($RutaRetorno, array(
                'modulo' => $Maestro->getmodulo(),
                'moduloAlias' => $Maestro->getmoduloAlias(),
                'tableName' => $tableName,
            )));
        }

        $Maestro->FormAction('Guardar', $this->generateUrl('maestrossimples_create', array('modulo' => $Maestro->getmodulo(), 'moduloAlias' => $Maestro->getmoduloAlias(), 'tableName' => $tableName)), 'glyphicon-circle-arrow-up', $Maestro->getParameters()['SecurityCreate'], 'btn2 btn2-success');
        $Maestro->addActions('Cancelar', $RutaRetorno, 'glyphicon-ban-circle', true, 'btn2 btn2-default', true);
        return $this->render('FCMainBundle:MaestrosSimples:form.html.twig', array(
            'modulo' => $Maestro->getmodulo(),
            'moduloAlias' => $Maestro->getmoduloAlias(),
            'tableName' => $tableName,
            'Title' => 'Incorporación de '. $Maestro->getParameters()['Title'],
            'Parameters'  => $Maestro->getParameters(),
            'RutaRetorno' => $RutaRetorno
        ));
    }

    public function createAction($tableName = null, Request $request) {
        $Leyenda = 'Incorporación';
        $RutaRetorno = ($request->request->get("RutaRetorno") ? strval($request->request->get("RutaRetorno")) : 'maestrossimples');

        $em = $this->getDoctrine()->getManager();
        $Maestro = new MaestrosSimplesParameters($em, $this->container, $this->get('seguridad'));
        if ($Maestro->SetParameters($tableName)==false) {
            $this->get('session')->getFlashBag()->add('warning', 'No se ha logrado establecer parametrización del Maestro.');
            return $this->redirect($this->generateUrl ($Maestro->getRootIndex()));
        }
        if ($Maestro->getParameters()['SecurityCreate']==false) {
            $this->get('session')->getFlashBag()->add('info', 'No tiene los permisos necesarios para la acción solicitada.');
            return $this->redirect($this->generateUrl($RutaRetorno, array(
                'modulo' => $Maestro->getmodulo(),
                'moduloAlias' => $Maestro->getmoduloAlias(),
                'tableName' => $tableName,
            )));
        }

        $id = null;
        $class = 'FC\\MainBundle\\Entity\\'. $Maestro->getParameters()['EntityName'];
        $entity = New $class();
        if ($entity) {
            foreach ($Maestro->getParameters()['Fields'] as $field) {
                switch ($field['Type']) {
                    case MaestrosSimplesFieldTypeEnum::ftFile:
                        $archivos = new Archivos();
                        $lsFileNew = $request->files->get($field['Name']);
                        $lsFileOld = null;
                        $lsFileDelete = $request->request->get($field['Name'] . '_Delete');
                        $status = $archivos->UploadFile($lsFileNew, $lsFileOld, $lsFileDelete, $field['FilePath'], $field['FileType']);
                        if ($status == Archivos::Error) {
                            return $this->redirect($this->generateUrl($RutaRetorno, array(
                                'modulo' => $Maestro->getmodulo(),
                                'moduloAlias' => $Maestro->getmoduloAlias(),
                                'tableName' => $tableName,
                            )));
                        }
                        $value = $status;
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftBoolean:
                        $value = ($request->request->get($field['Name']) == '1' ? 1 : 0);
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftInteger:
                        if ($field['PrimaryKey'] == true and $field['AutoIncrement'] == true) {
                            $documentos = new Documentos($em, $this->container);
                            $value = $documentos->generarIdTabla($Maestro->getParameters()['EntityName'], $field['Name'], 1, false);
                        } else {
                            $value = intval($request->request->get($field['Name']));
                        }
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftBigInteger:
                        $value = floatval($request->request->get($field['Name']));
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftFloat:
                        $value = floatval($request->request->get($field['Name']));
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftDate:
                        $value = new \DateTime(str_ireplace('/', '-', $request->request->get($field['Name'])));
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftDateTime:
                        $value = new \DateTime(str_ireplace('/', '-', $request->request->get($field['Name'])));
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftTime:
                        $value = new \DateTime(str_ireplace('/', '-', $request->request->get($field['Name'])));
                        break;
                    default:
                        $value = $request->request->get($field['Name']);
                        break;
                }
                if ($field['PrimaryKey']) {
                    $id = $value;
                }
                if ($value != '') {
                    $function = "set".$Maestro->getTransformChars($field['Name']);
                    $entity->$function($value);
                } else {
                    if ($field['Required'] == true) {
                        $this->get('session')->getFlashBag()->add('warning', 'Debe completar todos los campos requeridos antes de guardar los cambios.');
                        return $this->redirect($this->generateUrl($RutaRetorno, array(
                            'modulo' => $Maestro->getmodulo(),
                            'moduloAlias' => $Maestro->getmoduloAlias(),
                            'tableName' => $tableName,
                        )));
                    } else {
                        $function = "set".$Maestro->getTransformChars($field['Name']);
                        $entity->$function(null);
                    }
                }
            }
            try {
                $em->persist($entity);
                $em->flush();
                $this->get('Auditoria')->RegistrarAuditoria($Maestro->getParameters()['Title'], 'Código: '. $id .' - ' . $Leyenda);
                $this->get('session')->getFlashBag()->add('success', $Leyenda . ' realizada con éxito.');
                return $this->redirect($this->generateUrl($RutaRetorno, array(
                    'modulo' => $Maestro->getmodulo(),
                    'moduloAlias' => $Maestro->getmoduloAlias(),
                    'tableName' => $tableName,
                )));
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('warning', 'No se ha logrado realizar la ' . $Leyenda . '.');
                return $this->redirect($this->generateUrl($RutaRetorno, array(
                    'modulo' => $Maestro->getmodulo(),
                    'moduloAlias' => $Maestro->getmoduloAlias(),
                    'tableName' => $tableName,
                )));
            }
        } else {
            $this->get('session')->getFlashBag()->add('warning', 'No se ha logrado realizar la ' . $Leyenda . '.');
            return $this->redirect($this->generateUrl($RutaRetorno, array(
                'modulo' => $Maestro->getmodulo(),
                'moduloAlias' => $Maestro->getmoduloAlias(),
                'tableName' => $tableName,
            )));
        }
    }

    public function editAction($tableName = null, $id, Request $request){
        $Leyenda = 'Visualización';
        $params = $request->request->all();
        $RutaRetorno = (isset($params["RutaRetorno"]) ? strval($params["RutaRetorno"]) : 'maestrossimples');
        unset($params);

        $em = $this->getDoctrine()->getManager();
        $Maestro = new MaestrosSimplesParameters($em, $this->container, $this->get('seguridad'));
        if ($Maestro->SetParameters($tableName, $id)==false) {
            $this->get('session')->getFlashBag()->add('warning', 'No se ha logrado establecer parametrización del Maestro.');
            return $this->redirect($this->generateUrl ($Maestro->getRootIndex()));
        }
        if ($Maestro->getParameters()['SecurityShow']==false and $Maestro->getParameters()['SecurityUpdate']==false) {
            $this->get('session')->getFlashBag()->add('info', 'No tiene los permisos necesarios para la acción solicitada.');
            return $this->redirect($this->generateUrl($RutaRetorno, array(
                'modulo' => $Maestro->getmodulo(),
                'moduloAlias' => $Maestro->getmoduloAlias(),
                'tableName' => $tableName,
            )));
        }
        $this->get('Auditoria')->RegistrarAuditoria($Maestro->getParameters()['Title'], 'Código: '. $id .' - ' . $Leyenda);

        $Maestro->FormAction('Guardar', $this->generateUrl('maestrossimples_update',array('modulo' => $Maestro->getmodulo(), 'moduloAlias' => $Maestro->getmoduloAlias(), 'tableName' => $tableName, 'id' => $id)), 'glyphicon-circle-arrow-up', $Maestro->getParameters()['SecurityUpdate'], 'btn2 btn2-success');
        $Maestro->addActions('Cancelar', $RutaRetorno, 'glyphicon-ban-circle', true, 'btn2 btn2-default', true);
        return $this->render('FCMainBundle:MaestrosSimples:form.html.twig', array(
            'modulo' => $Maestro->getmodulo(),
            'moduloAlias' => $Maestro->getmoduloAlias(),
            'tableName' => $tableName,
            'Title' => 'Actualización de ' . $Maestro->getParameters()['Title'],
            'Parameters'  => $Maestro->getParameters(),
            'RutaRetorno' => $RutaRetorno
        ));
    }

    public function updateAction($tableName = null, Request $request, $id){
        $Leyenda = 'Actualización';
        $RutaRetorno = ($request->request->get("RutaRetorno") ? strval($request->request->get("RutaRetorno")) : 'maestrossimples');

        $em = $this->getDoctrine()->getManager();
        $Maestro = new MaestrosSimplesParameters($em, $this->container, $this->get('seguridad'));
        if ($Maestro->SetParameters($tableName, $id)==false) {
            $this->get('session')->getFlashBag()->add('warning', 'No se ha logrado establecer parametrización del Maestro.');
            return $this->redirect($this->generateUrl ($Maestro->getRootIndex()));
        }
        if ($Maestro->getParameters()['SecurityUpdate']==false) {
            $this->get('session')->getFlashBag()->add('info', 'No tiene los permisos necesarios para la acción solicitada.');
            return $this->redirect($this->generateUrl($RutaRetorno, array(
                'modulo' => $Maestro->getmodulo(),
                'moduloAlias' => $Maestro->getmoduloAlias(),
                'tableName' => $tableName,
            )));
        }

        $entity = $em->getRepository($Maestro->getParameters()['EntityBundle'] . ':' . $Maestro->getParameters()['EntityName'])->find($id);
        if ($entity) {
            foreach ($Maestro->getParameters()['Fields'] as $field) {
                switch ($field['Type']) {
                    case MaestrosSimplesFieldTypeEnum::ftFile:
                        $archivos = new Archivos();
                        $lsFileNew = $request->files->get($field['Name']);
                        $lsFileOld = $request->request->get($field['Name'] . '_Old');
                        $lsFileDelete = $request->request->get($field['Name'] . '_Delete');
                        $status = $archivos->UploadFile($lsFileNew, $lsFileOld, $lsFileDelete, $field['FilePath'], $field['FileType']);
                        if ($status == Archivos::Error) {
                            return $this->redirect($this->generateUrl($RutaRetorno, array(
                                'modulo' => $Maestro->getmodulo(),
                                'moduloAlias' => $Maestro->getmoduloAlias(),
                                'tableName' => $tableName,
                            )));
                        }
                        $value = $status;
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftBoolean:
                        $value = ($request->request->get($field['Name']) == '1' ? 1 : 0);
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftInteger:
                        $value = intval($request->request->get($field['Name']));
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftBigInteger:
                        $value = floatval($request->request->get($field['Name']));
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftFloat:
                        $value = floatval($request->request->get($field['Name']));
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftDate:
                        $value = new \DateTime(str_ireplace('/', '-', $request->request->get($field['Name'])));
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftDateTime:
                        $value = new \DateTime(str_ireplace('/', '-', $request->request->get($field['Name'])));
                        break;
                    case MaestrosSimplesFieldTypeEnum::ftTime:
                        $value = new \DateTime(str_ireplace('/', '-', $request->request->get($field['Name'])));
                        break;
                    default:
                        $value = $request->request->get($field['Name']);
                        break;
                }
                if ($value != '') {
                    $function = "set".$Maestro->getTransformChars($field['Name']);
                    $entity->$function($value);
                } else {
                    if ($field['Required'] == true) {
                        $this->get('session')->getFlashBag()->add('warning', 'Debe completar todos los campos requeridos antes de guardar los cambios.');
                        return $this->redirect($this->generateUrl($RutaRetorno, array(
                            'modulo' => $Maestro->getmodulo(),
                            'moduloAlias' => $Maestro->getmoduloAlias(),
                            'tableName' => $tableName,
                        )));
                    } else {
                        $function = "set".$Maestro->getTransformChars($field['Name']);
                        $entity->$function(null);
                    }
                }
            }
            try {
                $em->persist($entity);
                $em->flush();
                $this->get('Auditoria')->RegistrarAuditoria($Maestro->getParameters()['Title'], 'Código: '. $id .' - ' . $Leyenda);
                $this->get('session')->getFlashBag()->add('success', $Leyenda . ' realizada con éxito.');
                return $this->redirect($this->generateUrl($RutaRetorno, array(
                    'modulo' => $Maestro->getmodulo(),
                    'moduloAlias' => $Maestro->getmoduloAlias(),
                    'tableName' => $tableName,
                )));
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('warning', 'No se ha logrado realizar la ' . $Leyenda . '.');
                return $this->redirect($this->generateUrl($RutaRetorno, array(
                    'modulo' => $Maestro->getmodulo(),
                    'moduloAlias' => $Maestro->getmoduloAlias(),
                    'tableName' => $tableName,
                )));
            }
        } else {
            $this->get('session')->getFlashBag()->add('warning', 'No se ha logrado realizar la ' . $Leyenda . '.');
            return $this->redirect($this->generateUrl($RutaRetorno, array(
                'modulo' => $Maestro->getmodulo(),
                'moduloAlias' => $Maestro->getmoduloAlias(),
                'tableName' => $tableName,
            )));
        }
    }

    public function deleteAction($tableName = null, $id, $RutaRetorno = null){
        $Leyenda = 'Eliminación';
        $RutaRetorno = ($RutaRetorno!= null ? strval($RutaRetorno) : 'maestrossimples');

        $em = $this->getDoctrine()->getManager();
        $Maestro = new MaestrosSimplesParameters($em, $this->container, $this->get('seguridad'));
        if ($Maestro->SetParameters($tableName, $id)==false) {
            $this->get('session')->getFlashBag()->add('warning', 'No se ha logrado establecer parametrización del Maestro.');
            return $this->redirect($this->generateUrl ($Maestro->getRootIndex()));
        }
        if ($Maestro->getParameters()['SecurityDelete']==false) {
            $this->get('session')->getFlashBag()->add('info', 'No tiene los permisos necesarios para la acción solicitada.');
            return $this->redirect($this->generateUrl($RutaRetorno, array(
                'modulo' => $Maestro->getmodulo(),
                'moduloAlias' => $Maestro->getmoduloAlias(),
                'tableName' => $tableName,
            )));
        }

        if (isset($Maestro->getParameters()['ForeignTable'])) {
            foreach ($Maestro->getParameters()['ForeignTable'] as $item) {
                try {
                    $delimit = ($Maestro->getParameters()['Fields'][0]['Type']==MaestrosSimplesFieldTypeEnum::ftString ? "'" : "");
                    $entity = $em->createQuery('SELECT a FROM ' . $item['Bundle'] . ':' . $item['Table'] . ' a WHERE a.' . $item['Field'] . '=' .$delimit. $id .$delimit)->getResult();
                    if ($entity) {
                        $this->get('session')->getFlashBag()->add('warning', 'Hay información vinculada en: '. $item['Title']);
                        return $this->redirect($this->generateUrl($RutaRetorno, array(
                            'modulo' => $Maestro->getmodulo(),
                            'moduloAlias' => $Maestro->getmoduloAlias(),
                            'tableName' => $tableName,
                        )));
                    }
                } catch (\Exception $e) {
                    $this->get('session')->getFlashBag()->add('danger', get_class($this) .' - Linea: '. __LINE__ .' - Descripción: '. $e->getMessage());
                    return $this->redirect($this->generateUrl($RutaRetorno, array(
                        'modulo' => $Maestro->getmodulo(),
                        'moduloAlias' => $Maestro->getmoduloAlias(),
                        'tableName' => $tableName,
                    )));
                }
            }
        }

        $entity = $em->getRepository($Maestro->getParameters()['EntityBundle'] . ':' . $Maestro->getParameters()['EntityName'])->find($id);
        if (! $entity) {
            $this->get('session')->getFlashBag()->add('warning', 'No se ha encontrado el registro.');
            return $this->redirect($this->generateUrl($RutaRetorno, array(
                'modulo' => $Maestro->getmodulo(),
                'moduloAlias' => $Maestro->getmoduloAlias(),
                'tableName' => $tableName,
            )));
        }
        $em->remove($entity);
        $em->flush();

        $this->get('Auditoria')->RegistrarAuditoria($Maestro->getParameters()['Title'], 'Código: '. $id .' - ' . $Leyenda);
        $this->get('session')->getFlashBag()->add('success', $Leyenda . ' realizada con éxito.');
        return $this->redirect($this->generateUrl($RutaRetorno, array(
            'modulo' => $Maestro->getmodulo(),
            'moduloAlias' => $Maestro->getmoduloAlias(),
            'tableName' => $tableName,
        )));
    }
}
