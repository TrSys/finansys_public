<?php

namespace FC\MainBundle\Constantes;

class RolesEnum {
    const RolContratista = 1;
    const RolNexo = 2;
    const RolSegInd = 3;
}
