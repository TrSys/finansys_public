<?php

namespace FC\MainBundle\Servicios;

use Doctrine\ORM\EntityManager;
use FC\MainBundle\Entity\Seguridadauditor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

class Auditoria {
    private $Auditar;
    private $em;
    private $container;
    private $request;
    private $twig;

    public function __construct(EntityManager $entityManager, ContainerInterface $container, Request $request) {
        $this->em = $entityManager;
        $this->container = $container;
        $this->request = $request;
        $this->twig = $container->get('twig')->getGlobals();
		$this->Auditar = $this->getValueOfAuditar();
    }

    private function getValueOfAuditar() {
        try {
            $result = $this->em->createQuery("SELECT CASE WHEN a.value IS NULL THEN 0 ELSE a.value AS Retorna FROM FCMainBundle:Seguridadsistemasparametrizacion a WHERE a.typeparameter='SISTEMA' AND a.idparameter='AUDITORIA'")->getResult();
        } catch (\Exception $e) {
            $result = false;
        }
        return ($result ? $result[0]['Retorna'] : false);
    }

    public function RegistrarAuditoria($psOper, $psDescPlus) {
        try {
            $Aux = $this->request->attributes->get('_controller');
            $Aux = substr($Aux, strpos($Aux, '\\')+1);
            $Aux = substr($Aux, 0, strpos($Aux, '\\'));
        } catch (\Exception $e) {
            $Aux = "WEB";
        }
        try {
            if ($this->Auditar) {
                $idAudi = $this->generarIdTabla();
                $CodUsu = $this->container->get('seguridad')->getUsuario();
                $FechaProc = date('Y/m/d'); //"CONVERT(VARCHAR(4), DATEPART(yyyy, GETDATE())) + '/' + RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(mm, GETDATE())), 2) + '/' + RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(DD, GETDATE())), 2)";
                $HoraProc = date('H:i'); //"CONVERT(VARCHAR(5), GETDATE(), 108)";
                $psOper = substr($psOper, 0, 100);
                $psDescPlus = substr($psDescPlus, 0, 500);
                $WinUser = substr($this->request->getHttpHost(), 0, 50);
                $ComputerName = substr($this->request->getHost(), 0, 50);
                $ComputerIPv4 = substr($this->request->getClientIp(), 0, 15);
                $ProductName = substr($Aux, 0, 50);
                $Version = substr($this->twig['AppVersion'], 0, 10);

                $entity = new Seguridadauditor();
                $entity->setIdaudi($idAudi);
                $entity->setCodusu($CodUsu);
                $entity->setFechaproc($FechaProc);
                $entity->setHoraproc($HoraProc);
                $entity->setCodoper($psOper);
                $entity->setDescplus($psDescPlus);
                $entity->setWinuser($WinUser);
                $entity->setComputername($ComputerName);
                $entity->setComputeripv4($ComputerIPv4);
                $entity->setProductname($ProductName);
                $entity->setVersion($Version);
                $this->em->persist($entity);
                $this->em->flush();
            }
        } catch (\Exception $e) {
            $this->request->getSession()->getFlashBag()->add('danger', get_class($this) .' - Linea: '. __LINE__ .' - Descripción: '. $e->getMessage());
        }
    }

    private function generarIdTabla($planta=1) {
        $dql = 'SELECT
                    MAX(a.idaudi) AS MaxID,
                    MIN(b.nroini) AS MinID
                FROM FCMainBundle:Seguridadauditor a
                    LEFT JOIN FCMainBundle:Documentosnumeradores b WITH b.idplanta=' . $planta . ' AND b.iddocumento=' . DocumentosNumeradores::DocAudi . '
                WHERE
                    a.idaudi > (SELECT MIN(c.nroini) FROM FCMainBundle:Documentosnumeradores c
                                WHERE c.idplanta=' . $planta . ' AND c.iddocumento=' . DocumentosNumeradores::DocAudi . ')
                    AND a.idaudi < (SELECT d.nrofin FROM FCMainBundle:Documentosnumeradores d
								    WHERE d.idplanta=' . $planta . ' AND d.iddocumento=' . DocumentosNumeradores::DocAudi . ')';
        $Result = $this->em->createQuery($dql)->getResult();
        if ($Result) {
            if ($Result[0]['MaxID'] != null) {
                return (int) ($Result[0]['MaxID']) + 1;
            } else {
                return (int) ($Result[0]['MinID']) + 1;
            }
        } else {
            return null;
        }
    }
}