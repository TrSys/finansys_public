<?php

namespace FC\MainBundle\Servicios;

use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;
use Symfony\Component\HttpFoundation\Request;

class Listados {
    private $em;
    private $Parametros;
    private $request;
    private $session;

    public function __construct(EntityManager $entityManager, Request $request) {
        $this->em = $entityManager;
        $this->Parametros = array();
        $this->request = $request;
        $this->session = $this->request->getSession();
    }

    public function getPagination(Paginator $paginator, $page=1, $RowsInPage = 10) {
        $Qry = $this->getQuery();
        $query = $this->em->createQuery($Qry);
        $pagination = $paginator->paginate($query, $page, $RowsInPage, array('distinct' => false));
        return $pagination;
    }

    private function convertDDMMAAAAtoAAAAMMDD($Fecha) {
        $Aux = explode('/', $Fecha);
        if (count($Aux)==3) {
            $Fecha = $Aux[2] . '/' . $Aux[1] . '/' . $Aux[0];
        }
        return $Fecha;
    }

    private function getQryWhere() {
        $QryWhere = "";
        if (isset($this->Parametros['Where'])) {
            $QryWhere = $this->Parametros['Where'];
        }

        if (isset($this->Parametros['Filtros'])) {
            foreach ($this->Parametros['Filtros'] as $Item) {
                if (strtoupper($Item['PlaceHolder']) == 'DD/MM/AAAA') {
                    $Item['Condition'] = $this->convertDDMMAAAAtoAAAAMMDD($Item['Condition']);
                    $Item['ConditionTo'] = $this->convertDDMMAAAAtoAAAAMMDD($Item['ConditionTo']);
                }
                switch($Item['TypeFilter']) {
                    case ListadosTypesFilterEnum::tUnique:
                        if (strval($Item['Condition']) != "") {
                            $QryWhere .= ($QryWhere == "" ? '' : ' AND ');
                            if (strtoupper($Item['PlaceHolder']) == 'DD/MM/AAAA') {
                                $QryWhere .= 'DATE_DIFF('.$Item['EntityAlias'] . "." . $Item['FieldName'] . ", '" . str_replace("'", "`", $Item['Condition']) . "')=0";
                            } else {
                                $QryWhere .= $Item['EntityAlias'] . "." . $Item['FieldName'] . "='" . str_replace("'", "`", $Item['Condition']) . "'";
                            }
                        }
                        break;
                    case ListadosTypesFilterEnum::tFromTo:
                        if (strval($Item['Condition']) != "") {
                            $QryWhere .= ($QryWhere == "" ? '' : ' AND ');
                            if (strtoupper($Item['PlaceHolder']) == 'DD/MM/AAAA') {
                                $QryWhere .= 'DATE_DIFF('.$Item['EntityAlias'] . "." . $Item['FieldName'] . ", '" . str_replace("'", "`", $Item['Condition']) . "')>=0";
                            } else {
                                $QryWhere .= $Item['EntityAlias'] . "." . $Item['FieldName'] . ">='" . str_replace("'", "`", $Item['Condition']) . "'";
                            }
                        }
                        if (strval($Item['ConditionTo']) != "") {
                            $QryWhere .= ($QryWhere == "" ? '' : ' AND ');
                            if (strtoupper($Item['PlaceHolder']) == 'DD/MM/AAAA') {
                                $QryWhere .= 'DATE_DIFF('.$Item['EntityAlias'] . "." . $Item['FieldName'] . ", '" . str_replace("'", "`", $Item['ConditionTo']) . "')<=0";
                            } else {
                                $QryWhere .= $Item['EntityAlias'] . "." . $Item['FieldName'] . "<='" . str_replace("'", "`", $Item['ConditionTo']) . "'";
                            }
                        }
                        break;
                    case ListadosTypesFilterEnum::tLike:
                        if (strval($Item['Condition']) != "") {
                            $QryWhere .= ($QryWhere == "" ? '' : ' AND ');
                            $QryWhere .= $Item['EntityAlias'] .".". $Item['FieldName'] ." LIKE '%". str_replace("'", "`", $Item['Condition']) ."%'";
                        }
                        break;
                    case ListadosTypesFilterEnum::tMultiSelect:
                        if (strval($Item['Condition']) != "") {
                            $QryWhere .= ($QryWhere == "" ? '' : ' AND ');
                            $QryWhere .= $Item['EntityAlias'] . "." . $Item['FieldName'] . " IN (" . $Item['Condition'] . ")";
                        }
                        break;
                }
            }
        }
        return $QryWhere;
    }

    public function getQuery() {
        if (isset($this->Parametros['Query'])) {
            $Qry = $this->Parametros['Query'];

            $QryWhere = $this->getQryWhere();
            if ($QryWhere!='') {
                if (strpos($Qry, 'WHERE')<=0 || strrpos($Qry, 'FROM')>strrpos($Qry, 'WHERE')) {
                    $Qry.=' WHERE '. $QryWhere;
                } else {
                    $Qry.=' AND ('. $QryWhere . ')';
                }
            }

            if (isset($this->Parametros['OrderBy'])) {
                $QryOrderBy = $this->Parametros['OrderBy'];
                if ($QryOrderBy != '') {
                    if (strpos($Qry, 'ORDER BY') <= 0) {
                        $Qry .= ' ORDER BY ' . $QryOrderBy;
                    }
                }
            }
        } else {
            $QrySelect = $this->Parametros['EntityAlias'];
            $QryFrom = $this->Parametros['Entity'] .' '. $this->Parametros['EntityAlias'];
            foreach ($this->Parametros['Campos'] as $Item) {
                if (isset($Item['Relation']['Entity'])) {
                    $QryFrom .= ' LEFT JOIN '. $Item['Relation']['Entity'] .' '. $Item['Relation']['EntityAlias'] .' WITH '. $Item['Relation']['FieldJoined'];
                    if (isset($Item['FieldAlias'])) {
                        $QrySelect .= ($QrySelect!='' ? ', ': ''). $Item['Relation']['EntityAlias'] .".". $Item['FieldName'] .($Item['FieldName'] != $Item['FieldAlias'] ? " AS ". $Item['FieldAlias'] : '');
                    } else {
                        $QrySelect .= ($QrySelect!='' ? ', ': ''). $Item['Relation']['EntityAlias'] .".". $Item['FieldName'];
                    }

                } else {
                    if (isset($Item['FieldAlias'])) {
                        $QrySelect .= ($QrySelect!='' ? ', ': ''). $this->Parametros['EntityAlias'] .".". $Item['FieldName'] .($Item['FieldName'] != $Item['FieldAlias'] ? " AS ". $Item['FieldAlias'] : '');
                    } else {
                        $QrySelect .= ($QrySelect!='' ? ', ': ''). $this->Parametros['EntityAlias'] .".". $Item['FieldName'];
                    }
                }
            }

            $QryWhere = $this->getQryWhere();

            $QryOrderBy = "";
            if (isset($this->Parametros['OrderBy'])) {
                $QryOrderBy = $this->Parametros['OrderBy'];
            }

            $Qry = "SELECT ". $QrySelect ." FROM ". $QryFrom .($QryWhere == "" ? '' : ' WHERE '. $QryWhere).($QryOrderBy == "" ? '' : ' ORDER BY '. $QryOrderBy);
        }
        return $Qry;
    }

    public function getParametros() {
        return $this->Parametros;
    }

    public function setParametros($Value) {
        $this->Parametros = $Value;
    }

    public function setTitle($Value) {
        $this->Parametros['Title'] = $Value;
    }

    public function setEntity($Name, $Alias = 'a') {
        $this->Parametros['Entity'] = $Name;
        $this->Parametros['EntityAlias'] = $Alias;
    }

    public function setQuery($QueryDQL) {
        $this->Parametros['Query'] = $QueryDQL;
        if (!isset($this->Parametros['Entity'])) {
            $this->setEntity((isset($this->Parametros['Title']) ? $this->Parametros['Title'] : ''), 'a');
        }
    }

    public function addCampo($Caption, $Name, $Alias = null, $RelationEntity = null, $RelationEntityAlias = null, $RelationFieldJoined = null, $Visible = true, $Sortable = true) {
        $this->Parametros['Campos'][] =
            array(
                'Caption'=>$Caption,
                'FieldName'=>$Name,
                'FieldAlias'=>(strval($Alias) == "" ? $Name : $Alias),
                'Visible'=>$Visible,
                'Relation'=> array(
                    'Entity'=>$RelationEntity,
                    'EntityAlias'=>$RelationEntityAlias,
                    'FieldJoined'=>$RelationFieldJoined
                ),
                'Sortable'=>$Sortable
            );
    }

    public function setWhere($Value) {
        $this->Parametros['Where'] = $Value;
    }

    public function setOrderBy($Value) {
        $this->Parametros['OrderBy'] = $Value;
    }

    public function addFiltro($Caption, $PlaceHolder, $FieldName, $EntityAlias, $EntityLoaded = null, $AutoPost = true, $TypeFilter = ListadosTypesFilterEnum::tUnique, $DefaultValue = "", $DefaultValueTo = "") {
        try {
            if ($this->request->get('clear') == "true") {
                $this->session->remove($this->Parametros['Entity'].'.'.$FieldName);
                $this->session->remove($this->Parametros['Entity'].'.'.$FieldName.'To');
            } else {
                if (gettype($this->request->get($FieldName))=="string") {
                    $this->session->set($this->Parametros['Entity'].'.'.$FieldName, $this->request->get($FieldName));
                }
                if ($TypeFilter == ListadosTypesFilterEnum::tFromTo) {
                    if (gettype($this->request->get($FieldName.'To'))=="string") {
                        $this->session->set($this->Parametros['Entity'].'.'.$FieldName.'To', $this->request->get($FieldName.'To'));
                    }
                }
                if ($TypeFilter == ListadosTypesFilterEnum::tMultiSelect) {
                    if (gettype($this->request->get($FieldName))=="array") {
                        $this->session->set($this->Parametros['Entity'] . '.' . $FieldName, ($this->request->get($FieldName) ? "'" . join("', '", $this->request->get($FieldName)) . "'" : null));
                    }
                }
            }
        } catch (\Exception $e) {
        }

        $this->Parametros['Filtros'][] =
        array(
            'Caption' => $Caption,
            'PlaceHolder' => $PlaceHolder,
            'FieldName' => $FieldName,
            'EntityAlias' => $EntityAlias,
            'Condition' => (is_string($this->session->get($this->Parametros['Entity'].'.'.$FieldName)) ? $this->session->get($this->Parametros['Entity'].'.'.$FieldName) : $DefaultValue),
            'ConditionTo' => (is_string($this->session->get($this->Parametros['Entity'].'.'.$FieldName.'To')) ? $this->session->get($this->Parametros['Entity'].'.'.$FieldName.'To') : $DefaultValueTo),
            'EntityLoaded' => $EntityLoaded,
            'AutoPost' => $AutoPost,
            'TypeFilter' => $TypeFilter
        );
    }

    public function setPathIndex($Path, $ArgumentsArray = null) {
        $this->Parametros['PathIndex'] =
            array(
                'Path'=> $Path,
                'ArgumentsArray' => ($ArgumentsArray == null ? array(array('id', $this->Parametros['Campos'][0]['FieldAlias'])) : $ArgumentsArray)
            );
    }

    public function setPathShow($Path, $ArgumentsArray = null) {
        $this->Parametros['PathShow'] =
            array(
                'Path'=> $Path,
                'ArgumentsArray' => ($ArgumentsArray == null ? array(array('id', $this->Parametros['Campos'][0]['FieldAlias'])) : $ArgumentsArray)
            );
    }

    public function addActions($Caption, $Path, $Icon, $Security, $StyleClass = '', $ArgumentsArray = null) {
        $this->Parametros['addActions'][] =
            array(
                'Caption' => $Caption,
                'Path' => $Path,
                'Icon' => $Icon,
                'StyleClass' => $StyleClass,
                'Security' => $Security,
                'ArgumentsArray' => ($ArgumentsArray == null ? array(array('id', $this->Parametros['Campos'][0]['FieldAlias'])) : $ArgumentsArray)
            );
    }

    public function addHeaderActions($Caption, $Path, $Icon, $Security, $StyleClass = '', $ArgumentsArray = null) {
        $this->Parametros['addHeaderActions'][] =
            array(
                'Caption' => $Caption,
                'Path' => $Path,
                'Icon' => $Icon,
                'StyleClass' => $StyleClass,
                'Security' => $Security,
                'ArgumentsArray' => ($ArgumentsArray == null ? array(array('id', $this->Parametros['Campos'][0]['FieldAlias'])) : $ArgumentsArray)
            );
    }

    public function setIDRegistrosNoModificables($Value = array()) {
        $this->Parametros['IDRegistrosNoModificables'] = $Value;
    }
}